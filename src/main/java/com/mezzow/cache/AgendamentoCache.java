package com.mezzow.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;

import com.mezzow.domain.Agendamento;

import io.micronaut.cache.annotation.CacheConfig;
import io.micronaut.cache.annotation.CacheInvalidate;
import io.micronaut.cache.annotation.CachePut;
import io.micronaut.data.model.Page;

@Singleton 
@CacheConfig("agendamentosCache")
public class AgendamentoCache {
	
	Map<String, Page<Agendamento>> agendamentos = new HashMap<String, Page<Agendamento>>();
	
	public Page<Agendamento> get(String agendamentoChave ){
		return agendamentos.get(agendamentoChave);
	}
	
	public Map<String, Page<Agendamento>> getP(){
		return agendamentos;
	}
	@CachePut(parameters = {"agendamentoChave"}) 
	public Page<Agendamento> addAgendamento(String agendamentoChave, Page<Agendamento> p ){
//		if (unidades.containsKey(agendamentoChave)) {
//			unidades.put(agendamentoChave, p);
//		}
		
		return agendamentos.put(agendamentoChave, p);
	}
	
	@CacheInvalidate(parameters = {"agendamentoChave"}) 
	public void removeAgendamento(String agendamentoChave, Page<Agendamento> p ){
		if (agendamentos.containsKey(agendamentoChave)) {
			agendamentos.remove(agendamentoChave);
		}
		
	}
	
}	

