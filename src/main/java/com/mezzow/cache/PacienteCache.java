package com.mezzow.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;

import com.mezzow.domain.Paciente;

import io.micronaut.cache.annotation.CacheConfig;
import io.micronaut.cache.annotation.CacheInvalidate;
import io.micronaut.cache.annotation.CachePut;
import io.micronaut.data.model.Page;

@Singleton 
@CacheConfig("pacientesCache")
public class PacienteCache {
	
	Map<String, Page<Paciente>> pacientes = new HashMap<String, Page<Paciente>>();
	
	public Page<Paciente> get(String chave ){
		return pacientes.get(chave);
	}
	
	public Map<String, Page<Paciente>> getP(){
		return pacientes;
	}
	@CachePut(parameters = {"chave"}) 
	public Page<Paciente> addPaciente(String chave, Page<Paciente> p ){
//		if (unidades.containsKey(chave)) {
//			unidades.put(chave, p);
//		}
		
		return pacientes.put(chave, p);
	}
	
	@CacheInvalidate(parameters = {"chave"}) 
	public void removePaciente(String chave, Page<Paciente> p ){
		if (pacientes.containsKey(chave)) {
			pacientes.remove(chave);
		}
		
	}
	
}	

