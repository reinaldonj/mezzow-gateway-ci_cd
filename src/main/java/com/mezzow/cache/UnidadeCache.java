package com.mezzow.cache;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;

import com.mezzow.domain.Unidade;

import io.micronaut.cache.annotation.CacheConfig;
import io.micronaut.cache.annotation.CacheInvalidate;
import io.micronaut.cache.annotation.CachePut;
import io.micronaut.data.model.Page;

@Singleton 
@CacheConfig("unidadesCache")
public class UnidadeCache {
	
	Map<String, Page<Unidade>> unidades = new HashMap<String, Page<Unidade>>();
	
	public Page<Unidade> get(String unidadeChave ){
		return unidades.get(unidadeChave);
	}
	
	public Map<String, Page<Unidade>> getP(){
		return unidades;
	}
	@CachePut(parameters = {"unidadeChave"}) 
	public Page<Unidade> addPaciente(String unidadeChave, Page<Unidade> p ){
//		if (unidades.containsKey(chave)) {
//			unidades.put(chave, p);
//		}
		
		return unidades.put(unidadeChave, p);
	}
	
	@CacheInvalidate(parameters = {"unidadeChave"}) 
	public void removePaciente(String unidadeChave, Page<Unidade> p ){
		if (unidades.containsKey(unidadeChave)) {
			unidades.remove(unidadeChave);
		}
		
	}
	
}	

