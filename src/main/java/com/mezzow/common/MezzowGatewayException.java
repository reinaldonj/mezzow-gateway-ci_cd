package com.mezzow.common;

import io.micronaut.http.HttpStatus;

public class MezzowGatewayException extends RuntimeException {

	private final HttpStatus httpStatus;
	
	 public MezzowGatewayException(String message, Throwable cause, HttpStatus httpStatus) {
	        super(message, cause);
	        this.httpStatus = httpStatus;
	 }
	
	 public MezzowGatewayException(String message, HttpStatus httpStatus) {
	        super(message);
	        this.httpStatus = httpStatus;
	 }
	 
	
	 
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	 
	 
	 
}
