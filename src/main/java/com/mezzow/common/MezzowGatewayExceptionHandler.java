package com.mezzow.common;

import javax.inject.Singleton;

import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpResponseFactory;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;

@Produces
@Singleton 
@Requires(classes = {MezzowGatewayException.class, ExceptionHandler.class}) 
public class MezzowGatewayExceptionHandler implements ExceptionHandler<MezzowGatewayException, HttpResponse<APIError>> {

	@Override
    public HttpResponse<APIError> handle(HttpRequest request, MezzowGatewayException exception) {
		APIError apiError = new APIError();
		apiError.setCode("9"+exception.getHttpStatus().getCode());
		apiError.setMessage(exception.getMessage());
		return HttpResponseFactory.INSTANCE.status(exception.getHttpStatus(), apiError);
    }
	
}
