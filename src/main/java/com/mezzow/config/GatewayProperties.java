package com.mezzow.config;

import java.util.Set;

import io.micronaut.context.annotation.ConfigurationProperties;

@ConfigurationProperties("gateway")
public class GatewayProperties {
    private Set<String> services;

	public Set<String> getServices() {
		return services;
	}

	public void setServices(Set<String> services) {
		this.services = services;
	}
    
    
}