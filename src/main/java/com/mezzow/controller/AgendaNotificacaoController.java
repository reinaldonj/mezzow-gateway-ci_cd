package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.common.PermissaoCode;
import com.mezzow.domain.AgendaNotificacao;
import com.mezzow.rest.client.AgendaNotificacaoClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller(value = "/agendaNotificacao")
@Validated
public class AgendaNotificacaoController {

	private final AgendaNotificacaoClient client;

    public AgendaNotificacaoController(
    		AgendaNotificacaoClient client) {
    	
        this.client = client;
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<AgendaNotificacao> show(@NotNull Long id) {
    	return client.show(id);
    }

    
    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Get("/{?max,offset,sort,order}")
    public Page<AgendaNotificacao> list(
    		@Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order){
    	
        return client.find(max, offset, sort, order);
    }

    @Secured({PermissaoCode.ROLE_ADMIN})
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public AgendaNotificacao create(@Body AgendaNotificacao agendaNotificacao) {     
    		return client.save(agendaNotificacao);
    }

    @Secured({PermissaoCode.ROLE_ADMIN})
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public AgendaNotificacao update(@Body AgendaNotificacao agendaNotificacao) {
        return client.update(agendaNotificacao);
    }
    
    @Secured({PermissaoCode.ROLE_ADMIN})
    @Version("1")
    @Delete("/{id}")
    public void delete(@NotNull Long id) {
         client.delete(id);
    }

}
