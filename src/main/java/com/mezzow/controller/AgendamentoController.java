package com.mezzow.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import com.mezzow.common.PermissaoCode;
import com.mezzow.domain.Agenda;
import com.mezzow.domain.Agendamento;
import com.mezzow.domain.Paciente;
import com.mezzow.domain.Unidade;
import com.mezzow.rest.client.AgendamentoClient;
import com.mezzow.rest.client.PacienteClient;
import com.mezzow.service.AgendamentoService;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller(value = "/agendamento")
@Validated
public class AgendamentoController {

	private final AgendamentoClient agendamentoClient;
	private final PacienteClient pacienteClient;

	private AgendamentoService agendamentoService;
	
    public AgendamentoController(AgendamentoClient agendamentoClient, PacienteClient pacienteClient, AgendamentoService agendamentoService) {
        this.agendamentoClient = agendamentoClient;
        this.pacienteClient = pacienteClient;
        this.agendamentoService = agendamentoService;
    }
    
    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_ATENDENTE, PermissaoCode.ROLE_MEDICO_ATENDIMENTO, PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Version("1")
    @Get(uri = "/relatorio/download", consumes = {MediaType.MULTIPART_FORM_DATA})
    public HttpResponse<byte[]> downLoadTipoRelatorioAgendamento(
    		@Nullable @QueryValue List<String> grupoEmpresaId,
    		@Nullable @QueryValue List<String>  empresaId,
    		@Nullable @QueryValue List<Long>  unidadeId,
    		@Nullable @QueryValue String dataDe,
    		@Nullable @QueryValue String dataAte,
    		@Nullable @QueryValue List<Long> listaStatus
    		) throws IOException {
    	
    	byte[] pdfAsBytes = agendamentoService.combinarFiltrosGerarStreamPDF(grupoEmpresaId, empresaId, unidadeId, dataDe, dataAte, listaStatus);
    	
    	return  HttpResponse.ok().contentType(MediaType.APPLICATION_PDF_TYPE).body(pdfAsBytes).header("Content-Disposition", "inline; filename=file.pdf").contentLength(pdfAsBytes.length); // new StreamedFile(new ByteArrayInputStream(pdfAsBytes), MediaType.APPLICATION_PDF_TYPE);
    }
    
    
    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)    
    public Optional<Agendamento> show(Long id) {
    	return agendamentoService.show(id);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Get("/{?nomePaciente,cpfPaciente,unidadeId,dataDe,dataAte,listaStatus,max,offset,sort,order}")
    @Produces(MediaType.APPLICATION_JSON)
    public Page<Agendamento> list(
    		@Nullable String nomePaciente,
    		@Nullable String cpfPaciente,
    		@Nullable List<Long> unidadeId,
    		@Nullable String dataDe,
    		@Nullable String dataAte,
    		@Nullable List<Long> listaStatus,
    		@Nullable Integer max,
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order) {
    		
    	return agendamentoService.list(nomePaciente, cpfPaciente, unidadeId, dataDe, dataAte, listaStatus, max, offset, sort, order);
    }

	private String toString(List<Long> listaStatus) {
		
		if (listaStatus != null) return StringUtils.join(listaStatus, ',');
		
		return null;
	}

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_ATENDENTE})
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public Agendamento create(@Body @Valid Agendamento agendamento) {
    	
    	Unidade u = new Unidade();
    	u.setId(agendamento.getUnidade().getId());
    	agendamento.setUnidade(u);
    	
    	if (agendamento.getPaciente().getId() == null) {
    		Paciente save = pacienteClient.save(agendamento.getPaciente());
    		Paciente p = new Paciente();
    		p.setId(save.getId());
    		
    		agendamento.setPaciente(p);
    		
    	}

        return agendamentoService.save(agendamento);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_ATENDENTE})
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public Agendamento update(@Body @Valid Agendamento agendamento) {
        return agendamentoClient.update(agendamento);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_ATENDENTE})
    @Delete("/{id}")
    public void delete(@NotNull Long id) {
      
        agendamentoClient.delete(id);
    }
	
    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_ATENDENTE})
    @Version("1")
    @Get("/agenda/{dataReferencia}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Agenda> obterAgendas(@PathVariable @Nullable String dataReferencia) {
    	return agendamentoService.obterAgendas(dataReferencia);
    }
}
