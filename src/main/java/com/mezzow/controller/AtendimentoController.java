package com.mezzow.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.mezzow.common.PermissaoCode;
import com.mezzow.domain.Atendimento;
import com.mezzow.service.AtendimentoService;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller(value = "/atendimento")
@Validated
public class AtendimentoController {
	
	
	private final AtendimentoService service;
	
    public AtendimentoController(AtendimentoService Service) {
        this.service = Service;
        
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)    
    public Optional<Atendimento> show(Long id) {        
    	return service.show(id);
    }


    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_AUXILIAR_ADMIN}) 
    @Get("/{?grupoEmpresa,empresa,unidade,setor,medico,tipoConsulta,dataInicial,dataFinal,status,tipoOrientacaoPosConsulta,max,offset,sort,order}")
    @Produces(MediaType.APPLICATION_JSON)
    public Page<Atendimento> list(
    		@Nullable Long grupoEmpresa,
    		@Nullable Long empresa,
    		@Nullable List<Long> unidade,
    		@Nullable List<Long> setor,
    		@Nullable List<String> medico,
    		@Nullable List<Long> tipoConsulta,
    		@Nullable String dataInicial,
    		@Nullable String dataFinal,
    		@Nullable List<Long> status,
    		@Nullable List<Long> tipoOrientacaoPosConsulta,
    		@Nullable Integer max,
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order) {
    		
    	return service.list(grupoEmpresa, empresa, unidade, setor, medico, tipoConsulta, dataInicial, dataFinal, status, tipoOrientacaoPosConsulta, max, offset, sort, order);

    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO})
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public Atendimento update(@Body @Valid Atendimento atendimento) {
        return service.update(atendimento);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_AUXILIAR_ADMIN}) 
    @Get(value = "/download/{idAtendimento}", consumes = {MediaType.MULTIPART_FORM_DATA})
    public HttpResponse<byte[]> downLoadFile(@PathVariable Long idAtendimento) throws IOException {
        
    	byte[] pdfAsBytes = service.generateStreamPDFRelatorioMedico(idAtendimento);
    	
    	return  HttpResponse.ok().contentType(MediaType.APPLICATION_PDF_TYPE).body(pdfAsBytes).header("Content-Disposition", "inline; filename=file.pdf").contentLength(pdfAsBytes.length); // new StreamedFile(new ByteArrayInputStream(pdfAsBytes), MediaType.APPLICATION_PDF_TYPE);
    }
    
    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO, PermissaoCode.ROLE_ATENDENTE, PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Version("1")
    @Get(uri = "/relatorio/download", consumes = {MediaType.MULTIPART_FORM_DATA})
    public HttpResponse<byte[]> downLoadTipoRelatorioAgendamento(
    		@Nullable @QueryValue List<String> grupoEmpresaId,
    		@Nullable @QueryValue List<String>  empresaId,
    		@Nullable @QueryValue List<Long>  unidadeId,
    		@Nullable @QueryValue List<Long>  tipoConsultaId,
    		@Nullable @QueryValue String dataDe,
    		@Nullable @QueryValue String dataAte
    		) throws IOException {
//        return null;
    	byte[] pdfAsBytes = service.combinarFiltrosAtendimentoGerarStreamPDF(grupoEmpresaId, empresaId, unidadeId, tipoConsultaId, dataDe, dataAte);
//    	
//    	
    	return  HttpResponse.ok().contentType(MediaType.APPLICATION_PDF_TYPE).body(pdfAsBytes).header("Content-Disposition", "inline; filename=file.pdf").contentLength(pdfAsBytes.length); // new StreamedFile(new ByteArrayInputStream(pdfAsBytes), MediaType.APPLICATION_PDF_TYPE);
    }
    
}
