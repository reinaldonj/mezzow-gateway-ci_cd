package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.domain.Cid;
import com.mezzow.rest.client.CIDClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/cid")
@Validated
public class CIDController {

	
    private final CIDClient client;

    public CIDController(
    		CIDClient client) {
    	
        this.client = client;
    }
   
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<Cid> show(@NotNull Long id) {
    	return client.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{?codigo,descricao,max,offset,sort,order}")
    @Produces(MediaType.APPLICATION_JSON)
    public Page<Cid> list(
    		@Nullable String codigo, 
    		@Nullable String descricao, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order) {
    	
        return client.find(codigo, descricao, max, offset, sort, order);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public Cid create(@Body Cid usuario) {     
    	
    	
    		return client.save(usuario);
    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public Cid update(@Body Cid usuario) {
        return client.update(usuario);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public HttpResponse<?> delete(@NotNull Long id) {
        return client.delete(id);
    }
    
    
    

}
