package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.domain.ContratoProduto;
import com.mezzow.rest.client.ContratoProdutoClient;
import com.mezzow.service.ContratoProdutoService;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;
import io.reactivex.Single;

@Controller(value = "/contratoProduto")
@Validated
public class ContratoProdutoController {

	private final ContratoProdutoClient client;
	private final ContratoProdutoService service;
	
    public ContratoProdutoController(
    		ContratoProdutoClient client, ContratoProdutoService service) {
        this.client = client;
        this.service = service;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<ContratoProduto> show(@NotNull Long id) {
    	return client.show(id);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get(value = "/{?nomeProduto,nomeGrupoEmpresa,nomeEmpresa,nomeUnidade,max,offset,sort,order}", produces = MediaType.APPLICATION_JSON_STREAM)
    public Page<ContratoProduto> list(
    		@Nullable String  nomeProduto, 
    		@Nullable String nomeGrupoEmpresa, 
    		@Nullable String nomeEmpresa, 
    		@Nullable String nomeUnidade, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order,
    		@Header("Authorization") String authorization){
    		
    	return service.list(
    			Optional.ofNullable( nomeProduto ), 
    			Optional.ofNullable(nomeGrupoEmpresa), 
    			Optional.ofNullable(nomeEmpresa), 
    			Optional.ofNullable(nomeUnidade), 
    			Optional.ofNullable(max), 
    			Optional.ofNullable(offset), 
    			Optional.ofNullable(sort), 
    			Optional.ofNullable(order), 
    			authorization);
    }

    
	@Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public ContratoProduto create(@Body ContratoProduto usuario) {     
    	return client.save(usuario);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public ContratoProduto update(@Body ContratoProduto usuario) {
        return client.update(usuario);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public HttpResponse<?> delete(@NotNull Long id) {
      return client.delete(id);
    }
}
