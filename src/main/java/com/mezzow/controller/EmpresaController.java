package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.domain.Empresa;
import com.mezzow.rest.client.EmpresaClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/empresa")
@Validated
public class EmpresaController {

	private final EmpresaClient client;

    public EmpresaController(
    		EmpresaClient client) {
    	
        this.client = client;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<Empresa> show(@NotNull Long id) {
    	return client.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{?nome,cnpj,grupo,situacao,max,offset,sort,order}")
    public Page<Empresa> list(
    		@Nullable String nome, @Nullable String cnpj, @Nullable String grupo, @Nullable Boolean situacao, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order){
    	
        return client.find(nome, cnpj, grupo, situacao, max, offset, sort, order);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public Empresa create(@Body Empresa usuario) {     
    		return client.save(usuario);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public Empresa update(@Body Empresa usuario) {
        return client.update(usuario);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public HttpResponse<?> delete(@NotNull Long id) {
         return client.delete(id);
    }

}
