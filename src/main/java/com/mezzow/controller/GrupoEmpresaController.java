package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.domain.GrupoEmpresa;
import com.mezzow.rest.client.GrupoEmpresaClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/grupoEmpresa")
@Validated
public class GrupoEmpresaController {

	private final GrupoEmpresaClient client;

    public GrupoEmpresaController(
    		GrupoEmpresaClient client) {
    	
        this.client = client;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<GrupoEmpresa> show(@NotNull Long id) {
    	return client.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{?nome,descricao,max,offset,sort,order}")
    public Page<GrupoEmpresa> list(
    		@Nullable String nome, 
    		@Nullable String descricao, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order){
    	
        return client.find(nome, descricao, max, offset, sort, order);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public GrupoEmpresa create(@Body String usuario) {     
    	
//    	try {
    		return client.save(usuario);
//    	}catch (HttpClientResponseException e ) {
//    		return e.getResponse();
//    	}
    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public GrupoEmpresa update(@Body String usuario) {
        return client.update(usuario);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public HttpResponse<?> delete(@NotNull Long id) {
        return client.delete(id);
    }

}
