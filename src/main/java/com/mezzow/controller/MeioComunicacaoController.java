package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.common.PermissaoCode;
import com.mezzow.domain.MeioComunicacao;
import com.mezzow.rest.client.MeioComunicacaoClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller(value = "/meioComunicacao")
@Validated
public class MeioComunicacaoController {

	private final MeioComunicacaoClient client;

    public MeioComunicacaoController(
    		MeioComunicacaoClient client) {
    	
        this.client = client;
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<MeioComunicacao> show(@NotNull Long id) {
    	return client.show(id);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Get("/{?codigo,descricao,max,offset,sort,order}")
    public Page<MeioComunicacao> list(
    		@Nullable String codigo, @Nullable String descricao, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order){
    	
        return client.find(codigo, descricao, max, offset, sort, order);
    }

    @Secured({PermissaoCode.ROLE_ADMIN})
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public MeioComunicacao create(@Body MeioComunicacao meioComunicacao) {     
    		return client.save(meioComunicacao);
    }

    @Secured({PermissaoCode.ROLE_ADMIN})
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public MeioComunicacao update(@Body MeioComunicacao meioComunicacao) {
        return client.update(meioComunicacao);
    }
    
    @Secured({PermissaoCode.ROLE_ADMIN})
    @Version("1")
    @Delete("/{id}")
    public void delete(@NotNull Long id) {
         client.delete(id);
    }

}
