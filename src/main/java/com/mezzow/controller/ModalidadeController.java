package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.domain.Modalidade;
import com.mezzow.rest.client.ModalidadeClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/modalidade")
@Validated
public class ModalidadeController {

	private final ModalidadeClient client;

    public ModalidadeController(
    		ModalidadeClient client) {
    	
        this.client = client;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<Modalidade> show(@NotNull Long id) {
    	return client.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{?sigla,nome,max,offset,sort,order}")
    public Page<Modalidade> list(
    		@Nullable String sigla, 
    		@Nullable String nome, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order){
    	
        return client.find(sigla, nome, max, offset, sort, order);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public Modalidade create(@Body Modalidade usuario) {     
    	
    	
    		return client.save(usuario);
    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public Modalidade update(@Body Modalidade usuario) {
        return client.update(usuario);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public HttpResponse<?> delete(@NotNull Long id) {
        return client.delete(id);
    }

}
