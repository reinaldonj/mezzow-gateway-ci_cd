package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.domain.ModeloContraste;
import com.mezzow.rest.client.ModeloContrasteClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/modeloContraste")
@Validated
public class ModeloContrasteController {

	private final ModeloContrasteClient client;

    public ModeloContrasteController(
    		ModeloContrasteClient client) {
    	
        this.client = client;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<ModeloContraste> show(@NotNull Long id) {
    	return client.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{?titulo,nomeGrupoEmpresa,nomeUnidade,nomeModalidade,max,offset,sort,order}")
    public Page<ModeloContraste> list(
    		@Nullable String titulo, @Nullable String nomeGrupoEmpresa,
			@Nullable String nomeUnidade, @Nullable String nomeModalidade, @Nullable Integer max, @Nullable Integer offset,
			@Nullable String sort, @Nullable String order){
    	
        return client.find(titulo, nomeGrupoEmpresa,nomeUnidade,nomeModalidade, max, offset, sort, order);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public ModeloContraste create(@Body ModeloContraste usuario) {     
    	
    	
    		return client.save(usuario);
    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public ModeloContraste update(@Body ModeloContraste usuario) {
        return client.update(usuario);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public void delete(@NotNull Long id) {
         client.delete(id);
    }

}
