package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.domain.ModeloCorpoLaudo;
import com.mezzow.rest.client.ModeloCorpoLaudoClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/modeloCorpoLaudo")
@Validated
public class ModeloCorpoLaudoController {

	private final ModeloCorpoLaudoClient client;

    public ModeloCorpoLaudoController(
    		ModeloCorpoLaudoClient client) {
    	
        this.client = client;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<ModeloCorpoLaudo> show(@NotNull Long id) {
    	return client.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{?grupoEmpresa,unidade,tipoExame,titulo,max,offset,sort,order}")
    public Page<ModeloCorpoLaudo> list(
    		@Nullable String grupoEmpresa, @Nullable String unidade,
			@Nullable String tipoExame, @Nullable String titulo, @Nullable Integer max, @Nullable Integer offset,
			@Nullable String sort, @Nullable String order){
    	
        return client.find(grupoEmpresa, unidade,tipoExame,titulo, max, offset, sort, order);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public ModeloCorpoLaudo create(@Body ModeloCorpoLaudo usuario) {     
    	
    	
    		return client.save(usuario);
    	
    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public ModeloCorpoLaudo update(@Body ModeloCorpoLaudo usuario) {
        return client.update(usuario);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public void delete(@NotNull Long id) {
         client.delete(id);
    }

}
