package com.mezzow.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.common.PermissaoCode;
import com.mezzow.domain.ModeloNotificacao;
import com.mezzow.rest.client.ModeloNotificacaoClient;
import com.mezzow.service.ModeloNotificacaoService;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;
import io.reactivex.Single;

@Controller(value = "/modeloNotificacao")
@Validated
@Secured(SecurityRule.IS_AUTHENTICATED)
public class ModeloNotificacaoController {

	private final ModeloNotificacaoClient client;

    private final ModeloNotificacaoService service;
    
    public ModeloNotificacaoController(ModeloNotificacaoClient client, ModeloNotificacaoService service) {
        this.client = client;
        this.service = service; 
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Single<Optional<ModeloNotificacao>> show(@NotNull Long id) {
    	return service.show(id);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Get("/findByUnidadeIdAndTipoNotificacaoId{?unidadeId,tipoNotificacaoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ModeloNotificacao> findAllByUnidadeIdAndTipoNotificacaoId(@Nullable Long unidadeId, @Nullable Long tipoNotificacaoId) {
    	return client.findAllByUnidadeIdAndTipoNotificacaoId(unidadeId, tipoNotificacaoId);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Get("/findByGrupoEmpresaIdAndTipoNotificacaoId{?grupoEmpresaId,tipoNotificacaoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ModeloNotificacao> findAllByGrupoEmpresaIdAndTipoNotificacaoId(@Nullable Long grupoEmpresaId, @Nullable Long tipoNotificacaoId) {
    	return client.findAllByGrupoEmpresaIdAndTipoNotificacaoId(grupoEmpresaId, tipoNotificacaoId);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Get("/{?grupoEmpresa,unidade,tipoNotificacao,titulo,max,offset,sort,order}")
    public Single<Page<ModeloNotificacao>> list(
    		@Nullable String grupoEmpresa, @Nullable String unidade,
			@Nullable String tipoNotificacao, @Nullable String titulo, @Nullable Integer max, @Nullable Integer offset,
			@Nullable String sort, @Nullable String order){
        //LinkedHashMap linkedHashMap = (LinkedHashMap) grupoEmpresaClient.show(1l).getBody().get();
//        System.out.println("-->" + grupoEmpresaClient.show(1l).getBody().get());
//        System.out.println("2-->" + linkedHashMap.get("nome"));

//        LinkedHashMap modelos = (LinkedHashMap) client.find(grupoEmpresa, unidade,tipoNotificacao,titulo, max, offset, sort, order).getBody().get();
//        System.out.println("3-->" + modelos);

     /*   return HttpResponse.created(client.find(grupoEmpresa, unidade,tipoNotificacao,titulo, max, offset, sort, order).getBody().map(o -> {
            System.out.println("o----->> " + o);
            LinkedHashMap page = (LinkedHashMap) o;
            ObjectMapper objectMapper = new ObjectMapper();
            List<ModeloNotificacao> modelos = objectMapper.convertValue(page.get("content"), new TypeReference<List<ModeloNotificacao>>(){});
            System.out.println("content----->> " + modelos);
            modelos.forEach(o1 -> {
                System.out.println("o111----->> " + o1);
                //o1.setGrupoEmpresa(grupoEmpresaClient.show(o1.getGrupoEmpresa().getId()).getBody()));
                o1.getGrupoEmpresa().setNome("empresa");
                System.out.println("o222222----->> " + o1);
            });
//            ((ArrayList) page.get("content")).stream().map(m -> {
//                System.out.println("MMMM----->> " + m);
//                return m;
//            });
            page.put("content", modelos);
            System.out.println("page ----->> " + page);
            return page;
        }));

*/
    	System.out.println("******************************************> ENTROU NO MÉTODO FIND");
        return service.find(grupoEmpresa, unidade, tipoNotificacao, titulo, max, offset, sort, order);

    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN})
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public ModeloNotificacao create(@Body ModeloNotificacao usuario) {     
    	
    
    		return client.save(usuario);
    
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN})
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public ModeloNotificacao update(@Body ModeloNotificacao usuario) {
        return client.update(usuario);
    }
    
    
    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN})
    @Version("1")
    @Delete("/{id}")
    public HttpResponse<?> delete(@NotNull Long id) {
        return client.delete(id);
    }

}
