package com.mezzow.controller;

import com.mezzow.domain.Notificacao;
import com.mezzow.rest.client.NotificacaoClient;
import com.mezzow.service.NotificacaoService;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/notificacao")
@Validated
public class NotificacaoController {

	private final NotificacaoClient client;
	private final NotificacaoService service;

    public NotificacaoController(NotificacaoClient client, NotificacaoService service) {
        this.client = client;
        this.service = service;
    }

//    @Secured(SecurityRule.IS_AUTHENTICATED)
//    @Get("/{id}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Optional<Notificacao> show(@NotNull Long id) {
//    	return client.show(id);
//    }

    
//    @Secured(SecurityRule.IS_AUTHENTICATED)
//    @Get("/{?max,offset,sort,order}")
//    public Page<Notificacao> list(
//    		@Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order){
//    	
//        return client.find(max, offset, sort, order);
//    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public Notificacao create(@Body Notificacao notificacao) {     
    		return service.save(notificacao);
    }

//    @Secured(SecurityRule.IS_AUTHENTICATED)
//    @Version("1")
//    @Put
//    @Produces(MediaType.APPLICATION_JSON)
//    public Notificacao update(@Body Notificacao notificacao) {
//        return client.update(notificacao);
//    }
    
    
//    @Secured(SecurityRule.IS_AUTHENTICATED)
//    @Version("1")
//    @Delete("/{id}")
//    public Notificacao delete(@NotNull Long id) {
//        return client.delete(id);
//    }
}
