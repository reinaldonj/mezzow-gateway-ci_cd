package com.mezzow.controller;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import com.mezzow.cache.PacienteCache;
import com.mezzow.domain.Paciente;
import com.mezzow.rest.client.PacienteClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/paciente")
@Validated
public class PacienteController {

	private final PacienteClient client;

	@Inject
	private PacienteCache cache;
	
    public PacienteController(
    		PacienteClient client) {
    	
        this.client = client;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<Paciente> show(@NotNull Long id) {
    	return client.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Produces(MediaType.APPLICATION_JSON)
    @Get("/{?unidade,prontuario,nome,cpf,patientId,max,offset,sort,order}")
    public Page<Paciente> list(
    		@Nullable String unidade, @Nullable String prontuario,
    		@Nullable String nome, @Nullable String cpf, @Nullable String patientId, @Nullable Integer max, @Nullable Integer offset,
			@Nullable String sort, @Nullable String order){
    	
//    	REMOVIDO TRATAMENTO DE CACHE DE PACIENTES.
//    	Page<Paciente> page = cache.get(unidade + prontuario + nome + cpf + max + offset + sort + order);
//    	
//    	if (page != null) {
//    		return page;
//    	}else {
//    		Page<Paciente> find = client.find(unidade, prontuario, nome, cpf, max, offset, sort, order);
//    		cache.addPaciente(unidade + prontuario + nome + cpf + max + offset + sort + order, find);
//    		return find;
//    	}
    	
    	return client.find(unidade, prontuario, nome, cpf, patientId, max, offset, sort, order);
    	
        
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public Paciente create(@Body Paciente paciente) {     
    	
//    	try {
    		return client.save(paciente); 
//    		return e.getResponse();
//    	}
    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public Paciente update(@Body Paciente paciente) {
        return client.update(paciente);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public void delete(@NotNull Long id) {
        client.delete(id);
    }

}
