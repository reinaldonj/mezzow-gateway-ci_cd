package com.mezzow.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.domain.Perfil;
import com.mezzow.rest.client.PerfilClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/perfil")
@Validated
public class PerfilController {

	private final PerfilClient client;

    public PerfilController(
    		PerfilClient client) {
    	
        this.client = client;
    }
    
    @Consumes(MediaType.APPLICATION_JSON)
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<Perfil> show(@NotNull String id) {
    	return client.show(id);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{?authority,max,offset,sort,order}")
	Page<Perfil> find(
			@Nullable String authority,
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order){
    	
        return client.find(authority ,max, offset, sort, order);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Consumes(MediaType.APPLICATION_JSON)
	@Get("/listaId/{listaId}")
	List<Perfil> findByIds(List<String> listaId){
    	
        return client.findByIds(listaId);
    }
}
