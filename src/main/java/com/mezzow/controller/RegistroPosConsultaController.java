package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.common.PermissaoCode;
import com.mezzow.domain.RegistroPosConsulta;
import com.mezzow.rest.client.RegistroPosConsultaClient;
import com.mezzow.service.RegistroPosConsultaService;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;
import io.reactivex.Single;

@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller(value = "/registroPosConsulta")
@Validated
public class RegistroPosConsultaController {

    private final RegistroPosConsultaClient client;
    private final RegistroPosConsultaService service;

    public RegistroPosConsultaController(RegistroPosConsultaClient client, RegistroPosConsultaService service) {
        this.client = client;
        this.service = service;
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Version("1")
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Single<Optional<RegistroPosConsulta>> show(Long id) {
        return service.show(id);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Version("1")
    @Produces(MediaType.APPLICATION_JSON)
    @Get("/{?titulo,conteudo,modeloPosConsultaId,max,offset,sort,order}")
	Page<RegistroPosConsulta> find(@Nullable String titulo, @Nullable String conteudo, @Nullable Long modeloPosConsultaId, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order) {

        return client.find(titulo, conteudo, modeloPosConsultaId, max, offset, sort, order);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO})
    @Version("1")
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public RegistroPosConsulta create(@Body RegistroPosConsulta registroPosConsulta) {
        return client.save(registroPosConsulta);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO})
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public RegistroPosConsulta update(@Body RegistroPosConsulta registroPosConsulta) {
        return client.update(registroPosConsulta);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO})
    @Version("1")
    @Delete("/{id}")
    public HttpResponse<?> delete(@NotNull Long id) {
    	return client.delete(id);
    }
}
