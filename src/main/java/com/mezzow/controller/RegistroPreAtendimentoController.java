package com.mezzow.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.common.PermissaoCode;
import com.mezzow.domain.RegistroPreAtendimento;
import com.mezzow.domain.enums.StatusPreAtendimentoEnum;
import com.mezzow.rest.client.RegistroPreAtendimentoClient;
import com.mezzow.service.RegistroPreAtendimentoService;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller(value = "/registroPreAtendimento")
@Validated
public class RegistroPreAtendimentoController {

    private final RegistroPreAtendimentoClient client;
    private final RegistroPreAtendimentoService service;

    public RegistroPreAtendimentoController(RegistroPreAtendimentoService service, RegistroPreAtendimentoClient client) {
        this.service = service;
        this.client = client;
    }
    
    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Version("1")
    @Get(uri = "/relatorio/cancelamento/download", consumes = {MediaType.MULTIPART_FORM_DATA})
    public HttpResponse<byte[]> downLoadTipoRelatorioAgendamento(
    		@Nullable @QueryValue List<String> grupoEmpresaId,
    		@Nullable @QueryValue List<String>  empresaId,
    		@Nullable @QueryValue List<Long>  unidadeId,
    		@Nullable @QueryValue String dataDe,
    		@Nullable @QueryValue String dataAte
    		) throws IOException {
//        return null;
    	byte[] pdfAsBytes = service.combinarFiltrosCancelamentoGerarStreamPDF(grupoEmpresaId, empresaId, unidadeId, dataDe, dataAte);
//    	
//    	
    	return  HttpResponse.ok().contentType(MediaType.APPLICATION_PDF_TYPE).body(pdfAsBytes).header("Content-Disposition", "inline; filename=file.pdf").contentLength(pdfAsBytes.length); // new StreamedFile(new ByteArrayInputStream(pdfAsBytes), MediaType.APPLICATION_PDF_TYPE);
    }
    
    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Version("1")
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<RegistroPreAtendimento> show(Long id) {
        return service.show(id);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Version("1")
    @Get("/{?agendamentoId,perfilId,observacao,status,max,offset,sort,order}")
    @Produces(MediaType.APPLICATION_JSON)
    public Page<RegistroPreAtendimento> list(@Nullable Long agendamentoId, @Nullable String perfilId, @Nullable String observacao, @Nullable StatusPreAtendimentoEnum status, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order) {

        return service.find(agendamentoId, perfilId, observacao, max, offset, sort, order);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_ATENDENTE})
    @Version("1")
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public RegistroPreAtendimento create(@Body RegistroPreAtendimento registroPreAtendimento) {
        return service.save(registroPreAtendimento);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_ATENDENTE})
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public RegistroPreAtendimento update(@Body RegistroPreAtendimento registroPreAtendimento) {
        return service.update(registroPreAtendimento);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_ATENDENTE})
    @Version("1")
    @Delete("/{id}")
    public void delete(@NotNull Long id) {
    	 client.delete(id);
    }
}
