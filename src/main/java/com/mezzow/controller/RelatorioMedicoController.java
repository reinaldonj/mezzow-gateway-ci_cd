package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.domain.RelatorioMedico;
import com.mezzow.rest.client.RelatorioMedicoClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/relatorioMedico")
@Validated
public class RelatorioMedicoController {

    private final RelatorioMedicoClient service;

    public RelatorioMedicoController(RelatorioMedicoClient service) {
        this.service = service;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<RelatorioMedico> show(Long id) {
        return service.show(id);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Produces(MediaType.APPLICATION_JSON)
    @Get("/{?nomeGrupoEmpresa,nomeUnidade,nomeTipoConsulta,titulo,max,offset,sort,order}")
	Page<RelatorioMedico> find(
			@Nullable String nomeGrupoEmpresa, @Nullable String nomeUnidade,
			@Nullable String nomeTipoConsulta, @Nullable String titulo, @Nullable Integer max, @Nullable Integer offset,
			@Nullable String sort, @Nullable String order) {

        return service.find(nomeGrupoEmpresa, nomeUnidade, nomeTipoConsulta, titulo, max, offset, sort, order);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public RelatorioMedico create(@Body RelatorioMedico registroPreAtendimento) {
        return service.save(registroPreAtendimento);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public RelatorioMedico update(@Body RelatorioMedico registroPreAtendimento) {
        return service.update(registroPreAtendimento);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public HttpResponse<?> delete(@NotNull Long id) {
    	return service.delete(id);
    }
}
