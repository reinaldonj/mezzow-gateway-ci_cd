package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.common.PermissaoCode;
import com.mezzow.domain.RespostaContato;
import com.mezzow.rest.client.RespostaContatoClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller(value = "/respostaContato")
@Validated
public class RespostaContatoController {

    private final RespostaContatoClient service;

    public RespostaContatoController(RespostaContatoClient service) {
        this.service = service;
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Version("1")
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<RespostaContato> show(Long id) {
        return service.show(id);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN,PermissaoCode.ROLE_MEDICO_ATENDIMENTO,PermissaoCode.ROLE_ATENDENTE,PermissaoCode.ROLE_AUXILIAR_ADMIN})
    @Version("1")
    @Produces(MediaType.APPLICATION_JSON)
    @Get("/{?descricao,max,offset,sort,order}")
	Page<RespostaContato> list(@Nullable String descricao, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order) {

        return service.list(descricao, max, offset, sort, order);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN})
    @Version("1")
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public RespostaContato create(@Body RespostaContato registroPreAtendimento) {
        return service.save(registroPreAtendimento);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN})
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public RespostaContato update(@Body RespostaContato registroPreAtendimento) {
        return service.update(registroPreAtendimento);
    }

    @Secured({PermissaoCode.ROLE_ADMIN,PermissaoCode.ROLE_CLIENTE_ADMIN})
    @Version("1")
    @Delete("/{id}")
    public void delete(@NotNull Long id) {
    	service.delete(id);
    }
}
