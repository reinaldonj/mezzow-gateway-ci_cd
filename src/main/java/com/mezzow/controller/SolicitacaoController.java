package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.domain.Solicitacao;
import com.mezzow.rest.client.SolicitacaoClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/solicitacao")
@Validated
public class SolicitacaoController {

	private final SolicitacaoClient client;

//	@Inject
//	private SolicitacaoCache cache;
	
    public SolicitacaoController(
    		SolicitacaoClient client) {
    	
        this.client = client;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<Solicitacao> show(@NotNull Long id) {
    	return client.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Produces(MediaType.APPLICATION_JSON)
    @Get("/{?pacienteNome,setorNome,unidadeNome,empresaNome,max,offset,sort,order}")
    public Page<Solicitacao> list(@Nullable String pacienteNome, @Nullable String setorNome, @Nullable String  unidadeNome, @Nullable String  empresaNome, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order){
    	return client.list(pacienteNome, setorNome, unidadeNome, empresaNome, max, offset, sort, order);
    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public Solicitacao create(@Body Solicitacao usuario) {     
    	
//    	try {
    		return client.save(usuario);
//    	}catch (HttpClientResponseException e ) {
//    		return e.getResponse();
//    	}
    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public Solicitacao update(@Body Solicitacao usuario) {
        return client.update(usuario);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public void delete(@NotNull Long id) {
         client.delete(id);
    }

}

