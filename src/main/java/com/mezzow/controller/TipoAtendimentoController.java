
package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.domain.TipoAtendimento;
import com.mezzow.rest.client.TipoAtendimentoClient;
import com.mezzow.rest.domain.TipoAtendimentoResponse;
import com.mezzow.service.TipoAtendimentoService;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;
import io.reactivex.Single;

@Controller(value = "/tipoAtendimento")
@Validated
public class TipoAtendimentoController {

	private final TipoAtendimentoClient client;
	private final TipoAtendimentoService service;
	

    public TipoAtendimentoController(
    		TipoAtendimentoClient client, TipoAtendimentoService service) {
    	
        this.client = client;
        this.service = service;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Single<Optional<TipoAtendimentoResponse>> show(@NotNull Long id) {
    	return service.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{?nomeProduto,nome,descricao,max,offset,sort,order}")
    public Page<TipoAtendimento> list(
    		@Nullable String  nomeProduto, 
    		@Nullable String  nome, 		  
    		@Nullable String  descricao,  
    		@Nullable Integer  max, 
    		@Nullable Integer  offset, 
    		@Nullable String  sort, 
    		@Nullable String  order){
    	
        return service.list(Optional.ofNullable(nomeProduto), Optional.ofNullable(nome), Optional.ofNullable(descricao), Optional.ofNullable(max), Optional.ofNullable(offset), Optional.ofNullable(sort), Optional.ofNullable(order));
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public TipoAtendimento create(@Body TipoAtendimento usuario) {     
    	
//    	try {
    		return client.save(usuario);
//    	}catch (HttpClientResponseException e ) {
//    		return e.getResponse();
//    	}
    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public TipoAtendimento update(@Body TipoAtendimento usuario) {
        return client.update(usuario);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public HttpResponse<?> delete(@NotNull Long id) {
        return client.delete(id);
    }

}
/*
=======
package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.rest.client.TipoAtendimentoClient;
import com.mezzow.rest.domain.TipoAtendimentoResponse;
import com.mezzow.service.TipoAtendimentoService;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;
import io.reactivex.Single;

@Controller(value = "/tipoAtendimento")
@Validated
public class TipoAtendimentoController {

	private final TipoAtendimentoClient client;
	private final TipoAtendimentoService service;

    public TipoAtendimentoController(TipoAtendimentoClient client, TipoAtendimentoService service) {
        this.client = client;
        this.service = service;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Single<Optional<TipoAtendimentoResponse>> show(@NotNull Long id) {
    	return service.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{?nome,descricao,max,offset,sort,order}")
    public Page<TipoAtendimentoResponse> list(
    		@Nullable String nome, 
    		  
    		@Nullable String descricao,  
    		  
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order){
    	
        return service.find(nome, descricao, max, offset, sort, order);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public HttpResponse<?> create(@Body String usuario) {     
    	
    	try {
    		return client.save(usuario);
    	}catch (HttpClientResponseException e ) {
    		return e.getResponse();
    	}
    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public HttpResponse<?> update(@Body String usuario) {
        return client.update(usuario);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public HttpResponse<?> delete(@NotNull Long id) {
        return client.delete(id);
    }

}
>>>>>>> 27371051141e21f819ac457db7a261daf1913a73
*/
