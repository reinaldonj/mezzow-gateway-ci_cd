package com.mezzow.controller;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import com.mezzow.domain.ContratoProduto;
import com.mezzow.domain.TipoConsulta;
import com.mezzow.rest.client.ContratoProdutoClient;
import com.mezzow.rest.client.TipoConsultaClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/tipoConsulta")
@Validated
public class TipoConsultaController {

	private final TipoConsultaClient client;
	
	private final ContratoProdutoClient contratoProdutoClient;

    public TipoConsultaController(
    		TipoConsultaClient client, ContratoProdutoClient contratoProdutoClient) {
    	
        this.client = client;
        this.contratoProdutoClient = contratoProdutoClient;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<TipoConsulta> show(@NotNull Long id) {
    	return client.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{?codigoCliente,grupoEmpresa,descricao,nome,unidadeId,max,offset,sort,order}")
    public Page<TipoConsulta> list(
    		@Nullable String codigoCliente, @Nullable String grupoEmpresa, @Nullable String descricao, @Nullable String nome, @Nullable String unidadeId, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order){
    	
    	List<Long> idsProdutoRetornados = null;
    	
    	if (StringUtils.isNotBlank(unidadeId)) {
    		
    		Page<ContratoProduto> find = contratoProdutoClient.find(null, null, null, Collections.singletonList(unidadeId), max, offset, "dataCadastro", order);
    		
    		idsProdutoRetornados = find.getContent().stream().map(p -> p.getProduto().getId()).collect(Collectors.toList());
    		
    		if (idsProdutoRetornados != null && idsProdutoRetornados.isEmpty()) {
        		idsProdutoRetornados = null;
        	}
    	}

        return client.find(codigoCliente, grupoEmpresa, descricao, nome, idsProdutoRetornados, max, offset, sort, order);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public TipoConsulta create(@Body TipoConsulta usuario) {     
    	
    	
    		return client.save(usuario);

    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public TipoConsulta update(@Body TipoConsulta usuario) {
        return client.update(usuario);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public HttpResponse<?> delete(@NotNull Long id) {
        return client.delete(id);
    }

}
