package com.mezzow.controller;

import java.util.Optional;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import com.mezzow.cache.UnidadeCache;
import com.mezzow.domain.Unidade;
import com.mezzow.rest.client.UnidadeClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/unidade")
@Validated
public class UnidadeController {

	private final UnidadeClient client;

	@Inject
	private UnidadeCache cache;
	
    public UnidadeController(
    		UnidadeClient client) {
    	
        this.client = client;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<Unidade> show(@NotNull Long id) {
    	return client.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Produces(MediaType.APPLICATION_JSON)
    @Get("/{?cnpj,nome,empresa,max,offset,sort,order}")
    public Page<Unidade> list(
    		@Nullable String cnpj, 
    		@Nullable String nome, 
    		@Nullable String empresa, 
    		 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order){
    	
    	System.out.println("**********************************************************************************");
    	
    	System.out.println("**********************************************************************************");
    	System.out.println("**********************************************************************************");
    	System.out.println("**********************************************************************************");
    	System.out.println("**********************************************************************************");
    	System.out.println("**********************************************************************************");
    	System.out.println("**********************************************************************************");
    	
    	System.out.println(cache.getP().size());
    	
    	Page<Unidade> page = null;
//    	page = cache.get(cnpj + nome + empresa + max + offset + sort + order);
    	
    	if (page != null) {
    		System.out.println("RECUPERANDO DO CACHE ->>> **********************************************************************************");
    		return page;
    	}else {
    		System.out.println("CHAMANDO DA BASE  ->>> **********************************************************************************");
    		Page<Unidade> find = client.find(cnpj, nome,empresa,max, offset, sort, order);
//    		cache.addPaciente(cnpj + nome + empresa + max + offset + sort + order, find);
    		return find;
    		
//    		Flowable<Page<Unidade>> flatMap = client.findRx(cnpj, nome,empresa,max, offset, sort, order)
//	    	.subscribeOn(Schedulers.io())
//			.observeOn(Schedulers.io())
//			.timeout(10L, TimeUnit.SECONDS).retry(2l)
//			.flatMap(it -> {
//				
//				return Flowable.just( it.getBody().get() );
//			});
//	    	
//	    	return flatMap.blockingFirst();
    	}
    	
//    	if (cache.getP().size() > 0) {
//    		System.out.println(new Gson().toJson(cache.getP()));
//    	}
//    	
//    	Page<Unidade> page = cache.get(cnpj + nome + empresa + max + offset + sort + order);
//    	
//    	if (page != null) {
//    		return page;
//    	}else {
//    		Page<Unidade> find = client.find(cnpj, nome,empresa,max, offset, sort, order);
//    		cache.addPaciente(cnpj + nome + empresa + max + offset + sort + order, find);
//    		return find;
//    	}
    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public Unidade create(@Body Unidade usuario) {     
    	
//    	try {
    		return client.save(usuario);
//    	}catch (HttpClientResponseException e ) {
//    		return e.getResponse();
//    	}
    	
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public Unidade update(@Body Unidade usuario) {
        return client.update(usuario);
    }
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    public HttpResponse<?> delete(@NotNull Long id) {
    	return client.delete(id);
    }

}

