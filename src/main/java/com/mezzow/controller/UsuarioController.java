package com.mezzow.controller;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.common.APIError;
import com.mezzow.domain.Usuario;
import com.mezzow.rest.client.UsuarioClient;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpResponseFactory;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;

@Controller(value = "/usuario")
@Validated
public class UsuarioController {

	
    private final UsuarioClient client;

    public UsuarioController(
    		UsuarioClient client) {
    	
        this.client = client;
    }
    
    
    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<Usuario> show(@NotNull Long id, HttpRequest<?> request) {
    	return client.show(id);
    }

    
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/{?nome,nomeGrupoEmpresa,nomeEmpresa,nomeUnidade,max,offset,sort,order}")
    @Produces(MediaType.APPLICATION_JSON)
    public Page<Usuario> list(
    		@Nullable String nome, 
    		@Nullable String nomeGrupoEmpresa, 
    		@Nullable String nomeEmpresa, 
    		@Nullable String nomeUnidade, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order) {
    	
        return client.find(nome, nomeGrupoEmpresa, nomeEmpresa, nomeUnidade, max, offset, sort, order);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    @Produces(MediaType.APPLICATION_JSON)
    public HttpResponse<?> create(@Body Usuario usuario) {     
    	
    	try {
    		Usuario userSave = client.save(usuario);
    		return HttpResponseFactory.INSTANCE.status(HttpStatus.OK, userSave);			
		} catch (Exception e) {
			APIError apiError = new APIError();
			apiError.setCode("9001");
			apiError.setMessage("Usuário já cadastrado!");
			apiError.setDetailMessage(e.getMessage());
			return HttpResponseFactory.INSTANCE.ok(apiError);
		}
    	
    		
//    	try {
//    	
//    		
//    		return rxHttpClient.exchange(
//    	        HttpRequest.POST(UsuarioController.PATH, usuario)
//    	        	 .header("Authorization", authorization)
//    	            .contentType(MediaType.APPLICATION_JSON), String.class
//    	    ).blockingFirst();
//    	
//    	}catch (HttpClientResponseException e ) {
//    		return e.getResponse();
//    	}
    	
    	
    	
//    	return Flowable.fromPublisher(
//    			
//    			Publishers.map(
//    			client2.proxy(
//                      request.mutate()
//                              .body(request.getBody())
//              ), response -> response)).blockingFirst();
    	
    	
    	
//    	Publisher<?> map = Publishers.map(
//    			client2.proxy(
//                        request.mutate()
//                                .body(request.getBody())
//                ), response -> response);
//    	
//    	map.subscribe(null);
    	
//        return client.save(usuario);//client3.exchange( HttpRequest.POST("/usuario", o).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON),HttpResponse.class).blockingFirst();
 
        
//	return Flowable.fromPublisher(client.proxy(
//            request.mutate()
//            .body(request.getBody()))).blockingFirst();
//			
//			Publishers.map(
//        		
//                ), response -> response)).blockingFirst();
//        
        
        
        //return null; // HttpResponse.ok(client2.retrieve(request).blockingFirst());
        
        
    	//return client.proxy(request.mutate().body(request.getBody()));
         
    	
//    	return client.save(o);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Put
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario update(@Body Usuario usuario) {
        return client.update(usuario);
    }
    
    /*
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Version("1")
    @Delete("/{id}")
    @Operation(
            summary = "Exclui um tipo de exame"
    )
    @ApiResponse(
            responseCode = "204",
            description = "Tipo de exame excluido com sucesso",
            content = @Content(schema = @Schema(implementation = TipoExame.class))
    )
    @ApiResponse(
            responseCode = "404",
            description = "Modalidade não encontrada",
            content = @Content(schema = @Schema(implementation = Modalidade.class))
    )
    public HttpResponse delete(@NotNull Long id) {
        Optional<Usuario> optional = service.findById(id);

        if (optional.isPresent()) {
        	service.deleteById(id);
        } else {
            return HttpResponse.notFound();
        }

        return HttpResponse.noContent();
    }
    */

//    @Secured(SecurityRule.IS_AUTHENTICATED)
//    @Version("1")
//    @Get("/medicoRadiologista{?max,offset,sort,order}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Page<Usuario> findAllByPermissoesAuthorityEquals(@Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order) {
//
//        return client.findAllByPermissoesAuthorityEquals(max, offset, sort, order);
//        
//    }
}
