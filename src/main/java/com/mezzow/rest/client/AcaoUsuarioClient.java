package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.AcaoUsuario;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface AcaoUsuarioClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/acaoUsuario/{id}")
	Optional<AcaoUsuario> show(Long id);
	
	@Get("/acaoUsuario/{?codigo,descricao,max,offset,sort,order}")
	Page<AcaoUsuario> find(
    		@Nullable String codigo, 
    		@Nullable String descricao, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/acaoUsuario")
	AcaoUsuario save(@Body AcaoUsuario cid);
	
	@Put("/acaoUsuario")
	AcaoUsuario update(@Body AcaoUsuario cid);
	
	@Delete("/acaoUsuario/{id}")
	void delete(Long id);
}
