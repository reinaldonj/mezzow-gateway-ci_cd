package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.AgendaNotificacao;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-notificacao")
@Requires(notEnv = Environment.TEST)
public interface AgendaNotificacaoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/agendaNotificacao/{id}")
	Optional<AgendaNotificacao> show(Long id);
	
	@Get("/agendaNotificacao/{?max,offset,sort,order}")
	Page<AgendaNotificacao> find(
			@Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order);
	
	@Post("/agendaNotificacao")
	AgendaNotificacao save(@Body AgendaNotificacao cid);
	
	@Put("/agendaNotificacao")
	AgendaNotificacao update(@Body AgendaNotificacao cid);
	
	@Delete("/agendaNotificacao/{id}")
	void delete(Long id);
}
