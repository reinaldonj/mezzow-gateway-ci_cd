package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.Agenda;
import com.mezzow.domain.Agendamento;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Client(id = "mezzow-atendimento")
@Requires(notEnv = Environment.TEST)
public interface AgendamentoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/agendamento/{id}")
	Optional<Agendamento> show(Long id);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/agendamento/{id}")
	Flowable<HttpResponse<Optional<Agendamento>>> showRx(Long id);
	
	
	@Get("/agendamento/byIds/{idsAgendamento}")
	List<Agendamento> find(
			@Nullable List<Long> idsAgendamento);
	
	@Get("/agendamento/{?idsPaciente,unidadeId,dataDe,dataAte,listaStatus,max,offset,sort,order}")
	Page<Agendamento> find(
			@Nullable List<Long> idsPaciente,
    		@Nullable Long unidadeId,
    		@Nullable String dataDe,
    		@Nullable String dataAte,
    		@Nullable List<Long> listaStatus,
    		@Nullable Integer max,
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Get("/agendamento/{?idsPaciente,unidadeId,dataDe,dataAte,listaStatus,max,offset,sort,order}")
	Page<Agendamento> find(
			@Nullable List<Long> idsPaciente,
    		@Nullable List<Long> unidadeId,
    		@Nullable String dataDe,
    		@Nullable String dataAte,
    		@Nullable List<Long> listaStatus,
    		@Nullable Integer max,
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Get("/agendamento/{?idsPaciente,unidadeId,dataDe,dataAte,listaStatus,max,offset,sort,order}")
	Flowable<HttpResponse<Page<Agendamento>>> findRx(
			@Nullable List<Long> idsPaciente,
    		@Nullable List<Long> unidadeId,
    		@Nullable String dataDe,
    		@Nullable String dataAte,
    		@Nullable List<Long> listaStatus,
    		@Nullable Integer max,
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/agendamento")
	Agendamento save(@Body Agendamento cid);
	
	@Put("/agendamento")
	Agendamento update(@Body Agendamento cid);
	
	@Delete("/agendamento/{id}")
    void delete(Long id);
	
	@Get("/agendamento/byIds/{idsAgendamento}")
	List<Agendamento> byIds(List<Long> idsAgendamento);

	@Get("/agendamento/agenda/{dataReferencia}")
	List<Agenda> obterAgendas(@PathVariable @Nullable String dataReferencia);
}
