package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.Atendimento;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Client(id = "mezzow-atendimento")
@Requires(notEnv = Environment.TEST)
public interface AtendimentoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/atendimento/{id}")
	Optional<Atendimento> show(Long id);
	
	
	@Get("/atendimento/{?grupoEmpresa,empresa,unidade,setor,medico,tipoConsulta,dataInicial,dataFinal,status,max,offset,sort,order}")
	Page<Atendimento> find(
			@Nullable Long grupoEmpresa,
    		@Nullable Long empresa,
    		@Nullable List<Long> unidade,
    		@Nullable List<Long> setor,
    		@Nullable List<String> medico,
    		@Nullable List<Long> tipoConsulta,
    		@Nullable String dataInicial,
    		@Nullable String dataFinal,
    		@Nullable List<Long> status,
    		@Nullable Integer max,
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Get("/atendimento/{?grupoEmpresa,empresa,unidade,setor,medico,tipoConsulta,dataInicial,dataFinal,status,max,offset,sort,order}")
	Flowable<Page<Atendimento>> findRx(
			@Nullable Long grupoEmpresa,
    		@Nullable Long empresa,
    		@Nullable List<Long> unidade,
    		@Nullable List<Long> setor,
    		@Nullable List<String> medico,
    		@Nullable List<Long> tipoConsulta,
    		@Nullable String dataInicial,
    		@Nullable String dataFinal,
    		@Nullable List<Long> status,
    		@Nullable Integer max,
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Get("/atendimento/{?grupoEmpresa,empresa,unidade,setor,medico,tipoConsulta,dataInicial,dataFinal,status,max,offset,sort,order}")
	Flowable<HttpResponse<Page<Atendimento>>> findRxH(
			@Nullable Long grupoEmpresa,
    		@Nullable Long empresa,
    		@Nullable List<Long> unidade,
    		@Nullable List<Long> setor,
    		@Nullable List<String> medico,
    		@Nullable List<Long> tipoConsulta,
    		@Nullable String dataInicial,
    		@Nullable String dataFinal,
    		@Nullable List<Long> status,
    		@Nullable Integer max,
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Put("/atendimento")
	Atendimento update(@Body Atendimento cid);
	
	@Post("/atendimento")
	Atendimento save(@Body Atendimento cid);

	@Get("/atendimento/byRegistroPreAtendimento/{ids}")
	List<Atendimento> findByRegistroPreAtendimentoIdInList(List<Long> ids);

	
}
