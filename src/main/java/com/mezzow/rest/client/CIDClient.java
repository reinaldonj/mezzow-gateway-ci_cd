package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.Cid;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface CIDClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/cid/{id}")
	Optional<Cid> show(Long id);
	
	@Get("/cid/{?codigo,descricao,max,offset,sort,order}")
	Page<Cid> find(
    		@Nullable String codigo, 
    		@Nullable String descricao, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/cid")
	Cid save(@Body Cid cid);
	
	@Put("/cid")
	Cid update(@Body Cid cid);
	
	@Delete("/cid/{id}")
	HttpResponse<?> delete(Long id);
}
