package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.ContratoProduto;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Client(id = "mezzow-produto")
@Requires(notEnv = Environment.TEST)
public interface ContratoProdutoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/contratoProduto/{id}")
	Optional<ContratoProduto> show(Long id);
	
//	@Get("/contratoProduto/{?idsProduto,nomeGrupoContratoProduto,nomeContratoProduto,nomeUnidade,max,offset,sort,order}")
//	Page<ContratoProduto> find(
//			@Nullable List<String> idsProduto, 
//    		@Nullable String nomeGrupoContratoProduto, 
//    		@Nullable String nomeContratoProduto, 
//    		@Nullable String nomeUnidade,  
//    		@Nullable Integer max, 
//    		@Nullable Integer offset, 
//    		@Nullable String sort, 
//    		@Nullable String order);
	
	@Get("/contratoProduto/{?nomeProduto,idsGrupoEmpresa,idsEmpresa,idsUnidade,max,offset,sort,order}")
	Flowable<Page<ContratoProduto>>  findRx(
			@Nullable String nomeProduto, 
			@Nullable List<String> idsGrupoEmpresa, 
			@Nullable List<String> idsEmpresa, 
			@Nullable List<String> idsUnidade,  
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Get("/contratoProduto/{?nomeProduto,idsGrupoEmpresa,idsEmpresa,idsUnidade,max,offset,sort,order}")
	Flowable<HttpResponse<Page<ContratoProduto>>>  findRxH(
			@Nullable String nomeProduto, 
			@Nullable List<String> idsGrupoEmpresa, 
			@Nullable List<String> idsEmpresa, 
			@Nullable List<String> idsUnidade,  
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Get("/contratoProduto/{?nomeProduto,idsGrupoEmpresa,idsEmpresa,idsUnidade,max,offset,sort,order}")
	Page<ContratoProduto>  find(
			@Nullable String nomeProduto, 
			@Nullable List<String> idsGrupoEmpresa, 
			@Nullable List<String> idsEmpresa, 
			@Nullable List<String> idsUnidade,  
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/contratoProduto")
	ContratoProduto save(@Body ContratoProduto contratoProduto);
	
	@Put("/contratoProduto")
	ContratoProduto update(@Body ContratoProduto contratoProduto);
	
	@Delete("/contratoProduto/{id}")
	HttpResponse<?> delete(Long id);
}
