package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.Convenio;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface ConvenioClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/convenio/{id}")
	Optional<Convenio> show(Long id);
	
	@Get("/convenio/{?descricao,codigoAns,unidade,codigoCliente,max,offset,sort,order}")
	Page<Convenio> find(
    		@Nullable String descricao, 
    		@Nullable String codigoAns, 
    		@Nullable String unidade, 
    		@Nullable String codigoCliente,
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/convenio")
	Convenio save(@Body Convenio cid);
	
	@Put("/convenio")
	Convenio update(@Body Convenio cid);
	
	@Delete("/convenio/{id}")
	HttpResponse<?> delete(Long id);
}
