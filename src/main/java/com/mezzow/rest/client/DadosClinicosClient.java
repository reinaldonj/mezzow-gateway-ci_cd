package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.DadosClinicos;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface DadosClinicosClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/dadosClinicos/{id}")
	Optional<DadosClinicos> show(Long id);
	
	@Get("/dadosClinicos/{?titulo,grupo,unidade,max,offset,sort,order}")
	Page<DadosClinicos> find(
    		@Nullable String titulo, 
    		@Nullable String grupo, 
    		@Nullable String unidade, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/dadosClinicos")
	DadosClinicos save(@Body DadosClinicos cid);
	
	@Put("/dadosClinicos")
	DadosClinicos update(@Body DadosClinicos cid);
	
	@Delete("/dadosClinicos/{id}")
	void delete(Long id);
}
