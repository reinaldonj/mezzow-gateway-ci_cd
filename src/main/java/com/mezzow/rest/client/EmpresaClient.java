package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.Empresa;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)

public interface EmpresaClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/empresa/{id}")
	Optional<Empresa> show(Long id);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/empresa/byNome/{nome}")
	Flowable<List<Empresa>> findByNome(String nome);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/empresa/byNome/{nome}")
	Flowable<HttpResponse<List<Empresa>>> findByNomeRxH(String nome);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/empresa/byId/{valores}")
	Flowable<List<Empresa>> findByIdInListRx(List<String> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/empresa/byId/{valores}")
	Flowable<HttpResponse<List<Empresa>>> findByIdInListRxH(List<String> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/empresa/byId/{valores}")
	List<Empresa> findByIdInList(List<String> valores);
	
	@Get("/empresa/{?nome,cnpj,grupo,situacao,max,offset,sort,order}")
	Page<Empresa> find(
			@Nullable String nome, @Nullable String cnpj, @Nullable String grupo, @Nullable Boolean situacao, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order);
	
	@Post("/empresa")
	Empresa save(@Body Empresa cid);
	
	@Put("/empresa")
	Empresa update(@Body Empresa cid);
	
	@Delete("/empresa/{id}")
	HttpResponse<?> delete(Long id);
}
