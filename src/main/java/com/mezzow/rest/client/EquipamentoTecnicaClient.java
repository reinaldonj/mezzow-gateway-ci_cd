package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.EquipamentoTecnica;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface EquipamentoTecnicaClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/equipamentoTecnica/{id}")
	Optional<EquipamentoTecnica> show(Long id);
	
	@Get("/equipamentoTecnica/{?titulo,modalidade,unidade,max,offset,sort,order}")
	Page<EquipamentoTecnica> find(
    		@Nullable String titulo, 
    		@Nullable String modalidade, 
    		@Nullable String unidade, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/equipamentoTecnica")
	EquipamentoTecnica save(@Body EquipamentoTecnica cid);
	
	@Put("/equipamentoTecnica")
	EquipamentoTecnica update(@Body EquipamentoTecnica cid);
	
	@Delete("/equipamentoTecnica/{id}")
	void delete(Long id);
}
