package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.Especialidade;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface EspecialidadeClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/especialidade/{id}")
	Optional<Especialidade> show(Long id);
	
	@Get("/especialidade/{?descricao,max,offset,sort,order}")
	Page<Especialidade> find(
    		@Nullable String descricao, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/especialidade")
	Especialidade save(@Body Especialidade cid);
	
	@Put("/especialidade")
	Especialidade update(@Body Especialidade cid);
	
	@Delete("/especialidade/{id}")
	HttpResponse<?> delete(Long id);
}
