package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.Estado;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface EstadoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/estado/{id}")
	Optional<Estado> show(Long id);
	
	@Get("/estado/{?descricao,max,offset,sort,order}")
	Page<Estado> find(
    		@Nullable String descricao, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/estado")
	Estado save(@Body Estado cid);
	
	@Put("/estado")
	Estado update(@Body Estado cid);
	
	@Delete("/estado/{id}")
	Estado delete(Long id);
}
