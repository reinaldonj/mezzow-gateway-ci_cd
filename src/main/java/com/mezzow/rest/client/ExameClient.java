package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.Exame;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-exame")
@Requires(notEnv = Environment.TEST)

public interface ExameClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/exame/{id}")
	Optional<Exame> show(Long id);
	
	
	@Get("/exame/{?solicitacaoid}")
	Page<Exame> find(@Nullable Long solicitacaoid);
	
	@Post("/exame")
	Exame save(@Body Exame cid);
	
	@Put("/exame")
	Exame update(@Body Exame cid);
	
	@Delete("/exame/{id}")
	void delete(Long id);
}
