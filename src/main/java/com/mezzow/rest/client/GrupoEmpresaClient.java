
package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.GrupoEmpresa;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface GrupoEmpresaClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/grupoEmpresa/{id}")
	Optional<GrupoEmpresa> show(Long id);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/grupoEmpresa/byNome/{nome}")
	Flowable<List<GrupoEmpresa>> findByNome(String nome);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/grupoEmpresa/byNome/{nome}")
	Flowable<HttpResponse<List<GrupoEmpresa>>> findByNomeRxH(String nome);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/grupoEmpresa/byId/{valores}")
	Flowable<List<GrupoEmpresa>> findByIdInListRx(List<String> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/grupoEmpresa/byId/{valores}")
	Flowable<HttpResponse<List<GrupoEmpresa>>> findByIdInListRxH(List<String> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/grupoEmpresa/byId/{valores}")
	List<GrupoEmpresa> findByIdInList(List<String> valores);
	
	@Get("/grupoEmpresa/{?nome,descricao,max,offset,sort,order}")
	Page<GrupoEmpresa> find(
    		@Nullable String nome, 
    		@Nullable String descricao, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/grupoEmpresa")
	GrupoEmpresa save(@Body String cid);
	
	@Put("/grupoEmpresa")
	GrupoEmpresa update(@Body String cid);
	
	@Delete("/grupoEmpresa/{id}")
	HttpResponse<?> delete(Long id);
}

