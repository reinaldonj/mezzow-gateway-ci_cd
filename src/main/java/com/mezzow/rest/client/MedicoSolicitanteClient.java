package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.MedicoSolicitante;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-exame")
@Requires(notEnv = Environment.TEST)
public interface MedicoSolicitanteClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/medicoSolicitante/{id}")
	Optional<MedicoSolicitante> show(Long id);
	
	@Get("/medicoSolicitante/{?nome,registroConselho,max,offset,sort,order}")
	Page<MedicoSolicitante> find(
    		@Nullable String nome, 
    		@Nullable String registroConselho, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/medicoSolicitante")
	MedicoSolicitante save(@Body MedicoSolicitante medicoSolicitante);
	
	@Put("/medicoSolicitante")
	MedicoSolicitante update(@Body MedicoSolicitante medicoSolicitante);
	
	@Delete("/medicoSolicitante/{id}")
	void delete(Long id);
}
