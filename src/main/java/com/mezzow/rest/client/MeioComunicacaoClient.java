package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.MeioComunicacao;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-notificacao")
@Requires(notEnv = Environment.TEST)
public interface MeioComunicacaoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/meioComunicacao/{id}")
	Optional<MeioComunicacao> show(Long id);
	
	@Get("/meioComunicacao/{?codigo,descricao,max,offset,sort,order}")
	Page<MeioComunicacao> find(
			@Nullable String codigo, @Nullable String descricao, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order);
	
	@Post("/meioComunicacao")
	MeioComunicacao save(@Body MeioComunicacao cid);
	
	@Put("/meioComunicacao")
	MeioComunicacao update(@Body MeioComunicacao cid);
	
	@Delete("/meioComunicacao/{id}")
	void delete(Long id);
}
