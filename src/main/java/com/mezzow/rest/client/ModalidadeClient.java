package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.Modalidade;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface ModalidadeClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/modalidade/{id}")
	Optional<Modalidade> show(Long id);
	
	@Get("/modalidade/{?sigla,nome,max,offset,sort,order}")
	Page<Modalidade>find(
    		@Nullable String sigla, 
    		@Nullable String nome,  
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/modalidade")
	Modalidade save(@Body Modalidade cid);
	
	@Put("/modalidade")
	Modalidade update(@Body Modalidade cid);
	
	@Delete("/modalidade/{id}")
	HttpResponse<?> delete(Long id);
}
