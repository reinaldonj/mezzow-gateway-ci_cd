package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.ModeloConclusao;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface ModeloConclusaoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/modeloConclusao/{id}")
	Optional<ModeloConclusao> show(Long id);
	
	@Get("/modeloConclusao/{?grupoEmpresa,unidade,tipoExame,titulo,max,offset,sort,order}")
	Page<ModeloConclusao> find(
			@Nullable String grupoEmpresa, @Nullable String unidade,
			@Nullable String tipoExame, @Nullable String titulo, @Nullable Integer max, @Nullable Integer offset,
			@Nullable String sort, @Nullable String order);
	
	@Post("/modeloConclusao")
	ModeloConclusao save(@Body ModeloConclusao cid);
	
	@Put("/modeloConclusao")
	ModeloConclusao update(@Body ModeloConclusao cid);
	
	@Delete("/modeloConclusao/{id}")
	void delete(Long id);
}
