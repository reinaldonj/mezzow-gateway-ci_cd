package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.ModeloContraste;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface ModeloContrasteClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/modeloContraste/{id}")
	Optional<ModeloContraste> show(Long id);
	
	@Get("/modeloContraste/{?titulo,nomeGrupoEmpresa,nomeUnidade,nomeModalidade,max,offset,sort,order}")
	Page<ModeloContraste> find(
					@Nullable String titulo, @Nullable String nomeGrupoEmpresa,
					@Nullable String nomeUnidade, @Nullable String nomeModalidade, @Nullable Integer max, @Nullable Integer offset,
					@Nullable String sort, @Nullable String order);
	
	@Post("/modeloContraste")
	ModeloContraste save(@Body ModeloContraste cid);
	
	@Put("/modeloContraste")
	ModeloContraste update(@Body ModeloContraste cid);
	
	@Delete("/modeloContraste/{id}")
	void delete(Long id);
}
