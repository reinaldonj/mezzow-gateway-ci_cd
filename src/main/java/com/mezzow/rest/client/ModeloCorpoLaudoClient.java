package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.ModeloCorpoLaudo;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface ModeloCorpoLaudoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/modeloCorpoLaudo/{id}")
	Optional<ModeloCorpoLaudo> show(Long id);
	
	@Get("/modeloCorpoLaudo/{?grupoEmpresa,unidade,tipoExame,titulo,max,offset,sort,order}")
	Page<ModeloCorpoLaudo> find(
    		@Nullable String grupoEmpresa, 
    		@Nullable String unidade, 
    		@Nullable String tipoExame, 
    		@Nullable String titulo, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/modeloCorpoLaudo")
	ModeloCorpoLaudo save(@Body ModeloCorpoLaudo cid);
	
	@Put("/modeloCorpoLaudo")
	ModeloCorpoLaudo update(@Body ModeloCorpoLaudo cid);
	
	@Delete("/modeloCorpoLaudo/{id}")
	void delete(Long id);
}
