package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.mezzow.domain.ModeloNotificacao;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-notificacao")
@Requires(notEnv = Environment.TEST)
public interface ModeloNotificacaoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/modeloNotificacao/{id}")
	Optional<ModeloNotificacao> show(Long id);

	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/modeloNotificacao/findByUnidadeIdAndTipoNotificacaoId{?unidadeId,tipoNotificacaoId}")
	List<ModeloNotificacao> findAllByUnidadeIdAndTipoNotificacaoId(@NotNull Long unidadeId, @NotNull Long tipoNotificacaoId);

	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/modeloNotificacao/findByGrupoEmpresaIdAndTipoNotificacaoId{?grupoEmpresaId,tipoNotificacaoId}")
	List<ModeloNotificacao> findAllByGrupoEmpresaIdAndTipoNotificacaoId(@NotNull Long grupoEmpresaId, @NotNull Long tipoNotificacaoId);

	@Get("/modeloNotificacao/{?grupoEmpresa,unidade,tipoNotificacao,titulo,max,offset,sort,order}")
	Page<ModeloNotificacao> find(
			@Nullable List<Long> grupoEmpresa,
			@Nullable List<Long> unidade,
			@Nullable String tipoNotificacao,
			@Nullable String titulo,
			@Nullable Integer max,
			@Nullable Integer offset,
			@Nullable String sort,
			@Nullable String order);
	
	@Post("/modeloNotificacao")
	ModeloNotificacao save(@Body ModeloNotificacao cid);
	
	@Put("/modeloNotificacao")
	ModeloNotificacao update(@Body ModeloNotificacao cid);
	
	@Delete("/modeloNotificacao/{id}")
	HttpResponse<?> delete(Long id);
}
