package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.ModeloPosConsulta;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface ModeloPosConsultaClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/modeloPosConsulta/{id}")
	Optional<ModeloPosConsulta> show(Long id);
	
	@Get("/modeloPosConsulta/{?nomeGrupoEmpresa,nomeUnidade,nomeTipoConsulta,titulo,max,offset,sort,order}")
	Page<ModeloPosConsulta> find(
			@Nullable String nomeGrupoEmpresa, @Nullable String nomeUnidade,
			@Nullable String nomeTipoConsulta, @Nullable String titulo, @Nullable Integer max, @Nullable Integer offset,
			@Nullable String sort, @Nullable String order);
	
	@Post("/modeloPosConsulta")
	ModeloPosConsulta save(@Body ModeloPosConsulta modeloPosConsulta);
	
	@Put("/modeloPosConsulta")
	ModeloPosConsulta update(@Body ModeloPosConsulta modeloPosConsulta);
	
	@Delete("/modeloPosConsulta/{id}")
	HttpResponse delete(Long id);
	
	@Get("/modeloPosConsulta/byId/{valores}")
    @Produces(MediaType.APPLICATION_JSON)
    Flowable<HttpResponse<List<ModeloPosConsulta>>>  findByIdInListRxH(List<Long> valores);
}
