package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.MotivoCancelamento;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-atendimento")
@Requires(notEnv = Environment.TEST)
public interface MotivoCancelamentoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/motivoCancelamento/{id}")
	Optional<MotivoCancelamento> show(Long id);
	
	@Get("/motivoCancelamento/{?descricao,max,offset,sort,order}")
	Page<MotivoCancelamento> find(
			@Nullable String descricao, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order
	);
	
	@Post("/motivoCancelamento")
	MotivoCancelamento save(@Body MotivoCancelamento cid);
	
	@Put("/motivoCancelamento")
	MotivoCancelamento update(@Body MotivoCancelamento cid);
	
	@Delete("/motivoCancelamento/{id}")
	void delete(Long id);
}
