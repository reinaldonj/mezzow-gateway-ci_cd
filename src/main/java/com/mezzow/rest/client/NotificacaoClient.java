package com.mezzow.rest.client;

import com.mezzow.domain.Notificacao;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-util")
@Requires(notEnv = Environment.TEST)
public interface NotificacaoClient {
	 
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Get("/notificacao/{id}")
//	Optional<Notificacao> show(Long id);
//	
//	@Get("/notificacao/{?max,offset,sort,order}")
//	Page<Notificacao> find(
//			@Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order);
	
	@Post("/notificacao")
	Notificacao save(@Body Notificacao notificacao);
	
//	@Put("/notificacao")
//	Notificacao update(@Body Notificacao notificacao);
//	
//	@Delete("/notificacao/{id}")
//	Notificacao delete(Long id);
}
