package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.Paciente;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Client(id = "mezzow-exame")
@Requires(notEnv = Environment.TEST)
public interface PacienteClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/paciente/{id}")
	Optional<Paciente> show(Long id);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/paciente/{id}")
	Flowable<HttpResponse<Optional<Paciente>>> showRx(Long id);
	
	@Get("/paciente/{?unidade,prontuario,nome,cpf,patientId,max,offset,sort,order}")
	Page<Paciente> find(
    		@Nullable String unidade, 
    		@Nullable String prontuario, 
    		@Nullable String nome, 
    		@Nullable String cpf, 
    		@Nullable String patientId, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Get("/paciente/byNome/{nome}")
	List<Paciente> byNome(@Nullable String nome);
	
	@Get("/paciente/byCpf/{cpf}")
	List<Paciente> byCpf(@Nullable String cpf);
	
	@Post("/paciente")
	Paciente save(@Body Paciente paciente);
	
	@Put("/paciente")
	Paciente update(@Body Paciente paciente);
	
	@Delete("/paciente/{id}")
	void delete(Long id);
	
	@Get("/paciente/byIds/{ids}")
	List<Paciente> byIds(@PathVariable @Nullable List<Long> ids);
	
	@Get("/paciente/byIds/{ids}")
	Flowable<HttpResponse<List<Paciente>>> byIdsRx(@PathVariable @Nullable List<Long> ids);
	
	@Get("/paciente/list")
	Flowable<HttpResponse<List<Paciente>>> listByParametrosCombinados(@QueryValue @Nullable Optional<String> nome, @QueryValue @Nullable Optional<String> cpf);
}
