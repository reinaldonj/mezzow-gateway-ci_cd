package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.Perfil;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Client(id = "mezzow-seguranca")
@Requires(notEnv = Environment.TEST)
public interface PerfilClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/perfil/{id}")
	Optional<Perfil> show(String id);
	
	@Get("/perfil/{?authority,max,offset,sort,order}")
	Page<Perfil> find(
			@Nullable String authority,
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);

	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/perfil/listaId/{listaId}")
	List<Perfil> findByIds(List<String> listaId);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/perfil/listaId/{listaId}")
	Flowable<HttpResponse<List<Perfil>>> findByIdsRxH(List<String> listaId);

	@Put("/perfil")
	Perfil update(@Body Perfil perfilMedico);
}
