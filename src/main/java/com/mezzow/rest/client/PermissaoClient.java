package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.Permissao;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface PermissaoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/permissao/{id}")
	Optional<Permissao> show(Long id);
	
	@Get("/permissao/{?authority,descricao,max,offset,sort,order}")
	Page<Permissao> find(
    		@Nullable String authority, 
    		@Nullable String descricao,  
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/permissao")
	Permissao save(@Body Permissao cid);
	
	@Put("/permissao")
	Permissao update(@Body Permissao cid);
	
	@Delete("/permissao/{id}")
	void delete(Long id);
}
