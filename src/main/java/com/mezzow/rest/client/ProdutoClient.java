package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.Produto;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-produto")
@Requires(notEnv = Environment.TEST)
public interface ProdutoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/produto/{id}")
	Optional<Produto> show(Long id);
	
	@Get("/produto/{?nome,descricao,max,offset,sort,order}")
	Page<Produto> find(
			@Nullable String nome, 
			@Nullable String descricao, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Get("/produto/byNome/{nome}")
	List<Produto> findByNome(@PathVariable String nome);
	
	@Post("/produto")
	Produto save(@Body Produto produto);
	
	@Put("/produto")
	Produto update(@Body Produto produto);
	
	@Delete("/produto/{id}")
	HttpResponse<?> delete(Long id);
}
