package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.ProtocoloExame;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface ProtocoloExameClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/protocoloExame/{id}")
	Optional<ProtocoloExame> show(Long id);
	
	@Get("/protocoloExame/{?nomeUnidade,nome,nomeModalidade,nomeTipoExame,max,offset,sort,order}")
	Page<ProtocoloExame> find(
    		@Nullable String nomeUnidade, 
    		@Nullable String nome, 
    		@Nullable String nomeModalidade, 
    		@Nullable String nomeTipoExame, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/protocoloExame")
	ProtocoloExame save(@Body ProtocoloExame cid);
	
	@Put("/protocoloExame")
	ProtocoloExame update(@Body ProtocoloExame cid);
	
	@Delete("/protocoloExame/{id}")
	void delete(Long id);
}
