package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.RegistroContato;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-atendimento")
@Requires(notEnv = Environment.TEST)
public interface RegistroContatoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/registroContato/{id}")
	Optional<RegistroContato> show(Long id);
	
	@Get("/registroContato/{?observacao,max,offset,sort,order}")
	Page<RegistroContato> find(
			@Nullable String observacao, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order);
	
	@Post("/registroContato")
	RegistroContato save(@Body RegistroContato registroContato);
	
	@Put("/registroContato")
	RegistroContato update(@Body RegistroContato registroContato);
	
	@Delete("/registroContato/{id}")
	void delete(Long id);
}
