package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.RegistroPosConsulta;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-atendimento")
@Requires(notEnv = Environment.TEST)
public interface RegistroPosConsultaClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/registroPosConsulta/{id}")
	Optional<RegistroPosConsulta> show(Long id);
	
	@Get("/registroPosConsulta/{?titulo,conteudo,modeloPosConsultaId,max,offset,sort,order}")
	Page<RegistroPosConsulta> find(
			@Nullable String titulo, @Nullable String conteudo, @Nullable Long modeloPosConsultaId, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order);
	
	@Post("/registroPosConsulta")
	RegistroPosConsulta save(@Body RegistroPosConsulta registroPosConsulta);
	
	@Put("/registroPosConsulta")
	RegistroPosConsulta update(@Body RegistroPosConsulta registroPosConsulta);
	
	@Delete("/registroPosConsulta/{id}")
	HttpResponse<?> delete(Long id);
}
