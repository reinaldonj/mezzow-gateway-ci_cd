package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.RegistroPreAtendimento;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Client(id = "mezzow-atendimento")
@Requires(notEnv = Environment.TEST)
public interface RegistroPreAtendimentoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/registroPreAtendimento/{id}")
	Optional<RegistroPreAtendimento> show(Long id);
	
	@Get("/registroPreAtendimento{?agendamentoId,perfilId,observacao,max,offset,sort,order}")
	Page<RegistroPreAtendimento> find(
			@Nullable Long agendamentoId,
			@Nullable String perfilId, 
			@Nullable String observacao, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/registroPreAtendimento")
	RegistroPreAtendimento save(@Body RegistroPreAtendimento registroPreAtendimento);
	
	@Put("/registroPreAtendimento")
	RegistroPreAtendimento update(@Body RegistroPreAtendimento registroPreAtendimento);
	
	@Delete("/registroPreAtendimento/{id}")
	void delete(Long id);

	@Get("/registroPreAtendimento/listaId/{ids}")
	List<RegistroPreAtendimento> findByIds(List<Long> ids);
	
	@Get("/registroPreAtendimento/findCancelados")
	public Flowable<HttpResponse<List<RegistroPreAtendimento>>> findAgendamentosCanceladosRxH(
    		@QueryValue @Nullable List<Long> unidades,
    		@QueryValue @Nullable String dataDe,
    		@QueryValue @Nullable String dataAte);
}
