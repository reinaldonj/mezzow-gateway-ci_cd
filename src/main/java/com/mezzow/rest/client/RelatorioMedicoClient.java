package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.RelatorioMedico;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface RelatorioMedicoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/relatorioMedico/{id}")
	Optional<RelatorioMedico> show(Long id);
	
	@Get("/relatorioMedico/{?nomeGrupoEmpresa,nomeUnidade,nomeTipoConsulta,titulo,max,offset,sort,order}")
	Page<RelatorioMedico> find(
			@Nullable String nomeGrupoEmpresa, @Nullable String nomeUnidade,
			@Nullable String nomeTipoConsulta, @Nullable String titulo, @Nullable Integer max, @Nullable Integer offset,
			@Nullable String sort, @Nullable String order);
	
	@Post("/relatorioMedico")
	RelatorioMedico save(@Body RelatorioMedico relatorioMedico);
	
	@Put("/relatorioMedico")
	RelatorioMedico update(@Body RelatorioMedico relatorioMedico);
	
	@Delete("/relatorioMedico/{id}")
	HttpResponse<?> delete(Long id);
}
