package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.RespostaContato;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-atendimento")
@Requires(notEnv = Environment.TEST)
public interface RespostaContatoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/respostaContato/{id}")
	Optional<RespostaContato> show(Long id);
	
	@Get("/respostaContato/{?descricao,max,offset,sort,order}")
	Page<RespostaContato> list(@Nullable String descricao, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order);
	
	@Post("/respostaContato")
	RespostaContato save(@Body RespostaContato respostaContato);
	
	@Put("/respostaContato")
	RespostaContato update(@Body RespostaContato respostaContato);
	
	@Delete("/respostaContato/{id}")
	void delete(Long id);
}

