package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.Setor;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface SetorClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/setor/{id}")
	Optional<Setor> show(Long id);
	
	@Get("/setor/{?nome,unidade,max,offset,sort,order}")
	Page<Setor> find(
    		@Nullable String nome, 
    		@Nullable String unidade, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/setor")
	Setor save(@Body Setor cid);
	
	@Put("/setor")
	Setor update(@Body Setor cid);
	
	@Delete("/setor/{id}")
	HttpResponse<?> delete(Long id);
}
