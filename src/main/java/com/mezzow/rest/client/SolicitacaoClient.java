package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.Solicitacao;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-exame")
@Requires(notEnv = Environment.TEST)
public interface SolicitacaoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/solicitacao/{id}")
	Optional<Solicitacao> show(Long id);
	
	@Get("/solicitacao/{?pacienteNome,setorNome,unidadeNome,empresaNome,max,offset,sort,order}")
	Page<Solicitacao> list(@Nullable String pacienteNome, @Nullable String setorNome, @Nullable String  unidadeNome, @Nullable String  empresaNome, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order);
	
	@Post("/solicitacao")
	Solicitacao save(@Body Solicitacao cid);
	
	@Put("/solicitacao")
	Solicitacao update(@Body Solicitacao cid);
	
	@Delete("/solicitacao/{id}")
	void delete(Long id);
}
