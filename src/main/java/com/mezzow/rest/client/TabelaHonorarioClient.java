package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.TabelaHonorario;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface TabelaHonorarioClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/tabelaHonorario/{id}")
	Optional<TabelaHonorario> show(Long id);
	
	@Get("/tabelaHonorario/{?codigo,tipoExame,convenio,descricao,max,offset,sort,order}")
	Page<TabelaHonorario> find(
			@Nullable String codigo, 
			@Nullable String tipoExame, 
			@Nullable String convenio, 
			@Nullable String descricao,
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/tabelaHonorario")
	TabelaHonorario save(@Body TabelaHonorario cid);
	
	@Put("/tabelaHonorario")
	TabelaHonorario update(@Body TabelaHonorario cid);
	
	@Delete("/tabelaHonorario/{id}")
	void delete(Long id);
}
