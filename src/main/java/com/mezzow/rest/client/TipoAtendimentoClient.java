
package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.TipoAtendimento;
import com.mezzow.domain.TipoConsulta;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface TipoAtendimentoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/tipoAtendimento/{id}")
	Optional<TipoAtendimento> show(Long id);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/tipoAtendimento/{id}")
	Flowable<HttpResponse<Optional<TipoAtendimento>>> showRx(Long id);
	
	@Get("/tipoAtendimento/{?idsProduto,nome,descricao,max,offset,sort,order}")
	Page<TipoAtendimento> find(
			@Nullable List<String> idsProduto, 
    		@Nullable String nome, 
    		@Nullable String descricao,  
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/tipoAtendimento/byId/{valores}")
	Flowable<HttpResponse<List<TipoAtendimento>>> findByIdInListRx(List<Long> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/tipoAtendimento/byId/{valores}")
	List<TipoAtendimento> findByIdInList(List<Long> valores);
	
	@Post("/tipoAtendimento")
	TipoAtendimento save(@Body TipoAtendimento cid);
	
	@Put("/tipoAtendimento")
	TipoAtendimento update(@Body TipoAtendimento cid);
	
	@Delete("/tipoAtendimento/{id}")
	HttpResponse<?> delete(Long id);
}
