package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.TipoConsulta;
import com.mezzow.domain.Unidade;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface TipoConsultaClient {
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/tipoConsulta/{id}")
	Flowable<HttpResponse<Optional<TipoConsulta>>> showRx(Long id);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/tipoConsulta/{id}")
	Optional<TipoConsulta> show(Long id);
	
	@Get("/tipoConsulta/{?codigoCliente,grupoEmpresa,descricao,nome,listaProdutosId,max,offset,sort,order}")
	Page<TipoConsulta> find(
			@Nullable String codigoCliente, @Nullable String grupoEmpresa, @Nullable String descricao, @Nullable String nome, @Nullable List<Long> listaProdutosId, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order);
	
	@Post("/tipoConsulta")
	TipoConsulta save(@Body TipoConsulta cid);
	
	@Put("/tipoConsulta")
	TipoConsulta update(@Body TipoConsulta cid);
	
	@Delete("/tipoConsulta/{id}")
	HttpResponse<?> delete(Long id);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/tipoConsulta/byId/{valores}")
	Flowable<HttpResponse<List<TipoConsulta>>> findByIdInListRx(List<Long> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/tipoConsulta/byId/{valores}")
	List<TipoConsulta> findByIdInList(List<Long> valores);
}
