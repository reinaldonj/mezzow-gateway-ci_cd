package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.TipoExame;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface TipoExameClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/tipoExame/{id}")
	Optional<TipoExame> show(Long id);
	
	@Get("/tipoExame/{?codigoCliente,grupoEmpresa,modalidade,especialidades,descricao,nome,max,offset,sort,order}")
	Page<TipoExame> find(
    		@Nullable String codigoCliente, 
    		@Nullable String grupoEmpresa,  
    		@Nullable String modalidade,  
    		@Nullable String especialidades,  
    		@Nullable String descricao,  
    		@Nullable String nome,  
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/tipoExame")
	TipoExame save(@Body TipoExame cid);
	
	@Put("/tipoExame")
	TipoExame update(@Body TipoExame cid);
	
	@Delete("/tipoExame/{id}")
	void delete(Long id);
}
