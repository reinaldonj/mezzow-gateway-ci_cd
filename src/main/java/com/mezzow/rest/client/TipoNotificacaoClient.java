package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.TipoNotificacao;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-notificacao")
@Requires(notEnv = Environment.TEST)
public interface TipoNotificacaoClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/tipoNotificacao/{id}")
	Optional<TipoNotificacao> show(Long id);
	
	@Get("/tipoNotificacao/{?codigo,descricao,max,offset,sort,order}")
	Page<TipoNotificacao> find(
			@Nullable String codigo, @Nullable String descricao, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order);
	
	@Post("/tipoNotificacao")
	TipoNotificacao save(@Body TipoNotificacao cid);
	
	@Put("/tipoNotificacao")
	TipoNotificacao update(@Body TipoNotificacao cid);
	
	@Delete("/tipoNotificacao/{id}")
	HttpResponse<?> delete(Long id);
}
