package com.mezzow.rest.client;

import java.util.List;
import java.util.Optional;

import com.mezzow.domain.Unidade;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Client(id = "mezzow-cadastro")
@Requires(notEnv = Environment.TEST)
public interface UnidadeClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/unidade/{id}")
	Optional<Unidade> show(Long id);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/unidade/byNome/{nome}")
	Flowable<List<Unidade>> findByNome(String nome);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/unidade/byNome/{nome}")
	Flowable<HttpResponse<List<Unidade>>> findByNomeRxH(String nome);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/unidade/byId/{valores}")
	Flowable<HttpResponse<List<Unidade>>> findByIdInListRx(List<String> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/unidade/byId/{valores}")
	List<Unidade> findByIdInList(List<String> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/unidade/byEmpresaId/{valores}")
	Flowable<List<Unidade>> findByEmpresaIdInListRx(List<String> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/unidade/byGrupoEmpresaId/{valores}")
	Flowable<List<Unidade>> findByGrupoEmpresaIdInListRx(List<String> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/unidade/byEmpresaId/{valores}")
	List<Unidade> findByEmpresaIdInList(@PathVariable List<String> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/unidade/byEmpresaId/{valores}")
	Flowable<HttpResponse<List<Unidade>>> findByEmpresaIdInListRxH(@PathVariable List<String> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/unidade/byGrupoEmpresaId/{valores}")
	List<Unidade> findByGrupoEmpresaIdInList(@PathVariable List<String> valores);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/unidade/byGrupoEmpresaId/{valores}")
	Flowable<HttpResponse<List<Unidade>>> findByGrupoEmpresaIdInListRxH(@PathVariable List<String> valores);
	
	@Get("/unidade/{?cnpj,nome,empresa,max,offset,sort,order}")
	Page<Unidade> find(
    		@Nullable String cnpj, 
    		@Nullable String nome, 
    		@Nullable String empresa, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Get("/unidade/{?cnpj,nome,empresa,max,offset,sort,order}")
	Flowable<HttpResponse<Page<Unidade>>> findRx(
    		@Nullable String cnpj, 
    		@Nullable String nome, 
    		@Nullable String empresa, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/unidade")
	Unidade save(@Body Unidade cid);
	
	@Put("/unidade")
	Unidade update(@Body Unidade cid);
	
	@Delete("/unidade/{id}")
	HttpResponse<?> delete(Long id);
	
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/unidade/{id}")
	Flowable<HttpResponse<Optional<Unidade>>> showRx(Long id);
}

