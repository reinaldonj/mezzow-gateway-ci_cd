package com.mezzow.rest.client;

import java.util.Optional;

import com.mezzow.domain.Usuario;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.client.annotation.Client;

@Client(id = "mezzow-seguranca")
@Requires(notEnv = Environment.TEST)
public interface UsuarioClient {
	 
	@Consumes(MediaType.APPLICATION_JSON)
	@Get("/usuario/{id}")
	Optional<Usuario> show(Long id);
	
	@Get("/usuario/{?nome,nomeGrupoEmpresa,nomeEmpresa,nomeUnidade,max,offset,sort,order}")
	Page<Usuario> find(
    		@Nullable String nome, 
    		@Nullable String nomeGrupoEmpresa, 
    		@Nullable String nomeEmpresa, 
    		@Nullable String nomeUnidade, 
    		@Nullable Integer max, 
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order);
	
	@Post("/usuario")
	Usuario save(@Body Usuario usuario);
	
	@Put("/usuario")
	Usuario update(@Body Usuario usuario);
}
