package com.mezzow.rest.domain;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mezzow.domain.Empresa;
import com.mezzow.domain.GrupoEmpresa;
import com.mezzow.domain.Produto;
import com.mezzow.domain.Unidade;

import io.micronaut.core.annotation.Introspected;


@Introspected
public class ContratoProdutoResponse {

	private Long id;
	private Produto produto;	
    private GrupoEmpresa grupoEmpresa;
    private Unidade unidade;
    private Empresa empresa;

	@JsonFormat(pattern="yyyy-MM-dd")
	private Timestamp dataCadastro;

	@JsonFormat(pattern="yyyy-MM-dd")
	private Timestamp dataIniVigencia;

	@JsonFormat(pattern="yyyy-MM-dd")
	private Timestamp dataFimVigencia;
	
	
	
	public ContratoProdutoResponse(Long id, Produto produto, GrupoEmpresa grupoEmpresa, Unidade unidade,
			Empresa empresa, Timestamp dataCadastro, Timestamp dataIniVigencia, Timestamp dataFimVigencia) {
		super();
		this.id = id;
		this.produto = produto;
		this.grupoEmpresa = grupoEmpresa;
		this.unidade = unidade;
		this.empresa = empresa;
		this.dataCadastro = dataCadastro;
		this.dataIniVigencia = dataIniVigencia;
		this.dataFimVigencia = dataFimVigencia;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public GrupoEmpresa getGrupoEmpresa() {
		return grupoEmpresa;
	}

	public void setGrupoEmpresa(GrupoEmpresa grupoEmpresa) {
		this.grupoEmpresa = grupoEmpresa;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Timestamp getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Timestamp dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Timestamp getDataIniVigencia() {
		return dataIniVigencia;
	}

	public void setDataIniVigencia(Timestamp dataIniVigencia) {
		this.dataIniVigencia = dataIniVigencia;
	}

	public Timestamp getDataFimVigencia() {
		return dataFimVigencia;
	}

	public void setDataFimVigencia(Timestamp dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	
	
	
}
