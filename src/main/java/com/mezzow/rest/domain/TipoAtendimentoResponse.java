package com.mezzow.rest.domain;

import com.mezzow.domain.Produto;

public class TipoAtendimentoResponse {

    private Long id;
    private String nome;
    private String descricao;
    private Produto produto;
	
    public TipoAtendimentoResponse() {
	}
	
    public TipoAtendimentoResponse(Long id, String nome, String descricao, Produto produto) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.produto = produto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
}
