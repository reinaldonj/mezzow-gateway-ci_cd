package com.mezzow.service;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.commons.lang3.StringUtils;

import com.mezzow.cache.AgendamentoCache;
import com.mezzow.common.SecurityHelper;
import com.mezzow.domain.Agenda;
import com.mezzow.domain.Agendamento;
import com.mezzow.domain.Paciente;
import com.mezzow.domain.Perfil;
import com.mezzow.domain.TipoAtendimento;
import com.mezzow.domain.TipoConsulta;
import com.mezzow.domain.Unidade;
import com.mezzow.domain.enums.StatusAgendamentoEnum;
import com.mezzow.domain.relatorio.AgendamentoRel;
import com.mezzow.rest.client.AgendamentoClient;
import com.mezzow.rest.client.PacienteClient;
import com.mezzow.rest.client.TipoAtendimentoClient;
import com.mezzow.rest.client.TipoConsultaClient;
import com.mezzow.rest.client.UnidadeClient;
import com.mezzow.rest.client.UsuarioClient;
import com.mezzow.util.Pagina;
import com.mezzow.util.TemplateModel;
import com.mezzow.util.TemplateToPdfConverterUtil;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Sort;
import io.micronaut.http.HttpResponse;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

@Introspected
@Singleton
public class AgendamentoService {
	
	private static final String TEMPLATE_CSS_RELATORIO_AGENDAMENTO = 
	
			"table {\r\n"
			+ "  margin: 0;\r\n"
			+ "  padding: 0;\r\n"
			+ "  width: 100%;\r\n"
			+ "  table-layout: fixed;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "table caption {\r\n"
			+ "  font-size: 1.5em;\r\n"
			+ "  margin: .5em 0 .75em;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "table tr {\r\n"
			+ "  background-color: #f8f8f8;\r\n"
			+ "  border: 1px solid #ddd;\r\n"
			+ "  padding: .35em;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "table tr,\r\n"
			+ "table td {\r\n"
			+ "  padding: .625em;\r\n"
			+ "  text-align: center;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "table tr {\r\n"
			+ "  font-size: .85em;\r\n"
			+ "  letter-spacing: .1em;\r\n"
			+ "  text-transform: uppercase;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "@media screen and (max-width: 600px) {\r\n"
			+ "  \r\n"
			+ "  #container { width: 400px; }\r\n"
			+ "  \r\n"
			+ "  table {\r\n"
			+ "    border: 0;\r\n"
			+ "  }\r\n"
			+ "\r\n"
			+ "  table caption {\r\n"
			+ "    font-size: 1.3em;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table tr {\r\n"
			+ "    border: none;\r\n"
			+ "    clip: rect(0 0 0 0);\r\n"
			+ "    height: 1px;\r\n"
			+ "    margin: -1px;\r\n"
			+ "    overflow: hidden;\r\n"
			+ "    padding: 0;\r\n"
			+ "    position: absolute;\r\n"
			+ "    width: 1px;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table tr {\r\n"
			+ "    border-bottom: 3px solid #ddd;\r\n"
			+ "    display: block;\r\n"
			+ "    margin-bottom: .625em;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table td {\r\n"
			+ "    border-bottom: 1px solid #ddd;\r\n"
			+ "    display: block;\r\n"
			+ "    font-size: .8em;\r\n"
			+ "    text-align: right;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table td::before {\r\n"
			+ "    content: attr(data-label);\r\n"
			+ "    float: left;\r\n"
			+ "    font-weight: bold;\r\n"
			+ "    text-transform: uppercase;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table td:last-child {\r\n"
			+ "    border-bottom: 0;\r\n"
			+ "  }\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "body {\r\n"
			+ "  font-family: \"Open Sans\", sans-serif;\r\n"
			+ "  line-height: 1.25;\r\n"
			+ "}";
			
			
	private static final String TEMPLATE_BODY_RELATORIO_AGENDAMENTO = 
			"<div id=\"container\">\r\n"
			+"#if(${model.dominio.paginas.isEmpty()}) "
			+ "	<table >\r\n"
			+ "	  <caption style=\"text-align: left;\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHEAAABMCAIAAADV+M/0AAAS5klEQVR4nO2daZQcV3WAv/equns2zUgzoxnti6WRInnBWPYBwho7iZcYG+KACRCDY2xsjg/LOYDDGnEMGCMOwYGDFzAHJ2CFRTgxS7ATESCOlxhjQLJkWbs02jUazUg9S1e99/LjVlVX9/TM9IzahB99f0it7le33nL3e9+Vcs5RIxBUSqmJxgBgrNEoQGud/nU0KAC7e/uAvb2HDx87CfQNnAbCMPR9H5jZ1gzMnz1r8YIuYOn8biCX9UveYixglPO1N/btE82vujETg558SB2mCKqGdJoG51wZwYZhCHieR4qWd+0/Avz7L34NbHxi02+27gYOHO0HCqfyGAsoWzJDpxVAxmtoagLmds8Czl+19JJXnAtc/toLgLMWdifTICZb7XvpLyfmpzOBF2tPE7DWAk7hKQ0EoQUe3vjU3d/+KfDLp54Dwr5BwHla5TKAy/gA2ldaFh+h0g7AALIvsuNBCLjRQBkL+B2twGtevgq45a1XXHXJy4CMrwHjrHIW0NoH3Jkx+ARQ5/3aw4tIp8YYQGthdjY88gTwibu+Azz/661KeYBrzgE64wPaKWsNYBWAMwplAZwGos8lcwfwYl0nT9kgBFR+FHDO/NEFq4Db33ctcM2lr5DFCuuIFHox4MXa09Aa0bm79h4Gbll736M/eRzwMj7gWhojBWssULZbygG4aXFmxHeejjDlhwFXCIE/v+KP7/nUjcDSRXNlhmJ16FrLgDrv1x5qSacxZwEoj/UPPwa8+7Z/BPInBjKz2pIxoR3DyBXQ6Qr8XumtkFJkybOAUr4uqvigf6ClYyZwz+duBd521WtC44hpuoZmQM321DknqIShbvvCA5+/80FAz2gEVC5jAll8pW0SWahdPCeYKu9PcgAO8HzfjQaAPTUMfPi2t975wXcARowBVK22tc77tYdq6XQCOzmyq2Mj/8aP3g3cf9+G7Ox2IHAh4KxKRiOIJiLGM/YPhfe1Jb045zxRSloDwbETN9z0JuDrd9wMWGvLHOVpw9R4v+JaA2eBjNLv/thXgfvu2QBkuzqDIGDsrlUUf6p0x5PPFZlR3CpPc2ZSK+v7wZE+4MZbrgHu/czNRhCrM93ZOu/XHvzJh6SgjGxEb2Y8Ddz+5e/ed89DQLarAyiEQTy6lLiF9MposCLFFQe4+A9xHyyRwJlMRCiVxqzij8I6hTDMdncAMu0Fczo/fus1yaJ8b/qSZ/p63xgjrsi/bXwaeMM71vrNjUAoCMdFOxVZWVFQ1BCUAnylADM08tA3/x64+pKLSK1uGlDn/drDdOg0sQGOHB8ALrzivUDv4ROqMQc4CRyVWYvjKZxSSiwxBsrG28T9nwrxlvsCFRfrARRG589tB5750V1Ad2fbtEOCU5OnAnEMQn1i3T8BvXsOA15HqwkNRGyd8TxDhTmJXW8V1kpk0xR/U0r7GsCGgCnYaI+VApTv+coDApvsbIkYicb6GhBLzYSOwCS/Ke3hFZMLxth4iw3gNeUO7DwMyKLu/dyt8p5pCIA679cepsb7QqFiGj/57PZXvPFDANkMjGGrwSEht4izYpAzdM7RmAVcY46YcOzoKPk8oBqagKZZLVk/AwyPFICRgVMUQsCb0QRY33OpoIHney40gDs1TCydvBnNTS1NxEr89MhoMJgHGBkBvOZmm/MBF+cRJEBlgwB44qF1L3/pytSSp0B80+F92Zl19/5AjQSAasgBNuZiUaPvvOHqWTNbARcGgCHysmS1TQ25pzftAB792dOAHQmAuUvmvvNNbwaueN0aYNmiOZmsDwwPjQB7Dx7/3iOPA/d/66dAfmiEhhyglQLsQN7LZYHL3vBq4C2XvQo4/5yzOttbgYxSwOmh4X2H+oBHH/stcO+3fnL0cB+gmhsBZ63zNKBPh8C6e7///Xs+BsSRyCnsaZ33aw/V8n6aBX6zdS9w4ZXvNzqlf5SiEADdczqAHRu/2iLnX6o9AxMCGc//2J3/DHz2s/cDH/zo3wIfv+Wv2lqbxj4STQCnUMCmbXuBy6/7xMFjAwD5YeCVr13zlU/dBJy/amkphhIqS2M+1j949Q2fBp743+cA1dwYKUylAM/yqx/9A/CSVYship0rryoSrIr3nXMSjRCUDz70M8AMDnmdrYCoe1/pMDTAsoXdQEtzYyEwgI7cFxs9LUfosWV3L/Cd9Z8G3nzFq4DQmtAaYg2eiDAXpVKseDjnrVwM3L/u/Zdf/UHgAx/6G+CLH71eBkt2Fq0AHccaxAxwChHBxgLMntX6wBffD5x/5QeAoeFR0fGepwHTNyjLPH/V9UCoHOBjraRqirGZCrtc5/3aQ1W8n8TxJJN8zqW3Aju277cNWfkZyPp+4fgAcMO7Xg98/Y5bhejKKkEERgvh/kNHgOWL5wMy0lO6egM7PzTy8Mangbdc+SqEBk0xc1eNuR6EVuyBa9+3Dvjev/xHpn0mEDoLMFzo6VkIbH7kK8TZbIuLLeaJMFfL+4Ll15t3AC/s7AVoyMZ5EgUYjMYC5/UsmRRhLuuftXge8SEpHGCVFZ4dE22roHmbGnN//fpXE0fpnXVi+xvJm3pi+ZdHcdJ7rbRTSgMXntsDfG/9xjhf62R1skxZ8svOXyH7oKuIBNZ5v/YwNfv0f57ZAqihAqAbc0aOXdxNq1wuC6zqWSCD1YRCRVg1E9XbxOpI/kxyAePbhkopqR8QovNiCeMqEYlKPTX21+6OdgCt0nUunqdNfhR44pktpOi0moDa1Pb0mU07iRWxdnGZjeQdjcm2NAE9S+aNXYDFpXQlFiWCb/P2/cD6h38JnDw19Ja/eCXw6otWxwuYiI2Sagzgx7945tHHfkMsN95+9euAC85eLMKpshfkSgyscuQOozXw9KadyZcTU0nx2apG1WEqMAmdlp3h9j2HATIeSew5pkcXBAsWdAPz5nSkvxfQODm/2M5V//n474DLr1sLhAODANY9sP4RYPPP7gYWz589ga9tnBWS/MBnvgF86YvrMxkPCIZHgB/88L+Bzf91b0tDBTMgWpQzsvy+gUFAOWyKr0PnyOhkyek0e3pzKgqTqnhfay2BjIPHTwBEQrB0TwuhGEbZjEeFNGSUvE8KaW67/RtAmB8GGrs6BV2+bxDYsm0PsGT+bFuJK6NIo9bizt113wbA62h1vg9oOwM40DcAHDp4tOes+YBztjjR5O/YCew9dJwx1KOdM75KljwyGgCNDdkSy2Eci6rO+7WHanVUPj8MnM6PAqJIk3ONjiu0q1fMT8bbMRrYOkusQx5/Zuuzz+0EVGszMFwoAJ5SOucDM2Y0R8/oCoRgbQhonf3xxicBN2QAmnQYBhA5no1NTUBzS0OMxzEmBeYpLZbvtl29AL5Ok6oBlJ8sOZ/PA43i40wGk8lThcxmeLQAjBZCmU56TGSCaHXuikUToRI329fAz598zg0XAK+5ARDWNM55uQzQ1dEqj6hK+UDlyZztU7/dAeBHIUQx4AkCYE5nG9DV3hbj0Wk8iakmAYrtuw8DKpMpNwA0xFcMhkclFoxRULJrFay9Ou/XHqYYky7P3GmSBFFDZvXyhckvWpcnndMS/XfP74n40aYSdoWga95sYMGczrGPkETvlWSTzAt7DwFKClpjFSw1vWIj+75f2XKIsR48Mgj0HjkBuIyHncT+dM6NkUb1uNTvBSajUzk6rRpzWSCXyQIj+QKAF4vSMARaZ7UuXTgneU6PqUJJlyBs3XWQTIbEERR6HA2WL5oDNDc1IFqujE5FuFsHHDo6sP9gH0TGsnNoIaHAAGcvj/xjMYdVqX0qaR7P83bs3Y9kukA1N7gyOjUOyGV8oCmbjTBEwniiPZtkT5N5SNB+RksDMNA/CHh48msYGGDR/M7uzrbUoxEHyPMJbx7vGwD2HTgqexqZe8KboV29fEHypXVWl90Yc1GwGti1/8jQwGlANWUBrJMItPE0cE6ptiyXIfG/xDNmNAT8Fi+UEkQ5OYXDJQtvammceKPSUOf92kNVdGqtbWjIAnM72oDe3YcAm8tGx18oACuXzp3gVofFyQWRnfuPACf7B8kVM9gmdsnOXrEoeSljFIZL8d3WHfvUaAD4zQ1AgAmsAWjIAKt6FjK+75jAFqHTBH3JWEVogHmd7SSWqXWqkslcBtXGpOWD6NOnn3gO8JWKHGRrgXN7lsTZ5grK2jgnRu1WiWePFDKNOSAwpog/55/TU7QcxgSBStyIzdv3xnExGR1tQVt7G7B0UTeglNKVNHliBmzZsQ9QmWJMIAEvlmnLF3cnXxpnvVS5wnhnVuf92kN1dBofxppzlwEPPvgoYFWsYbwMcO6KJTKm7OjKGPm5bb0ANi5GF9IOLdDYNmPZoqLlMCbxq3VkHStg6/ZePJ9YbiilXBACi+Z2ALMTDyo1GVcaSMsPjezYdxRwWcFTumStxOa58LzlyZdGlRTVjCdY/GrSYcmvr1lzNkBTjnQNV3MO6FkyJx5cYuwLfj/+56YX9oA41zLYAgQhsGDp3HlzO4ovdSUmi3FWqvHFP961/whZv4hf6aAQAquXLUzebYzxIjc6Sk9FLoBSwP6DfUeP9QNkfEDZEkvKGEtzBnjlmtXJl151Kcg679ce/GrSv0kI5yXnLANWLl8IbNuxD+UBXV0zgYXzZqcfKQ9Hai1XJnbEDqXENJXyARuMAMsXz5HEdXxRVbuolrXk4A8cOg4cONovt1SFuqyOKixXp7ScdUpHai2q24hyoloDL+w+YE4PA7qtGbAmcrsjzT5c6OlZBFxwzvLk+TG3KG1FoqzW3y9YSeJ7wFUXXwh8YfMulwNYNr+LuP0DY5zryJOBg0dOAAfEufY9Ip4FsGEAnLdyiTySEkcp8WVCuXG3c99hoHAqL7VjYnU45/AzwNkri9a+VlFKTkV74TkVJr9u3r6fMGCM3IuqU4cLUoQumf2yWoV4hpW5vM77tYdq6dRPkf1b33gxcNc3f1wYOA2sWBY516fzw4y19q0Fmpsatuw4CIwMniblXEfWvtbAiqVRwlUSFb6JpIcQRWBNa1MG+O3zewBGAr+lCQjEKzFWyHbJ/KI5OTIa4BmKly4xxgHNvg9sen4P2k9+Nc4JW0j5l9fa9PY3XCwrIOb6RGtOotKnVdOrgWtuvuMHG34OtC/ukl2LLo4XOaIYGNRay473n8zLnNKegcQ4ZnfOzGYygJUo+5hEgbz35EAeOHV6WKR5En4UOTi3sy2ZobXJ4pPIsU125HjfqdFC6lacc9r3Ads/APzlG/9kwz0fKVvypI4Z8WvqUGOY4t3IlDH75LPbpPZcjHMXGsbx0+VJyZpMcOdAWSv147iYAMs7TDiIs7ZlYebkXnQQRq+T4ZF7ropfxnhUxncqjkGRuhlT+P3VngvXaCAMC8DLX7ryxmv/DPjaN34EZLpmBlL/FVeplHTeSByVikcot4A9T2RxfPM8VrLRX1HVX2U88QGoXBbQVRCKSZBI+sD3TN9J4Mbrr5LVmTjMOimqMqjzfu2hBnfO1lz5PuDAoROqIQu4anpIkOK41Of4zlmFfKlyk11MFyRiulcRkStiFv0zUpA7Z7/64ZeAObNbJcA2jTtn1d/fh3jtAuX3Ta/7lD8jC4Q22axKqeTim8tkXDVzBcbvPzEW4cTNJ+LFyK2UID/8rw+sBa665CLA1u+b/kHBGfVDSV/Lvv3L3/3k2q8B2a5ZQCGwUj06SVuTMuKaOvGOi6oKhFnfB4Ij/cDate/65Huv5f/3rjlJMo4o7R71mbj7+0C2u7MgxTbxi6b9luncOC8+Mq78yfp+4WgfcNPN0mfiPSZVfXQmUOf92kP1dDpuGXjSY0YM43d95KvA/fdtyMxuB6IEr/FK1YUu6X42HoeWWgVn3hxB/FdfJT1mrgG+fsd7AGutWN9n3oGhlv2lEiEAfHjdN9d9fj3p/lJi5dio8KEmr4QJ9yBhfQAyvjKjhqS/1IfffueH3kZUGlHvL/WHDdOn07FBmigo5wCyWn374V8CN//dV4DTx/vlRlfcWy5O6Ud8XerX164Niq+LF6WCk/0t7e2M01sueeOZv7X2fSUl6GBxFftKaimVaGpg/L6S8dTUNIyqaH/G6St599qbgLMWzwFCazxVcjWtVlDn/dpD1b6pjJ4K6iRVByilSnvKbouKVkp6ykY0booNpSaeuyLOWI3TU9atXLMS+PR7/4B7yk5jZxO/QEdxwgr9pINjg4DKetIGxGWjCGl0R0/JnVQvksVJ6YOkTCVWPxqIGMlIP+mXnQ3c8rbLrvrTi4CMVFc4W3aLvVZrLIM679ceXvS+5wlYW4zCxYG5sl7yv3t2y27g8NGTwEh+WKqFhR4TM0PqXGwm09BcoZf8pa+9AFi+sDv96mlHl6cHL+KepjGXXfse9/+RKITA3gNHgN29Rw8c6ydO6gVhmPF9oL21GZjbNWvpgi5gcaX/RyKxPeRub4qRp9wwZhpQ5/3aw/8BEIjodALq754AAAAASUVORK5CYII=\" id=\"p1img1\"></img></caption>\r\n"
			+ "	  <thead>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\">TELECONSULTA AGENDAMENTO</th>\r\n"
			+ "		  <th scope=\"col\" colspan=\"3\">De $dateTool.format('dd/MM/yyyy',${model.dominio.dataDe}) até $dateTool.format('dd/MM/yyyy',${model.dominio.dataAte})</th>\r\n"
			+ "		  <th scope=\"col\">PAGINA 1</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\">AGENDAMENTO</th>\r\n"
			+ "		  <th scope=\"col\">PACIENTE</th>\r\n"
			+ "		  <th scope=\"col\">CONSULTA</th>\r\n"
			+ "		  <th scope=\"col\">STATUS</th>\r\n"
			+ "		  <th scope=\"col\">CONSULTA ANTERIOR</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  </thead>\r\n"
			+ "	  <tbody>\r\n"
			+ "	  </tbody>\r\n"
			+ "   <tfoot>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\" colspan=\"5\" style=\"text-align: left;\" >TOTAL:</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\">&#160;</th>\r\n"
			+ "		 <th scope=\"col\">CADASTRADAS: 0</th>\r\n"
			+ "		  <th scope=\"col\">PENDENTES: 0</th>\r\n"
			+ "		  <th scope=\"col\" >FINALIZADAS: 0</th>\r\n"
			+ "		  <th scope=\"col\" >CANCELADAS: 0</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  </tfoot>"
			+ "	</table>\r\n"
			+"#else"		
			+"#foreach($pagina in ${model.dominio.paginas}) "
			+ "	<table >\r\n"
			+ "	  <caption style=\"text-align: left;\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHEAAABMCAIAAADV+M/0AAAS5klEQVR4nO2daZQcV3WAv/equns2zUgzoxnti6WRInnBWPYBwho7iZcYG+KACRCDY2xsjg/LOYDDGnEMGCMOwYGDFzAHJ2CFRTgxS7ATESCOlxhjQLJkWbs02jUazUg9S1e99/LjVlVX9/TM9IzahB99f0it7le33nL3e9+Vcs5RIxBUSqmJxgBgrNEoQGud/nU0KAC7e/uAvb2HDx87CfQNnAbCMPR9H5jZ1gzMnz1r8YIuYOn8biCX9UveYixglPO1N/btE82vujETg558SB2mCKqGdJoG51wZwYZhCHieR4qWd+0/Avz7L34NbHxi02+27gYOHO0HCqfyGAsoWzJDpxVAxmtoagLmds8Czl+19JJXnAtc/toLgLMWdifTICZb7XvpLyfmpzOBF2tPE7DWAk7hKQ0EoQUe3vjU3d/+KfDLp54Dwr5BwHla5TKAy/gA2ldaFh+h0g7AALIvsuNBCLjRQBkL+B2twGtevgq45a1XXHXJy4CMrwHjrHIW0NoH3Jkx+ARQ5/3aw4tIp8YYQGthdjY88gTwibu+Azz/661KeYBrzgE64wPaKWsNYBWAMwplAZwGos8lcwfwYl0nT9kgBFR+FHDO/NEFq4Db33ctcM2lr5DFCuuIFHox4MXa09Aa0bm79h4Gbll736M/eRzwMj7gWhojBWssULZbygG4aXFmxHeejjDlhwFXCIE/v+KP7/nUjcDSRXNlhmJ16FrLgDrv1x5qSacxZwEoj/UPPwa8+7Z/BPInBjKz2pIxoR3DyBXQ6Qr8XumtkFJkybOAUr4uqvigf6ClYyZwz+duBd521WtC44hpuoZmQM321DknqIShbvvCA5+/80FAz2gEVC5jAll8pW0SWahdPCeYKu9PcgAO8HzfjQaAPTUMfPi2t975wXcARowBVK22tc77tYdq6XQCOzmyq2Mj/8aP3g3cf9+G7Ox2IHAh4KxKRiOIJiLGM/YPhfe1Jb045zxRSloDwbETN9z0JuDrd9wMWGvLHOVpw9R4v+JaA2eBjNLv/thXgfvu2QBkuzqDIGDsrlUUf6p0x5PPFZlR3CpPc2ZSK+v7wZE+4MZbrgHu/czNRhCrM93ZOu/XHvzJh6SgjGxEb2Y8Ddz+5e/ed89DQLarAyiEQTy6lLiF9MposCLFFQe4+A9xHyyRwJlMRCiVxqzij8I6hTDMdncAMu0Fczo/fus1yaJ8b/qSZ/p63xgjrsi/bXwaeMM71vrNjUAoCMdFOxVZWVFQ1BCUAnylADM08tA3/x64+pKLSK1uGlDn/drDdOg0sQGOHB8ALrzivUDv4ROqMQc4CRyVWYvjKZxSSiwxBsrG28T9nwrxlvsCFRfrARRG589tB5750V1Ad2fbtEOCU5OnAnEMQn1i3T8BvXsOA15HqwkNRGyd8TxDhTmJXW8V1kpk0xR/U0r7GsCGgCnYaI+VApTv+coDApvsbIkYicb6GhBLzYSOwCS/Ke3hFZMLxth4iw3gNeUO7DwMyKLu/dyt8p5pCIA679cepsb7QqFiGj/57PZXvPFDANkMjGGrwSEht4izYpAzdM7RmAVcY46YcOzoKPk8oBqagKZZLVk/AwyPFICRgVMUQsCb0QRY33OpoIHney40gDs1TCydvBnNTS1NxEr89MhoMJgHGBkBvOZmm/MBF+cRJEBlgwB44qF1L3/pytSSp0B80+F92Zl19/5AjQSAasgBNuZiUaPvvOHqWTNbARcGgCHysmS1TQ25pzftAB792dOAHQmAuUvmvvNNbwaueN0aYNmiOZmsDwwPjQB7Dx7/3iOPA/d/66dAfmiEhhyglQLsQN7LZYHL3vBq4C2XvQo4/5yzOttbgYxSwOmh4X2H+oBHH/stcO+3fnL0cB+gmhsBZ63zNKBPh8C6e7///Xs+BsSRyCnsaZ33aw/V8n6aBX6zdS9w4ZXvNzqlf5SiEADdczqAHRu/2iLnX6o9AxMCGc//2J3/DHz2s/cDH/zo3wIfv+Wv2lqbxj4STQCnUMCmbXuBy6/7xMFjAwD5YeCVr13zlU/dBJy/amkphhIqS2M+1j949Q2fBp743+cA1dwYKUylAM/yqx/9A/CSVYship0rryoSrIr3nXMSjRCUDz70M8AMDnmdrYCoe1/pMDTAsoXdQEtzYyEwgI7cFxs9LUfosWV3L/Cd9Z8G3nzFq4DQmtAaYg2eiDAXpVKseDjnrVwM3L/u/Zdf/UHgAx/6G+CLH71eBkt2Fq0AHccaxAxwChHBxgLMntX6wBffD5x/5QeAoeFR0fGepwHTNyjLPH/V9UCoHOBjraRqirGZCrtc5/3aQ1W8n8TxJJN8zqW3Aju277cNWfkZyPp+4fgAcMO7Xg98/Y5bhejKKkEERgvh/kNHgOWL5wMy0lO6egM7PzTy8Mangbdc+SqEBk0xc1eNuR6EVuyBa9+3Dvjev/xHpn0mEDoLMFzo6VkIbH7kK8TZbIuLLeaJMFfL+4Ll15t3AC/s7AVoyMZ5EgUYjMYC5/UsmRRhLuuftXge8SEpHGCVFZ4dE22roHmbGnN//fpXE0fpnXVi+xvJm3pi+ZdHcdJ7rbRTSgMXntsDfG/9xjhf62R1skxZ8svOXyH7oKuIBNZ5v/YwNfv0f57ZAqihAqAbc0aOXdxNq1wuC6zqWSCD1YRCRVg1E9XbxOpI/kxyAePbhkopqR8QovNiCeMqEYlKPTX21+6OdgCt0nUunqdNfhR44pktpOi0moDa1Pb0mU07iRWxdnGZjeQdjcm2NAE9S+aNXYDFpXQlFiWCb/P2/cD6h38JnDw19Ja/eCXw6otWxwuYiI2Sagzgx7945tHHfkMsN95+9euAC85eLMKpshfkSgyscuQOozXw9KadyZcTU0nx2apG1WEqMAmdlp3h9j2HATIeSew5pkcXBAsWdAPz5nSkvxfQODm/2M5V//n474DLr1sLhAODANY9sP4RYPPP7gYWz589ga9tnBWS/MBnvgF86YvrMxkPCIZHgB/88L+Bzf91b0tDBTMgWpQzsvy+gUFAOWyKr0PnyOhkyek0e3pzKgqTqnhfay2BjIPHTwBEQrB0TwuhGEbZjEeFNGSUvE8KaW67/RtAmB8GGrs6BV2+bxDYsm0PsGT+bFuJK6NIo9bizt113wbA62h1vg9oOwM40DcAHDp4tOes+YBztjjR5O/YCew9dJwx1KOdM75KljwyGgCNDdkSy2Eci6rO+7WHanVUPj8MnM6PAqJIk3ONjiu0q1fMT8bbMRrYOkusQx5/Zuuzz+0EVGszMFwoAJ5SOucDM2Y0R8/oCoRgbQhonf3xxicBN2QAmnQYBhA5no1NTUBzS0OMxzEmBeYpLZbvtl29AL5Ok6oBlJ8sOZ/PA43i40wGk8lThcxmeLQAjBZCmU56TGSCaHXuikUToRI329fAz598zg0XAK+5ARDWNM55uQzQ1dEqj6hK+UDlyZztU7/dAeBHIUQx4AkCYE5nG9DV3hbj0Wk8iakmAYrtuw8DKpMpNwA0xFcMhkclFoxRULJrFay9Ou/XHqYYky7P3GmSBFFDZvXyhckvWpcnndMS/XfP74n40aYSdoWga95sYMGczrGPkETvlWSTzAt7DwFKClpjFSw1vWIj+75f2XKIsR48Mgj0HjkBuIyHncT+dM6NkUb1uNTvBSajUzk6rRpzWSCXyQIj+QKAF4vSMARaZ7UuXTgneU6PqUJJlyBs3XWQTIbEERR6HA2WL5oDNDc1IFqujE5FuFsHHDo6sP9gH0TGsnNoIaHAAGcvj/xjMYdVqX0qaR7P83bs3Y9kukA1N7gyOjUOyGV8oCmbjTBEwniiPZtkT5N5SNB+RksDMNA/CHh48msYGGDR/M7uzrbUoxEHyPMJbx7vGwD2HTgqexqZe8KboV29fEHypXVWl90Yc1GwGti1/8jQwGlANWUBrJMItPE0cE6ptiyXIfG/xDNmNAT8Fi+UEkQ5OYXDJQtvammceKPSUOf92kNVdGqtbWjIAnM72oDe3YcAm8tGx18oACuXzp3gVofFyQWRnfuPACf7B8kVM9gmdsnOXrEoeSljFIZL8d3WHfvUaAD4zQ1AgAmsAWjIAKt6FjK+75jAFqHTBH3JWEVogHmd7SSWqXWqkslcBtXGpOWD6NOnn3gO8JWKHGRrgXN7lsTZ5grK2jgnRu1WiWePFDKNOSAwpog/55/TU7QcxgSBStyIzdv3xnExGR1tQVt7G7B0UTeglNKVNHliBmzZsQ9QmWJMIAEvlmnLF3cnXxpnvVS5wnhnVuf92kN1dBofxppzlwEPPvgoYFWsYbwMcO6KJTKm7OjKGPm5bb0ANi5GF9IOLdDYNmPZoqLlMCbxq3VkHStg6/ZePJ9YbiilXBACi+Z2ALMTDyo1GVcaSMsPjezYdxRwWcFTumStxOa58LzlyZdGlRTVjCdY/GrSYcmvr1lzNkBTjnQNV3MO6FkyJx5cYuwLfj/+56YX9oA41zLYAgQhsGDp3HlzO4ovdSUmi3FWqvHFP961/whZv4hf6aAQAquXLUzebYzxIjc6Sk9FLoBSwP6DfUeP9QNkfEDZEkvKGEtzBnjlmtXJl151Kcg679ce/GrSv0kI5yXnLANWLl8IbNuxD+UBXV0zgYXzZqcfKQ9Hai1XJnbEDqXENJXyARuMAMsXz5HEdXxRVbuolrXk4A8cOg4cONovt1SFuqyOKixXp7ScdUpHai2q24hyoloDL+w+YE4PA7qtGbAmcrsjzT5c6OlZBFxwzvLk+TG3KG1FoqzW3y9YSeJ7wFUXXwh8YfMulwNYNr+LuP0DY5zryJOBg0dOAAfEufY9Ip4FsGEAnLdyiTySEkcp8WVCuXG3c99hoHAqL7VjYnU45/AzwNkri9a+VlFKTkV74TkVJr9u3r6fMGCM3IuqU4cLUoQumf2yWoV4hpW5vM77tYdq6dRPkf1b33gxcNc3f1wYOA2sWBY516fzw4y19q0Fmpsatuw4CIwMniblXEfWvtbAiqVRwlUSFb6JpIcQRWBNa1MG+O3zewBGAr+lCQjEKzFWyHbJ/KI5OTIa4BmKly4xxgHNvg9sen4P2k9+Nc4JW0j5l9fa9PY3XCwrIOb6RGtOotKnVdOrgWtuvuMHG34OtC/ukl2LLo4XOaIYGNRay473n8zLnNKegcQ4ZnfOzGYygJUo+5hEgbz35EAeOHV6WKR5En4UOTi3sy2ZobXJ4pPIsU125HjfqdFC6lacc9r3Ads/APzlG/9kwz0fKVvypI4Z8WvqUGOY4t3IlDH75LPbpPZcjHMXGsbx0+VJyZpMcOdAWSv147iYAMs7TDiIs7ZlYebkXnQQRq+T4ZF7ropfxnhUxncqjkGRuhlT+P3VngvXaCAMC8DLX7ryxmv/DPjaN34EZLpmBlL/FVeplHTeSByVikcot4A9T2RxfPM8VrLRX1HVX2U88QGoXBbQVRCKSZBI+sD3TN9J4Mbrr5LVmTjMOimqMqjzfu2hBnfO1lz5PuDAoROqIQu4anpIkOK41Of4zlmFfKlyk11MFyRiulcRkStiFv0zUpA7Z7/64ZeAObNbJcA2jTtn1d/fh3jtAuX3Ta/7lD8jC4Q22axKqeTim8tkXDVzBcbvPzEW4cTNJ+LFyK2UID/8rw+sBa665CLA1u+b/kHBGfVDSV/Lvv3L3/3k2q8B2a5ZQCGwUj06SVuTMuKaOvGOi6oKhFnfB4Ij/cDate/65Huv5f/3rjlJMo4o7R71mbj7+0C2u7MgxTbxi6b9luncOC8+Mq78yfp+4WgfcNPN0mfiPSZVfXQmUOf92kP1dDpuGXjSY0YM43d95KvA/fdtyMxuB6IEr/FK1YUu6X42HoeWWgVn3hxB/FdfJT1mrgG+fsd7AGutWN9n3oGhlv2lEiEAfHjdN9d9fj3p/lJi5dio8KEmr4QJ9yBhfQAyvjKjhqS/1IfffueH3kZUGlHvL/WHDdOn07FBmigo5wCyWn374V8CN//dV4DTx/vlRlfcWy5O6Ud8XerX164Niq+LF6WCk/0t7e2M01sueeOZv7X2fSUl6GBxFftKaimVaGpg/L6S8dTUNIyqaH/G6St599qbgLMWzwFCazxVcjWtVlDn/dpD1b6pjJ4K6iRVByilSnvKbouKVkp6ykY0booNpSaeuyLOWI3TU9atXLMS+PR7/4B7yk5jZxO/QEdxwgr9pINjg4DKetIGxGWjCGl0R0/JnVQvksVJ6YOkTCVWPxqIGMlIP+mXnQ3c8rbLrvrTi4CMVFc4W3aLvVZrLIM679ceXvS+5wlYW4zCxYG5sl7yv3t2y27g8NGTwEh+WKqFhR4TM0PqXGwm09BcoZf8pa+9AFi+sDv96mlHl6cHL+KepjGXXfse9/+RKITA3gNHgN29Rw8c6ydO6gVhmPF9oL21GZjbNWvpgi5gcaX/RyKxPeRub4qRp9wwZhpQ5/3aw/8BEIjodALq754AAAAASUVORK5CYII=\" id=\"p1img1\"></img></caption>\r\n"
			+ "	  <thead>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\">TELECONSULTA</th>\r\n"
			+ "		  <th scope=\"col\" colspan=\"3\">De $dateTool.format('dd/MM/yyyy',${model.dominio.dataDe}) até $dateTool.format('dd/MM/yyyy',${model.dominio.dataAte})</th>\r\n"
			+ "		  <th scope=\"col\">PAGINA ${pagina.numeroPagina}</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\">AGENDAMENTO</th>\r\n"
			+ "		  <th scope=\"col\">PACIENTE</th>\r\n"
			+ "		  <th scope=\"col\">CONSULTA</th>\r\n"
			+ "		  <th scope=\"col\">STATUS</th>\r\n"
			+ "		  <th scope=\"col\">CONSULTA ANTERIOR</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  </thead>\r\n"
			+ "	  <tbody>\r\n"
			+ "		#foreach($agendamento in ${pagina.dominios})"
			+ "		<tr>\r\n"
			+ "		#if($foreach.count % 2 >= 1) "
			+ "		  <td data-label=\"Account\"  style=\"background-color: #E9F4FB;\">$dateTool.format('dd/MM/yyyy',${agendamento.dataCadastro})</td>\r\n"
			+ "		  <td data-label=\"Due Date\" style=\"background-color: #E9F4FB;\">${agendamento.paciente.nome.toUpperCase()}</td>\r\n"
			+ "		  <td data-label=\"Amount\"   style=\"background-color: #E9F4FB;\">$dateTool.format('dd/MM/yyyy HH:mm',${agendamento.dataConsulta})</td>\r\n"
			+ "		  <td data-label=\"Period\"   style=\"background-color: #E9F4FB;\">${agendamento.statusAgendamento.descricao}</td>\r\n"
			+ "		  <td data-label=\"Period\"   style=\"background-color: #E9F4FB;\">n/a</td>\r\n"
			+ "		#else "
			+ "		  <td data-label=\"Account\">$dateTool.format('dd/MM/yyyy',${agendamento.dataCadastro})</td>\r\n"
			+ "		  <td data-label=\"Due Date\">${agendamento.paciente.nome.toUpperCase()}</td>\r\n"
			+ "		  <td data-label=\"Amount\">$dateTool.format('dd/MM/yyyy HH:mm',${agendamento.dataConsulta})</td>\r\n"
			+ "		  <td data-label=\"Period\">${agendamento.statusAgendamento.descricao}</td>\r\n"
			+ "		  <td data-label=\"Period\">n/a</td>\r\n"
			+ "		#end "
			+ "		</tr>\r\n"
			+ "		#end "
			+ "	  </tbody>\r\n"
			+ "   <tfoot>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\" colspan=\"5\" style=\"text-align: left;\" >TOTAL:</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\">&#160;</th>\r\n"
			+ "		 <th scope=\"col\">CADASTRADAS: ${model.dominio.totalCadastradas}</th>\r\n"
			+ "		  <th scope=\"col\">PENDENTES: ${model.dominio.totalPendentes}</th>\r\n"
			+ "		  <th scope=\"col\" >FINALIZADAS: ${model.dominio.totalFinalizadas}</th>\r\n"
			+ "		  <th scope=\"col\" >CANCELADAS: ${model.dominio.totalCanceladas}</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  </tfoot>"
			+ "	</table>\r\n"
			+ "		#end "
			+ "	\r\n"
			+"#end"
			+ "	</div>";
			
			
	
	
	
	@Inject
	private AgendamentoClient agendamentoClient;
	@Inject
	private PacienteClient pacienteClient;
	@Inject
	private UnidadeClient unidadeClient;
	@Inject
	private AgendamentoCache cache;
	@Inject
	private TipoConsultaClient tipoConsultaClient;
	
	@Inject
	private TipoAtendimentoClient tipoAtendimentoClient;
	
	@Inject UsuarioClient usuarioClient;
	
	private final SecurityHelper securityHelper;

	AgendamentoService(SecurityHelper securityHelper) {
		this.securityHelper = securityHelper;
	}
	
	public Optional<Agendamento> show(Long id) {
        //Long timeout = 60L;
        final boolean stopped;
//        System.out.println("Optional<Agendamento> show(Long id) ->>> **********************************************************************************");
    	Flowable<Optional<Agendamento>> flatMap = agendamentoClient.showRx(id)
    		.subscribeOn(Schedulers.io())
			.observeOn(Schedulers.io())
			//.timeout(timeout, TimeUnit.SECONDS, p -> p.onComplete()).retry(2l)
			.onErrorResumeNext(throwable -> {
                if (throwable instanceof java.util.concurrent.TimeoutException) {
                    return Flowable.just(null);
                }

                return Flowable.error(throwable);
            })
			.flatMap(it -> {
				Agendamento anull = null;
				if (it.body().isEmpty()) return Flowable.just(Optional.ofNullable(anull));
				
				return 
				Flowable.zip(
						pacienteClient.showRx(it.body().get().getPaciente().getId()).subscribeOn(Schedulers.io())/*.timeout(timeout, TimeUnit.SECONDS).retry(2l)*/,
						unidadeClient.showRx(it.body().get().getUnidade().getId()).subscribeOn(Schedulers.io())/*.timeout(timeout, TimeUnit.SECONDS).retry(2l)*/,
						tipoConsultaClient.showRx(it.body().get().getTipoConsulta() == null || it.body().get().getTipoConsulta().getId() == null ? 0L : it.body().get().getTipoConsulta().getId() ).subscribeOn(Schedulers.io()).onErrorReturn(p -> HttpResponse.ok(Optional.ofNullable(null)))/* .timeout(timeout, TimeUnit.SECONDS).retry(2l)*/,
						//tipoAtendimentoClient.showRx( it.body().get().getTipoAtendimento() == null || it.body().get().getTipoAtendimento().getId() == null ? 0L : it.body().get().getTipoAtendimento().getId() ).subscribeOn(Schedulers.io()).onErrorReturn(p -> HttpResponse.ok(Optional.ofNullable(null)))/* .timeout(timeout, TimeUnit.SECONDS).retry(2l)*/,
						(paci, uni, tcon/*, tAte */ ) -> {
							
							Agendamento ag = new Agendamento();
	    					ag.setDataCadastro(it.body().get().getDataCadastro());
	    					ag.setUsuarioCadastro(it.body().get().getUsuarioCadastro());
	    					ag.setDataConfirmacao(it.body().get().getDataConfirmacao());
	    					ag.setUsuarioConfirmacao(it.body().get().getUsuarioConfirmacao());
	    					ag.setDataCancelamento(it.body().get().getDataCancelamento());
	    					ag.setUsuarioCancelamento(it.body().get().getUsuarioCancelamento());
	    					ag.setDataConsulta(it.body().get().getDataConsulta());
	    					ag.setId(it.body().get().getId());
	    					ag.setPaciente(paci.getBody().orElse(null).orElse(null));
	    					ag.setUnidade( uni.getBody().orElse(null).orElse(null) );
							ag.setTipoConsulta(tcon.getBody().orElse(null).orElse(null) );
							//ag.setTipoAtendimento(tAte.getBody().orElse(null).orElse(null) );
							return Optional.of(ag);
						}
						
				);
				
			});
    	
    	return flatMap.blockingFirst();
    	
    }
	
	private Pageable createPageable(Integer offset, Integer max, String sort, String order) {
		return Pageable.from(Optional.ofNullable(offset).orElse(0),max == null ? 10 : max, Sort.of(new Sort.Order(sort != null ? sort : "id", order != null ? Sort.Order.Direction.valueOf(order.toUpperCase()) : Sort.Order.Direction.ASC, true)) );
	}
	
	public Page<Agendamento> list(
    		@Nullable String nomePaciente,
    		@Nullable String cpfPaciente,
    		@Nullable List<Long> unidadeId,
    		@Nullable String dataDe,
    		@Nullable String dataAte,
    		@Nullable List<Long> listaStatus,
    		@Nullable Integer max,
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order) {
    		
//		System.out.println("11111111111111111111111111111111111111111111111111");
			Flowable<Page<Agendamento>> flatMap = 
			
			pacientesByNomeCpf(nomePaciente, cpfPaciente)
				.subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
				.map(a -> a.getBody().orElse(Collections.emptyList()))
				.flatMap(pacientes -> Flowable.just( pacientes.stream().map(p -> p.getId()).collect(Collectors.toList()) ))
				.flatMap(listaIdsPacientes -> {
//					System.out.println("11111111111111111111111111111111111111111111111111");
					return
						agendamentoClient
				    		.findRx(listaIdsPacientes , unidadeId , dataDe , dataAte , listaStatus , 99999 , null , null , null)
				    		.subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
				    		.map(a -> a.getBody().get())
				    		.flatMap(find -> {
//				    			System.out.println("11111111111111111111111111111111111111111111111111");
				    			if (find.getTotalSize() <=0) {
				    				return Flowable.just( Page.of(Collections.emptyList(), find.getPageable(), 0) );
				    	    	}
//				    			System.out.println("11111111111111111111111111111111111111111111111111");
				    			List<Agendamento> it = find.getContent();
//				    			System.out.println("11111111111111111111111111111111111111111111111111");
				    	    	List<String> idsUnidadesRetornados 		= it.stream().map(p -> String.valueOf(p.getUnidade().getId())).collect(Collectors.toList());
				    			List<Long> idsPacientesRetornados 		= it.stream().map(p -> p.getPaciente().getId()).collect(Collectors.toList());
				    			List<Long> idsTipoConsultaRetornados 	= it.stream().map(p -> p.getTipoConsulta() != null ? p.getTipoConsulta().getId() : null).filter(Objects::nonNull).collect(Collectors.toList());
				    			//List<Long> idsTipoAtendimentoRetornados = it.stream().map(p -> p.getTipoAtendimento() != null ? p.getTipoAtendimento().getId() : null).filter(Objects::nonNull).collect(Collectors.toList());
//				    			System.out.println("11111111111111111111111111111111111111111111111111");
				    			Flowable<Page<Agendamento>> zip = Flowable.zip(
				    					callTipoConsultaByIdInListRx(idsTipoConsultaRetornados).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()), 
				    					//callTipoAtendimentoByIdInListRx(idsTipoAtendimentoRetornados).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()),
				    					callUnidadeByIdInListRx(idsUnidadesRetornados).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()),
				    					callPacienteByIdsRx(idsPacientesRetornados).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()),
				    					(tCon, /*tAte,*/ unid, paci) -> {
				    						
				    						List<Paciente> paciList = paci.getBody().orElse(Collections.emptyList());
				    						List<Unidade> unidList = unid.getBody().orElse(Collections.emptyList());
				    						List<TipoConsulta> tipoConsultaList = tCon.getBody().orElse(Collections.emptyList());
				    						//List<TipoAtendimento> tipoAtendimentoList = tAte.getBody().orElse(Collections.emptyList());
				    						
				    						final Map<Long, Paciente> listaPacientesIdMapRetornados 				= paciList				.stream().collect( Collectors.toMap(Paciente::getId			, p -> p)  );
				    						final Map<Long, Unidade> listaUnidadesIdMapRetornados 					= unidList				.stream().collect( Collectors.toMap(Unidade::getId			, p -> p)  );
				    						final Map<Long, TipoConsulta> listaTipoConsultaIdMapRetornados 			= tipoConsultaList		.stream().collect( Collectors.toMap(TipoConsulta::getId		, p -> p)  );
				    						//final Map<Long, TipoAtendimento> listaTipoAtendimentoIdMapRetornados 	= tipoAtendimentoList	.stream().collect( Collectors.toMap(TipoAtendimento::getId	, p -> p)  );
				    						
				    						it.forEach(ag -> {
				    							
				    							Paciente paciente 				= listaPacientesIdMapRetornados.get(ag.getPaciente().getId());
				    							Unidade unidade		 			= listaUnidadesIdMapRetornados.get(ag.getUnidade().getId());
				    							TipoConsulta tipoConsulta 		= listaTipoConsultaIdMapRetornados.get(ag.getTipoConsulta().getId());
				    						
				    							ag.setPaciente(paciente);
				    							ag.setUnidade( unidade );
				    							ag.setTipoConsulta(tipoConsulta);
				    							
//				    							if (ag.getTipoAtendimento() != null) {
//				    								TipoAtendimento tipoAtendimento = listaTipoAtendimentoIdMapRetornados.get(ag.getTipoAtendimento().getId());
//				    								ag.setTipoAtendimento(tipoAtendimento);	
//				    							}
				    						});
				    						
				    						List<Agendamento> retorno = new ArrayList<>();
				    						retorno.addAll(it);
				    						
				    						Page<Agendamento> retornoPageAgendamento = null;
				    						
				    						final int direction = "ASC".equalsIgnoreCase(Optional.ofNullable(order).orElse("ASC")) ? 1 : -1;
				    						
				    						final int offsetLocal = offset == null || offset <= 0 ? 0 : offset ;
				    						
				    						switch (sort) {
												
				    							case "dataConsulta":
				    								retornoPageAgendamento = Page.of(
					    								retorno
						    								.stream()
						    							    .sorted((agend1, agend2) -> {
						    							    	return direction * agend1.getDataConsulta().compareTo(agend2.getDataConsulta());
						    							    })
						    							    .skip(offsetLocal * max)
						    						        .limit(max == null ? 10 : max)
						    							    .collect(Collectors.toList()),
						    							createPageable(offset, max, sort, order), 
					    							    find.getTotalSize()
				    							    );
													break;
				    							case "paciente.nome":
				    								retornoPageAgendamento = Page.of(
						    								retorno
							    								.stream()
							    							    .sorted((agend1, agend2) -> {
							    							    	return direction * agend1.getPaciente().getNome().compareTo(agend2.getPaciente().getNome());
							    							    })
							    							    .skip(offsetLocal * max)
							    						        .limit(max == null ? 10 : max)
							    							    .collect(Collectors.toList())
							    							    ,
							    							    
							    							createPageable(offset, max, sort, order),
						    							    find.getTotalSize()
					    							    );
				    								break;
				    							case "paciente.cpf":
				    								retornoPageAgendamento = Page.of(
						    								retorno
							    								.stream()
							    							    .sorted((agend1, agend2) -> {
							    							    	return direction * agend1.getPaciente().getCpf().compareTo(agend2.getPaciente().getCpf());
							    							    })
							    							    .skip(offsetLocal * max)
							    						        .limit(max == null ? 10 : max)
							    							    .collect(Collectors.toList()),
							    							createPageable(offset, max, sort, order),
						    							    find.getTotalSize()
					    							    );
				    								break;
				    							case "unidade.nome":
				    								retornoPageAgendamento = Page.of(
						    								retorno
							    								.stream()
							    							    .sorted((agend1, agend2) -> {
							    							    	return direction * agend1.getUnidade().getNome().compareTo(agend2.getUnidade().getNome());
							    							    })
							    							    .skip(offsetLocal * max)
							    						        .limit(max == null ? 10 : max)
							    							    .collect(Collectors.toList()),
							    							createPageable(offset, max, sort, order),
						    							    find.getTotalSize()
					    							    );
				    								break;
				    							case "statusAgendamento.descricao":
				    								retornoPageAgendamento = Page.of(
						    								retorno
							    								.stream()
							    							    .sorted((agend1, agend2) -> {
							    							    	return direction * agend1.getStatusAgendamento().getDescricao().compareToIgnoreCase(agend2.getStatusAgendamento().getDescricao());
							    							    })
							    							    .skip(offsetLocal * max)
							    						        .limit(max == null ? 10 : max)
							    							    .collect(Collectors.toList()),
							    							createPageable(offset, max, sort, order),
						    							    find.getTotalSize()
					    							    );
				    								break;
												default:
													retornoPageAgendamento = Page.of(
						    								retorno
							    								.stream()
							    							    .sorted((agend1, agend2) -> {
							    							    	return -1 /*DESC*/ * agend1.getDataConsulta().compareTo(agend2.getDataConsulta());
							    							    })
							    							    .skip(offsetLocal * max)
							    						        .limit(max == null ? 10 : max)
							    							    .collect(Collectors.toList()),
							    							createPageable(offset, max, sort, order),
						    							    find.getTotalSize()
					    							    );
													break;
											}
				    						
				    						return retornoPageAgendamento;
				    						
				    					});
				    			
				    			return zip;
				    		});
				});
		
			return flatMap.blockingLast();
			
    }


	private Flowable<HttpResponse<List<Paciente>>> pacientesByNomeCpf(String nomePaciente, String cpfPaciente) {
//		System.out.println("11111111111111111111111111111111111111111111111111");
		if (StringUtils.isEmpty(nomePaciente) && StringUtils.isEmpty(cpfPaciente)) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
//		System.out.println("11111111111111111111111111111111111111111111111111");
		return pacienteClient.listByParametrosCombinados(
				Optional.ofNullable(nomePaciente), 
				Optional.ofNullable(cpfPaciente)
		);
	}


	private List<Paciente> callPacienteByIds(List<Long> idsPacientesRetornados) {
		
		if (idsPacientesRetornados == null || idsPacientesRetornados.isEmpty()) {
			return Collections.emptyList();
		}
		
		return pacienteClient.byIds(idsPacientesRetornados);
	}
	
	private Flowable<HttpResponse<List<Paciente>>> callPacienteByIdsRx(List<Long> idsPacientesRetornados) {
		
		if (idsPacientesRetornados == null || idsPacientesRetornados.isEmpty()) {
			return   Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		return pacienteClient.byIdsRx(idsPacientesRetornados);
	}


	private List<Unidade> callUnidadeByIdInList(List<String> idsUnidadesRetornados) {
		
		if (idsUnidadesRetornados == null || idsUnidadesRetornados.isEmpty()) {
			return Collections.emptyList();
		}
		
		return unidadeClient.findByIdInList(idsUnidadesRetornados);
	}
	
	private Flowable<HttpResponse<List<Unidade>>> callUnidadeByIdInListRx(List<String> idsUnidadesRetornados) {
		
		if (idsUnidadesRetornados == null || idsUnidadesRetornados.isEmpty()) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		return unidadeClient.findByIdInListRx(idsUnidadesRetornados);
	}


	private List<TipoAtendimento> callTipoAtendimentoByIdInList(List<Long> idsTipoAtendimentoRetornados) {
		
		if (idsTipoAtendimentoRetornados == null || idsTipoAtendimentoRetornados.isEmpty()) {
			return Collections.emptyList();
		}
		
		return tipoAtendimentoClient.findByIdInList(idsTipoAtendimentoRetornados);
	}
	
	private Flowable<HttpResponse<List<TipoAtendimento>>> callTipoAtendimentoByIdInListRx(List<Long> idsTipoAtendimentoRetornados) {
		
		if (idsTipoAtendimentoRetornados == null || idsTipoAtendimentoRetornados.isEmpty()) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		return tipoAtendimentoClient.findByIdInListRx(idsTipoAtendimentoRetornados);
	}


	private List<TipoConsulta> callTipoConsultaByIdInList(List<Long> idsTipoConsultaRetornados) {
		
		if (idsTipoConsultaRetornados == null || idsTipoConsultaRetornados.isEmpty()) {
			return Collections.emptyList();
		}
		
		
		return tipoConsultaClient.findByIdInList(idsTipoConsultaRetornados);
	}
	
	private Flowable<HttpResponse<List<TipoConsulta>>> callTipoConsultaByIdInListRx(List<Long> idsTipoConsultaRetornados) {
		
		if (idsTipoConsultaRetornados == null || idsTipoConsultaRetornados.isEmpty()) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		
		return tipoConsultaClient.findByIdInListRx(idsTipoConsultaRetornados);
	}
	
	
	public List<Agendamento> list(
    		@Nullable List<Long> idsAgendamento
    		) {
    		
    		List<Agendamento> findList = agendamentoClient.find(idsAgendamento);
    		
	    	Page<Agendamento> find = Page.of(findList, Pageable.UNPAGED, findList.size()) ;
	    	
	    	if (find.getTotalSize() <=0) {
	    		return Collections.emptyList();
	    	}
	    	
	    	List<Agendamento> it = find.getContent();
	    	
	    	List<String> idsUnidadesRetornados =  it.stream().map(p ->  String.valueOf(p.getUnidade().getId())).collect(Collectors.toList());
			List<Long> idsPacientesRetornados = it.stream().map(p -> p.getPaciente().getId()).collect(Collectors.toList());
			List<Long> idsTipoConsultaRetornados = it.stream().map(p -> p.getTipoConsulta().getId()).collect(Collectors.toList());
			
			//List<Long> idsTipoAtendimentoRetornados	= it.stream().map(p -> p.getTipoAtendimento() != null ? p.getTipoAtendimento().getId() : null).filter(Objects::nonNull).collect(Collectors.toList());
//			List<Long> idsTipoAtendimentoRetornados = it.stream().map(p -> p.getTipoAtendimento().getId()).collect(Collectors.toList());
	    	
			Flowable<List<Agendamento>> zip = Flowable.zip(
					callTipoConsultaByIdInListRx(idsTipoConsultaRetornados), 
//					callTipoAtendimentoByIdInListRx(idsTipoAtendimentoRetornados),
					callUnidadeByIdInListRx(idsUnidadesRetornados),
					callPacienteByIdsRx(idsPacientesRetornados),
					(tCon, /*tAte,*/ unid, paci) -> {
						
						String nomePaciente = "";
						String cpfPaciente = "";
						
						final Map<Long, Paciente> listaPacientesIdMapRetornados = paci.getBody().orElse(Collections.emptyList()).stream().collect( Collectors.toMap(Paciente::getId, p -> p)  );
						final Map<Long, Unidade> listaUnidadesIdMapRetornados = unid.getBody().orElse(Collections.emptyList()).stream().collect( Collectors.toMap(Unidade::getId, p -> p)  );
						final Map<Long, TipoConsulta> listaTipoConsultaIdMapRetornados = tCon.getBody().orElse(Collections.emptyList()).stream().collect( Collectors.toMap(TipoConsulta::getId, p -> p)  );
//						final Map<Long, TipoAtendimento> listaTipoAtendimentoIdMapRetornados = tAte.getBody().orElse(Collections.emptyList()).stream().collect( Collectors.toMap(TipoAtendimento::getId, p -> p)  );
						
						if (!StringUtils.isEmpty(nomePaciente) || !StringUtils.isEmpty(cpfPaciente)) {
							if (listaPacientesIdMapRetornados.isEmpty()) {
								return Collections.emptyList();
							}
						}
						
						it.forEach(ag -> {
							
							Paciente paciente = listaPacientesIdMapRetornados.get(ag.getPaciente().getId());
							Unidade unidade = listaUnidadesIdMapRetornados.get(ag.getUnidade().getId());
							TipoConsulta tipoConsulta = listaTipoConsultaIdMapRetornados.get(ag.getTipoConsulta().getId());
							
//							if (ag.getTipoAtendimento()  != null && ag.getTipoAtendimento().getId() != null) {
//								TipoAtendimento tipoAtendimento = listaTipoAtendimentoIdMapRetornados.get(ag.getTipoAtendimento().getId());
//								ag.setTipoAtendimento(tipoAtendimento);
//							}

							ag.setPaciente(paciente);
							ag.setUnidade( unidade );
							ag.setTipoConsulta(tipoConsulta);
							
							
						});
						
						
						return it;
						
					});
			
			return zip.blockingLast();
			
			
			
//			List<Unidade> findByIdInList = unidadeClient.findByIdInList(idsUnidadesRetornados);
//			List<Paciente> byIds = pacienteClient.byIds(idsPacientesRetornados);
//			String nomePaciente = "";
//			String cpfPaciente = "";
//			final Map<Long, Paciente> listaPacientesIdMapRetornados = byIds.stream()
//					
//					.filter(p -> {
//						
//						if (!StringUtils.isEmpty(nomePaciente)) {
//							return p.getNome().toLowerCase().indexOf(nomePaciente.toLowerCase()) > -1;
//						}else {
//							return true;
//						}
//						
//					})
//					.filter(p -> {
//						
//						if (!StringUtils.isEmpty(cpfPaciente)) {
//							return p.getCpf().toLowerCase().indexOf(cpfPaciente) > -1;
//						}else {
//							return true;
//						}
//						
//					})
//					.collect( Collectors.toMap(Paciente::getId, p -> p)  );
//			
//			
//			final Map<Long, Unidade> listaUnidadesIdMapRetornados = findByIdInList.stream().collect( Collectors.toMap(Unidade::getId, p -> p)  );
//			
//			if (!StringUtils.isEmpty(nomePaciente) || !StringUtils.isEmpty(cpfPaciente)) {
//				if (listaPacientesIdMapRetornados.isEmpty()) {
//					return Collections.emptyList();
//				}
//			}
//			
//			
//			List<Agendamento> collect = it.stream().map(a -> {
//				
//				Paciente paciente = listaPacientesIdMapRetornados.get(a.getPaciente().getId());
//				
//				if (paciente == null) return null;
//				
//				Agendamento ag = new Agendamento();
//				ag.setDataCadastro(a.getDataCadastro());
//				ag.setDataConsulta(a.getDataConsulta());
//				ag.setId(a.getId());
//				ag.setPaciente(paciente);
//				ag.setUnidade( listaUnidadesIdMapRetornados.get(a.getUnidade().getId()) );
//				ag.setStatusAgendamento(a.getStatusAgendamento());
//				
//				return ag;
//			}).collect(Collectors.toList());
//			
//			 collect.removeAll(Collections.singleton(null));
//			
//			Page<Agendamento> pageOf = Page.of(collect, find.getPageable(), find.getTotalSize());
////			cache.addAgendamento(nomePaciente + cpfPaciente + unidadeId + dataDe + dataAte + listaStatus + max + offset + sort + order, pageOf);
//			return collect;
//    	}
	}
	
	private Flowable<HttpResponse<List<Unidade>>> findByGrupoEmpresaRx(List<String> grupoEmpresa) {
		
		if (null != grupoEmpresa  && !grupoEmpresa.isEmpty()) {
			return unidadeClient.findByGrupoEmpresaIdInListRxH(grupoEmpresa);
		}
		
		return Flowable.just(HttpResponse.ok(Collections.emptyList()));
	}
	
	private Flowable<HttpResponse<List<Unidade>>> findByEmpresaRx(List<String> empresa) {

		if (null != empresa  && !empresa.isEmpty()) {
		
			return unidadeClient.findByEmpresaIdInListRxH(empresa);
			
		}

		return Flowable.just(HttpResponse.ok(Collections.emptyList()));
	}
	
	private List<Unidade> findByGrupoEmpresa(List<String> grupoEmpresa) {
		
//		System.out.println("findByGrupoEmpresa");
//		System.out.println("***********************************************************************");
//		System.out.println("***********************************************************************");
//		System.out.println("***********************************************************************");
//		System.out.println("***********************************************************************");
//		System.out.println(new Gson().toJson(grupoEmpresa));
		if (null != grupoEmpresa  && !grupoEmpresa.isEmpty()) {
		
			
			List<Unidade> findByGrupoEmpresaIdInList = unidadeClient.findByGrupoEmpresaIdInList(grupoEmpresa);
			
			return findByGrupoEmpresaIdInList;
			
		}
		
		
		
		return Collections.emptyList();
	}
	
	private List<Unidade> findByEmpresa(List<String> empresa) {
		
//		System.out.println("findByEmpresa");
//		System.out.println("***********************************************************************");
//		System.out.println("***********************************************************************");
//		System.out.println("***********************************************************************");
//		System.out.println("***********************************************************************");
		
		if (null != empresa  && !empresa.isEmpty()) {
		
			List<Unidade> findByGrupoEmpresaIdInList = unidadeClient.findByEmpresaIdInList(empresa);
			
			return findByGrupoEmpresaIdInList;
			
		}
		
		
		
		return Collections.emptyList();
	}
	
	private String getDateDe(String dataDe) {
		LocalDateTime dataIni = null;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		if (dataDe != null) {
			dataDe += " 00:00";
			dataIni = LocalDateTime.parse(dataDe, formatter);
		}
		
		if (dataIni == null) {
			dataIni = LocalDateTime.now().minusMonths(2L);
		}
		return dataIni.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
	
	private String getDateAte(String dataAte) {
		
		LocalDateTime dataFim = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		
		if (dataAte != null) {
			dataAte += " 23:59"; 
			dataFim = LocalDateTime.parse(dataAte, formatter);
		}
		
		if (dataFim == null) {
			dataFim = LocalDateTime.now();
		}
		
		return dataFim.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
	
	private Timestamp getDateDeTimestamp(String dataDe) {
		LocalDateTime dataIni = null;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		if (dataDe != null) {
			dataDe += " 00:00";
			dataIni = LocalDateTime.parse(dataDe, formatter);
		}
		
		if (dataIni == null) {
			dataIni = LocalDateTime.now().minusMonths(2L);
		}
		return Timestamp.valueOf(dataIni);
	}
	
	private Timestamp getDateAteTimestamp(String dataAte) {
		
		LocalDateTime dataFim = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		
		if (dataAte != null) {
			dataAte += " 23:59"; 
			dataFim = LocalDateTime.parse(dataAte, formatter);
		}
		
		if (dataFim == null) {
			dataFim = LocalDateTime.now();
		}
		
		return Timestamp.valueOf(dataFim);
	}
	
	public byte[] combinarFiltrosGerarStreamPDF(
			List<String> grupoEmpresaId, 
			List<String> empresaId, 
			List<Long> unidadeId,
			String dataDe, 
			String dataAte, 
			List<Long> listaStatusId) {
		
		return 
				Flowable.zip(
						this.findByGrupoEmpresaRx(grupoEmpresaId).subscribeOn(Schedulers.io()),
						this.findByEmpresaRx(empresaId).subscribeOn(Schedulers.io()),
						(uGrupoEmpresa1, uEmpresa) -> {
							
							List<Unidade> listauGrupoEmpresa1 = uGrupoEmpresa1.getBody().orElse(Collections.emptyList());
							List<Unidade> listauEmpresa = uEmpresa.getBody().orElse(Collections.emptyList());
							
							return agendamentoClient.find(null, somarUnidades(unidadeId, listauGrupoEmpresa1,listauEmpresa), getDateDe(dataDe), getDateAte(dataAte), listaStatusId, -1, null, null, null);
						})
						.flatMap(agendamentoPageResponse -> {
							
							if (agendamentoPageResponse.getTotalSize() <=0) {
					    		return Flowable.just( Page.<Agendamento>of(Collections.emptyList(), agendamentoPageResponse.getPageable(), 0) );
					    	}
							
							return  
									Flowable.just(
											this.list(
													agendamentoPageResponse.getContent().stream().map(p -> p.getId()).collect(Collectors.toList())
							));
						})
						.map(fullAgendamentos -> {
							return generatePdfFromAgendamento(fullAgendamentos, dataDe, dataAte);
						}).blockingFirst();
	}


	private byte[] generatePdfFromAgendamento(Iterable<Agendamento> fullAgendamentos, String dataDe, String dataAte) throws IOException {
		
		return TemplateToPdfConverterUtil.generatePdf(
    			TemplateModel.<AgendamentoRel>of(geraRelatorioDomain(fullAgendamentos, dataDe, dataAte)), 
    			TEMPLATE_BODY_RELATORIO_AGENDAMENTO, 
    			TEMPLATE_CSS_RELATORIO_AGENDAMENTO);
		
	}


	private List<Long> somarUnidades(List<Long> unidadeId, List<Unidade> uGrupoEmpresa1,
			List<Unidade> uEmpresa) {
		
		
		List<Unidade> unidades_ = new ArrayList<>();
		
		if (uGrupoEmpresa1 != null && !unidades_.isEmpty()) {
			unidades_.addAll(uGrupoEmpresa1);
		}
		
		if (uEmpresa != null && !unidades_.isEmpty()) {
			unidades_.addAll(uEmpresa);
		}

		List<Long> unidadesCalculada = unidades_.stream().map(p -> p.getId()).collect(Collectors.toList());
		
		if (unidadeId != null) {
			unidadesCalculada.addAll(unidadeId);
		}
		
		if (unidadesCalculada.isEmpty()) {
			unidadesCalculada = null;
		}
		return unidadesCalculada;
	}


	private AgendamentoRel geraRelatorioDomain(Iterable<Agendamento> agendamentos,
			String dataDe, 
			String dataAte) {
		
		Map<Integer, List<Agendamento>> mapaPagina = agruparAgendamentosPorPagina(agendamentos, 14);//new HashMap<Integer, List<Agendamento>>();
		
		List<Pagina<Agendamento>> paginas = generateListPaginaByMap(mapaPagina);
		
		if (!paginas.isEmpty()) {
			paginas.get(paginas.size() - 1).setUltimaPagina(true);
		}
		
		AgendamentoRel rel = new AgendamentoRel();
		
		rel.setPaginas(paginas);
		
		rel.setDataDe(getDateDeTimestamp(dataDe));
		rel.setDataAte(getDateAteTimestamp(dataDe));
		
		List<Agendamento> collect = StreamSupport.stream(agendamentos.spliterator(), false).collect(Collectors.toList());

		rel.setTotalCadastradas(collect.size() + "");
		rel.setTotalCanceladas(collect.stream().filter(a -> a.getStatusAgendamento().getId() == StatusAgendamentoEnum.CANCELADO.getCodStatus()).collect(Collectors.toList()).size() + "");
		rel.setTotalFinalizadas(collect.stream().filter(a -> a.getStatusAgendamento().getId() == StatusAgendamentoEnum.CONFIRMADO.getCodStatus()).collect(Collectors.toList()).size() + "");
		rel.setTotalPendentes(collect.stream().filter(a -> a.getStatusAgendamento().getId() == StatusAgendamentoEnum.PENDENTE.getCodStatus()).collect(Collectors.toList()).size() + "");
		
		return rel;
	}


	private List<Pagina<Agendamento>> generateListPaginaByMap(Map<Integer, List<Agendamento>> mapaPagina) {
		return mapaPagina.keySet().stream().map(numeroPagina -> {
			
			Pagina<Agendamento> p = new Pagina<Agendamento>();
			
			if (1 == numeroPagina) {
				p.setPrimeiraPagina(true);
			}
			
			p.setNumeroPagina(numeroPagina + "");
			p.setDominios(mapaPagina.get(numeroPagina));
			
			return p;
			
		}).collect(Collectors.toList());
	}


	private Map<Integer, List<Agendamento>> agruparAgendamentosPorPagina(Iterable<Agendamento> agendamentos, int qntPorPagina) {
		
		int i = 1;
		int pagina = 1;
		int size = 0;
		Map<Integer, List<Agendamento>> mapaPagina = new HashMap<Integer, List<Agendamento>>();
		
		for (Agendamento agendamento : agendamentos) {
			
			if (mapaPagina.get(pagina) == null) {
				mapaPagina.put(pagina, new ArrayList<Agendamento>());
			}
			
			mapaPagina.get(pagina).add(agendamento);
			
			if (i == qntPorPagina) {
				pagina++;
				i = 0;
			}
			
			i++;
			size++;
		}
		return mapaPagina;
	}


	public List<Agenda> obterAgendas(String dataReferencia) {
		return agendamentoClient.obterAgendas(dataReferencia);
	}
	
	public Agendamento save(Agendamento agendamento) {
		
		Perfil usuarioLogado = usuarioClient.show(securityHelper.getUsuarioId()).get().getPerfil();
		
		// INFORMA O USUÁRIO E A DATA DO CADASTRO.
		agendamento.setUsuarioCadastro(usuarioLogado);
		agendamento.setDataCadastro(Timestamp.valueOf(LocalDateTime.now()));
		
		return agendamentoClient.save(agendamento);
	}
	
	public Agendamento update(Agendamento agendamento) {
		
		Perfil usuarioLogado = usuarioClient.show(securityHelper.getUsuarioId()).get().getPerfil();
		
		StatusAgendamentoEnum statusAgendamento = StatusAgendamentoEnum.getByDescricao(agendamento.getStatusAgendamento().getDescricao());
		
		if(StatusAgendamentoEnum.CONFIRMADO.equals(statusAgendamento)) {
			System.out.println("############### agendamento CONFIRMADO");
			
			// INFORMA O USUÁRIO E A DATA DA CONFIRMACAO.
			if(agendamento.getDataConfirmacao() == null && agendamento.getUsuarioConfirmacao() == null) {
				agendamento.setUsuarioConfirmacao(usuarioLogado);
				agendamento.setDataConfirmacao(Timestamp.valueOf(LocalDateTime.now()));				
			}
			
		} else if (StatusAgendamentoEnum.CANCELADO.equals(statusAgendamento)) {			
			System.out.println("############### agendamento CANCELADO");
			
			// INFORMA O USUÁRIO E A DATA DA CONFIRMACAO.
			if(agendamento.getDataCancelamento() == null && agendamento.getUsuarioCancelamento() == null) {
				agendamento.setUsuarioCancelamento(usuarioLogado);
				agendamento.setDataCancelamento(Timestamp.valueOf(LocalDateTime.now()));
			}
						
		} else {
			System.out.println("############### Status do Agendamento - OUTROS");
			System.out.println(statusAgendamento.getDescStatus());
		}
		
		return agendamentoClient.update(agendamento);
	}
}
