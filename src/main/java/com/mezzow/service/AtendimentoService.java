package com.mezzow.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.mezzow.domain.Agendamento;
import com.mezzow.domain.Atendimento;
import com.mezzow.domain.ModeloPosConsulta;
import com.mezzow.domain.Perfil;
import com.mezzow.domain.Unidade;
import com.mezzow.domain.enums.StatusAtendimentoEnum;
import com.mezzow.rest.client.AgendamentoClient;
import com.mezzow.rest.client.AtendimentoClient;
import com.mezzow.rest.client.ModeloPosConsultaClient;
import com.mezzow.rest.client.PacienteClient;
import com.mezzow.rest.client.PerfilClient;
import com.mezzow.rest.client.RegistroPreAtendimentoClient;
import com.mezzow.rest.client.TipoConsultaClient;
import com.mezzow.rest.client.UnidadeClient;
import com.mezzow.util.Pagina;
import com.mezzow.util.TemplateModel;
import com.mezzow.util.TemplateToPdfConverterUtil;
import com.mezzow.util.VelocityUtil;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Sort;
import io.micronaut.http.HttpResponse;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

@Introspected
@Singleton
public class AtendimentoService {
	
	//TODO INCLUIR E RECUPERAR DO BANCO
	// ADD unidade_id null
	private static final String TEMPLATE_BODY_DOWNLOAD_RELATORIO_MEDICO = "<div id=\"header\" style=\"width:100%;\">"
			+ "#if(${model.hasLogo})"
			+ "		<img class=\"img_logo\" style=\"max-height:400px;\" src=\"${model.linkLogo}\"/>"
			+ "#end"
			+ "	<div class=\"nome-hospital\">"
			+ "		<p>&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>"
			+ "		<p class=\"link-titulo\"></p>"
			+ "	</div>"
			+ "	<div id=\"titulo_header\" >"
			+ "		<img style=\"max-height:100px;\" src=\"https://storage.googleapis.com/mezzow-visclin/logo/opty.png\"/>"
			+ "	</div>"
			+ "	<div id=\"dados\">"
			+ "		<table border=\"0\" style=\"border:none;\">"
			+ "			<tr>"
			+ "				<td>"
			+ "					<strong>Paciente:</strong> ${model.dominio.registroPreAtendimento.agendamento.paciente.nome}"
			+ "				</td>"
			+ "				<td>"
			+ "					<strong>Sexo:</strong> ${model.dominio.registroPreAtendimento.agendamento.paciente.sexo}"
			+ "				</td>"
			+ "			</tr>"
			+ "			<tr>"
			+ "				<td>"
			+ "					<strong>Nasc.:</strong>"
			+ "					${dateTool.format('dd/MM/yyyy',$model.dominio.registroPreAtendimento.agendamento.paciente.dataNascimento)}"
			+ "				</td>"
			+ "				<td>"
			+ "					<strong>Data Atendimento : </strong>"
			+ "					$dateTool.format('dd/MM/yyyy HH:mm',${model.dominio.dataCadastro})"
			+ "				</td>"
			+ "			</tr>"
			+ "		</table>"
			+ "	</div>"
			+ "	<div id=\"titulo_laudo\">"
			+ "		<center>"
			+ "			<h3>${model.dominio.titulo}</h3>"
			+ "		</center>"
			+ "	</div>"
			+ "</div>"
			+ "<div id=\"footer\" style=\"width:100%;\">"
			+ "	<div id=\"rodape\">"
			+ "		<center> R. do Rocio, 423 - Conjunto 202 - Vila Olímpia, 04552-000 São Paulo - SP</center>"
			+ "		<center>Tel: (11) 3842-4480</center>"
			+ "	</div>"
			+ "</div>"
			+ "<br></br>"
			+ "<div id=\"conteudo\">"
			+ "    ${model.dominio.conteudo}       "
			+ "</div>";
	
	//TODO INCLUIR E RECUPERAR DO BANCO
	// ADD unidade_id null
	private static final String TEMPLATE_CSS_DOWNLOAD_RELATORIO_MEDICO = "@page { size: A4 portrait; margin-top:68.05mm; margin-bottom: 70.05mm; margin-right: 15.05mm; @top-left { content: element(header); width: 100%; } /*@top-right { content: element(titulo_header); }*/ @bottom-center { content: element(footer); width: 80%; } @bottom-right { counter-increment: page; content: \"Pag.: \"counter(page) \" de \"counter(pages); font-size: 10px; } padding: 0; } @page :left { margin-left: 15.05mm; } #header { position: running(header); } #dados{ margin-top:45px; } .quebra_pagina{ page-break-after:always; } #header img{ margin-top: 5px; max-height: 250px; height: 100px; } p {margin:0; padding:0;} #footer { margin:0px; margin-bottom:0px; font-size: 14px; font-family: Arial, \"Helvetica Neue\", Helvetica, sans-serif; position: running(footer); } #rodape{ border-top: 1px solid #ccc; padding: 15px 0px 0px 0px; font-weight: bold; } #footer p { margin:0; padding:0; } #pagenumber:before { content: counter(page); } #pagecount:before { content: counter(pages); } html { } body { margin: 0px; padding: 0px; background-color: white; } img { border: 0px; } p.figure { border: 0px solid red; float: left; clear: both; padding: 0px 15px; margin: 0px; width: 200px; page-break-inside: avoid; } p.figure img { text-align: center; } p.figure b { text-align: center; font-style: italic; font-weight: normal; } table { font-family: Arial, \"Helvetica Neue\", Helvetica, sans-serif; font-size: 14px; width: 100%; border-bottom: 1px solid #CCCCCC; border-top: 1px solid #CCCCCC; padding: 10px 5px; color: #000; } #titulo_laudo { font-family: Arial, \"Helvetica Neue\", Helvetica, sans-serif; padding: 1px 5px; line-height: 0px; } #titulo_laudo h3{ font-size: 16px; text-transform: uppercase; text-decoration: underline; } #conteudo { font-family: Arial, \"Helvetica Neue\", Helvetica, sans-serif; font-size: 15px; padding: 0px 5px 20px 5px; line-height: 20px; margin-top: -20px; } #conteudo h3{ text-transform: uppercase; } #conteudo h4{ font-family: Arial, \"Helvetica Neue\", Helvetica, sans-serif; padding: 0; margin: 15px 0 5px 0; } #assinaturas{ margin-bottom: 20px; font-family: Arial, \"Helvetica Neue\", Helvetica, sans-serif; font-size: 14px; } #assinaturas table{ border: none; } #assinaturas img { height: 60px !important; width: 200px !important; } #titulo_header{ position: running(titulo_header); font-family: Arial, \"Helvetica Neue\", Helvetica, sans-serif; width: 100%; float: right; text-align: right; margin-top: -75px; } #titulo_header h5{ padding: 0; font-size: 12px; line-height: 18px; margin: 0; } .nome-hospital{ padding-left: 100px; padding-top: 20px; font-family: Arial, \"Helvetica Neue\", Helvetica, sans-serif; text-align: center; font-weight: bold; font-size: 22px; } .link-titulo{ margin-top: 10px; font-family: Arial, \"Helvetica Neue\", Helvetica, sans-serif; font-size: 14px; } .img_logo{ position: absolute; } #titulo_header img{ margin-top: 25px; height: 50px; }";
	
	
	private static final String TEMPLATE_CSS_RELATORIO_ATENDIMENTO = 
			
			"table {\r\n"
			+ "  margin: 0;\r\n"
			+ "  padding: 0;\r\n"
			+ "  width: 100%;\r\n"
			+ "  table-layout: fixed;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "table caption {\r\n"
			+ "  font-size: 1.5em;\r\n"
			+ "  margin: .5em 0 .75em;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "table tr {\r\n"
			+ "  background-color: #f8f8f8;\r\n"
			+ "  border: 1px solid #ddd;\r\n"
			+ "  padding: .35em;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "table tr,\r\n"
			+ "table td {\r\n"
			+ "  padding: .625em;\r\n"
			+ "  text-align: center;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "table tr {\r\n"
			+ "  font-size: .85em;\r\n"
			+ "  letter-spacing: .1em;\r\n"
			+ "  text-transform: uppercase;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "@media screen and (max-width: 600px) {\r\n"
			+ "  \r\n"
			+ "  #container { width: 400px; }\r\n"
			+ "  \r\n"
			+ "  table {\r\n"
			+ "    border: 0;\r\n"
			+ "  }\r\n"
			+ "\r\n"
			+ "  table caption {\r\n"
			+ "    font-size: 1.3em;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table tr {\r\n"
			+ "    border: none;\r\n"
			+ "    clip: rect(0 0 0 0);\r\n"
			+ "    height: 1px;\r\n"
			+ "    margin: -1px;\r\n"
			+ "    overflow: hidden;\r\n"
			+ "    padding: 0;\r\n"
			+ "    position: absolute;\r\n"
			+ "    width: 1px;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table tr {\r\n"
			+ "    border-bottom: 3px solid #ddd;\r\n"
			+ "    display: block;\r\n"
			+ "    margin-bottom: .625em;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table td {\r\n"
			+ "    border-bottom: 1px solid #ddd;\r\n"
			+ "    display: block;\r\n"
			+ "    font-size: .8em;\r\n"
			+ "    text-align: right;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table td::before {\r\n"
			+ "    content: attr(data-label);\r\n"
			+ "    float: left;\r\n"
			+ "    font-weight: bold;\r\n"
			+ "    text-transform: uppercase;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table td:last-child {\r\n"
			+ "    border-bottom: 0;\r\n"
			+ "  }\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "body {\r\n"
			+ "  font-family: \"Open Sans\", sans-serif;\r\n"
			+ "  line-height: 1.25;\r\n"
			+ "}";
			
			
	private static final String TEMPLATE_BODY_RELATORIO_ATENDIMENTO = 
			"<div id=\"container\">\r\n"
			+"#if(${model.dominio.paginas.isEmpty()}) "
			+ "	<table >\r\n"
			+ "	  <caption style=\"text-align: left;\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHEAAABMCAIAAADV+M/0AAAS5klEQVR4nO2daZQcV3WAv/equns2zUgzoxnti6WRInnBWPYBwho7iZcYG+KACRCDY2xsjg/LOYDDGnEMGCMOwYGDFzAHJ2CFRTgxS7ATESCOlxhjQLJkWbs02jUazUg9S1e99/LjVlVX9/TM9IzahB99f0it7le33nL3e9+Vcs5RIxBUSqmJxgBgrNEoQGud/nU0KAC7e/uAvb2HDx87CfQNnAbCMPR9H5jZ1gzMnz1r8YIuYOn8biCX9UveYixglPO1N/btE82vujETg558SB2mCKqGdJoG51wZwYZhCHieR4qWd+0/Avz7L34NbHxi02+27gYOHO0HCqfyGAsoWzJDpxVAxmtoagLmds8Czl+19JJXnAtc/toLgLMWdifTICZb7XvpLyfmpzOBF2tPE7DWAk7hKQ0EoQUe3vjU3d/+KfDLp54Dwr5BwHla5TKAy/gA2ldaFh+h0g7AALIvsuNBCLjRQBkL+B2twGtevgq45a1XXHXJy4CMrwHjrHIW0NoH3Jkx+ARQ5/3aw4tIp8YYQGthdjY88gTwibu+Azz/661KeYBrzgE64wPaKWsNYBWAMwplAZwGos8lcwfwYl0nT9kgBFR+FHDO/NEFq4Db33ctcM2lr5DFCuuIFHox4MXa09Aa0bm79h4Gbll736M/eRzwMj7gWhojBWssULZbygG4aXFmxHeejjDlhwFXCIE/v+KP7/nUjcDSRXNlhmJ16FrLgDrv1x5qSacxZwEoj/UPPwa8+7Z/BPInBjKz2pIxoR3DyBXQ6Qr8XumtkFJkybOAUr4uqvigf6ClYyZwz+duBd521WtC44hpuoZmQM321DknqIShbvvCA5+/80FAz2gEVC5jAll8pW0SWahdPCeYKu9PcgAO8HzfjQaAPTUMfPi2t975wXcARowBVK22tc77tYdq6XQCOzmyq2Mj/8aP3g3cf9+G7Ox2IHAh4KxKRiOIJiLGM/YPhfe1Jb045zxRSloDwbETN9z0JuDrd9wMWGvLHOVpw9R4v+JaA2eBjNLv/thXgfvu2QBkuzqDIGDsrlUUf6p0x5PPFZlR3CpPc2ZSK+v7wZE+4MZbrgHu/czNRhCrM93ZOu/XHvzJh6SgjGxEb2Y8Ddz+5e/ed89DQLarAyiEQTy6lLiF9MposCLFFQe4+A9xHyyRwJlMRCiVxqzij8I6hTDMdncAMu0Fczo/fus1yaJ8b/qSZ/p63xgjrsi/bXwaeMM71vrNjUAoCMdFOxVZWVFQ1BCUAnylADM08tA3/x64+pKLSK1uGlDn/drDdOg0sQGOHB8ALrzivUDv4ROqMQc4CRyVWYvjKZxSSiwxBsrG28T9nwrxlvsCFRfrARRG589tB5750V1Ad2fbtEOCU5OnAnEMQn1i3T8BvXsOA15HqwkNRGyd8TxDhTmJXW8V1kpk0xR/U0r7GsCGgCnYaI+VApTv+coDApvsbIkYicb6GhBLzYSOwCS/Ke3hFZMLxth4iw3gNeUO7DwMyKLu/dyt8p5pCIA679cepsb7QqFiGj/57PZXvPFDANkMjGGrwSEht4izYpAzdM7RmAVcY46YcOzoKPk8oBqagKZZLVk/AwyPFICRgVMUQsCb0QRY33OpoIHney40gDs1TCydvBnNTS1NxEr89MhoMJgHGBkBvOZmm/MBF+cRJEBlgwB44qF1L3/pytSSp0B80+F92Zl19/5AjQSAasgBNuZiUaPvvOHqWTNbARcGgCHysmS1TQ25pzftAB792dOAHQmAuUvmvvNNbwaueN0aYNmiOZmsDwwPjQB7Dx7/3iOPA/d/66dAfmiEhhyglQLsQN7LZYHL3vBq4C2XvQo4/5yzOttbgYxSwOmh4X2H+oBHH/stcO+3fnL0cB+gmhsBZ63zNKBPh8C6e7///Xs+BsSRyCnsaZ33aw/V8n6aBX6zdS9w4ZXvNzqlf5SiEADdczqAHRu/2iLnX6o9AxMCGc//2J3/DHz2s/cDH/zo3wIfv+Wv2lqbxj4STQCnUMCmbXuBy6/7xMFjAwD5YeCVr13zlU/dBJy/amkphhIqS2M+1j949Q2fBp743+cA1dwYKUylAM/yqx/9A/CSVYship0rryoSrIr3nXMSjRCUDz70M8AMDnmdrYCoe1/pMDTAsoXdQEtzYyEwgI7cFxs9LUfosWV3L/Cd9Z8G3nzFq4DQmtAaYg2eiDAXpVKseDjnrVwM3L/u/Zdf/UHgAx/6G+CLH71eBkt2Fq0AHccaxAxwChHBxgLMntX6wBffD5x/5QeAoeFR0fGepwHTNyjLPH/V9UCoHOBjraRqirGZCrtc5/3aQ1W8n8TxJJN8zqW3Aju277cNWfkZyPp+4fgAcMO7Xg98/Y5bhejKKkEERgvh/kNHgOWL5wMy0lO6egM7PzTy8Mangbdc+SqEBk0xc1eNuR6EVuyBa9+3Dvjev/xHpn0mEDoLMFzo6VkIbH7kK8TZbIuLLeaJMFfL+4Ll15t3AC/s7AVoyMZ5EgUYjMYC5/UsmRRhLuuftXge8SEpHGCVFZ4dE22roHmbGnN//fpXE0fpnXVi+xvJm3pi+ZdHcdJ7rbRTSgMXntsDfG/9xjhf62R1skxZ8svOXyH7oKuIBNZ5v/YwNfv0f57ZAqihAqAbc0aOXdxNq1wuC6zqWSCD1YRCRVg1E9XbxOpI/kxyAePbhkopqR8QovNiCeMqEYlKPTX21+6OdgCt0nUunqdNfhR44pktpOi0moDa1Pb0mU07iRWxdnGZjeQdjcm2NAE9S+aNXYDFpXQlFiWCb/P2/cD6h38JnDw19Ja/eCXw6otWxwuYiI2Sagzgx7945tHHfkMsN95+9euAC85eLMKpshfkSgyscuQOozXw9KadyZcTU0nx2apG1WEqMAmdlp3h9j2HATIeSew5pkcXBAsWdAPz5nSkvxfQODm/2M5V//n474DLr1sLhAODANY9sP4RYPPP7gYWz589ga9tnBWS/MBnvgF86YvrMxkPCIZHgB/88L+Bzf91b0tDBTMgWpQzsvy+gUFAOWyKr0PnyOhkyek0e3pzKgqTqnhfay2BjIPHTwBEQrB0TwuhGEbZjEeFNGSUvE8KaW67/RtAmB8GGrs6BV2+bxDYsm0PsGT+bFuJK6NIo9bizt113wbA62h1vg9oOwM40DcAHDp4tOes+YBztjjR5O/YCew9dJwx1KOdM75KljwyGgCNDdkSy2Eci6rO+7WHanVUPj8MnM6PAqJIk3ONjiu0q1fMT8bbMRrYOkusQx5/Zuuzz+0EVGszMFwoAJ5SOucDM2Y0R8/oCoRgbQhonf3xxicBN2QAmnQYBhA5no1NTUBzS0OMxzEmBeYpLZbvtl29AL5Ok6oBlJ8sOZ/PA43i40wGk8lThcxmeLQAjBZCmU56TGSCaHXuikUToRI329fAz598zg0XAK+5ARDWNM55uQzQ1dEqj6hK+UDlyZztU7/dAeBHIUQx4AkCYE5nG9DV3hbj0Wk8iakmAYrtuw8DKpMpNwA0xFcMhkclFoxRULJrFay9Ou/XHqYYky7P3GmSBFFDZvXyhckvWpcnndMS/XfP74n40aYSdoWga95sYMGczrGPkETvlWSTzAt7DwFKClpjFSw1vWIj+75f2XKIsR48Mgj0HjkBuIyHncT+dM6NkUb1uNTvBSajUzk6rRpzWSCXyQIj+QKAF4vSMARaZ7UuXTgneU6PqUJJlyBs3XWQTIbEERR6HA2WL5oDNDc1IFqujE5FuFsHHDo6sP9gH0TGsnNoIaHAAGcvj/xjMYdVqX0qaR7P83bs3Y9kukA1N7gyOjUOyGV8oCmbjTBEwniiPZtkT5N5SNB+RksDMNA/CHh48msYGGDR/M7uzrbUoxEHyPMJbx7vGwD2HTgqexqZe8KboV29fEHypXVWl90Yc1GwGti1/8jQwGlANWUBrJMItPE0cE6ptiyXIfG/xDNmNAT8Fi+UEkQ5OYXDJQtvammceKPSUOf92kNVdGqtbWjIAnM72oDe3YcAm8tGx18oACuXzp3gVofFyQWRnfuPACf7B8kVM9gmdsnOXrEoeSljFIZL8d3WHfvUaAD4zQ1AgAmsAWjIAKt6FjK+75jAFqHTBH3JWEVogHmd7SSWqXWqkslcBtXGpOWD6NOnn3gO8JWKHGRrgXN7lsTZ5grK2jgnRu1WiWePFDKNOSAwpog/55/TU7QcxgSBStyIzdv3xnExGR1tQVt7G7B0UTeglNKVNHliBmzZsQ9QmWJMIAEvlmnLF3cnXxpnvVS5wnhnVuf92kN1dBofxppzlwEPPvgoYFWsYbwMcO6KJTKm7OjKGPm5bb0ANi5GF9IOLdDYNmPZoqLlMCbxq3VkHStg6/ZePJ9YbiilXBACi+Z2ALMTDyo1GVcaSMsPjezYdxRwWcFTumStxOa58LzlyZdGlRTVjCdY/GrSYcmvr1lzNkBTjnQNV3MO6FkyJx5cYuwLfj/+56YX9oA41zLYAgQhsGDp3HlzO4ovdSUmi3FWqvHFP961/whZv4hf6aAQAquXLUzebYzxIjc6Sk9FLoBSwP6DfUeP9QNkfEDZEkvKGEtzBnjlmtXJl151Kcg679ce/GrSv0kI5yXnLANWLl8IbNuxD+UBXV0zgYXzZqcfKQ9Hai1XJnbEDqXENJXyARuMAMsXz5HEdXxRVbuolrXk4A8cOg4cONovt1SFuqyOKixXp7ScdUpHai2q24hyoloDL+w+YE4PA7qtGbAmcrsjzT5c6OlZBFxwzvLk+TG3KG1FoqzW3y9YSeJ7wFUXXwh8YfMulwNYNr+LuP0DY5zryJOBg0dOAAfEufY9Ip4FsGEAnLdyiTySEkcp8WVCuXG3c99hoHAqL7VjYnU45/AzwNkri9a+VlFKTkV74TkVJr9u3r6fMGCM3IuqU4cLUoQumf2yWoV4hpW5vM77tYdq6dRPkf1b33gxcNc3f1wYOA2sWBY516fzw4y19q0Fmpsatuw4CIwMniblXEfWvtbAiqVRwlUSFb6JpIcQRWBNa1MG+O3zewBGAr+lCQjEKzFWyHbJ/KI5OTIa4BmKly4xxgHNvg9sen4P2k9+Nc4JW0j5l9fa9PY3XCwrIOb6RGtOotKnVdOrgWtuvuMHG34OtC/ukl2LLo4XOaIYGNRay473n8zLnNKegcQ4ZnfOzGYygJUo+5hEgbz35EAeOHV6WKR5En4UOTi3sy2ZobXJ4pPIsU125HjfqdFC6lacc9r3Ads/APzlG/9kwz0fKVvypI4Z8WvqUGOY4t3IlDH75LPbpPZcjHMXGsbx0+VJyZpMcOdAWSv147iYAMs7TDiIs7ZlYebkXnQQRq+T4ZF7ropfxnhUxncqjkGRuhlT+P3VngvXaCAMC8DLX7ryxmv/DPjaN34EZLpmBlL/FVeplHTeSByVikcot4A9T2RxfPM8VrLRX1HVX2U88QGoXBbQVRCKSZBI+sD3TN9J4Mbrr5LVmTjMOimqMqjzfu2hBnfO1lz5PuDAoROqIQu4anpIkOK41Of4zlmFfKlyk11MFyRiulcRkStiFv0zUpA7Z7/64ZeAObNbJcA2jTtn1d/fh3jtAuX3Ta/7lD8jC4Q22axKqeTim8tkXDVzBcbvPzEW4cTNJ+LFyK2UID/8rw+sBa665CLA1u+b/kHBGfVDSV/Lvv3L3/3k2q8B2a5ZQCGwUj06SVuTMuKaOvGOi6oKhFnfB4Ij/cDate/65Huv5f/3rjlJMo4o7R71mbj7+0C2u7MgxTbxi6b9luncOC8+Mq78yfp+4WgfcNPN0mfiPSZVfXQmUOf92kP1dDpuGXjSY0YM43d95KvA/fdtyMxuB6IEr/FK1YUu6X42HoeWWgVn3hxB/FdfJT1mrgG+fsd7AGutWN9n3oGhlv2lEiEAfHjdN9d9fj3p/lJi5dio8KEmr4QJ9yBhfQAyvjKjhqS/1IfffueH3kZUGlHvL/WHDdOn07FBmigo5wCyWn374V8CN//dV4DTx/vlRlfcWy5O6Ud8XerX164Niq+LF6WCk/0t7e2M01sueeOZv7X2fSUl6GBxFftKaimVaGpg/L6S8dTUNIyqaH/G6St599qbgLMWzwFCazxVcjWtVlDn/dpD1b6pjJ4K6iRVByilSnvKbouKVkp6ykY0booNpSaeuyLOWI3TU9atXLMS+PR7/4B7yk5jZxO/QEdxwgr9pINjg4DKetIGxGWjCGl0R0/JnVQvksVJ6YOkTCVWPxqIGMlIP+mXnQ3c8rbLrvrTi4CMVFc4W3aLvVZrLIM679ceXvS+5wlYW4zCxYG5sl7yv3t2y27g8NGTwEh+WKqFhR4TM0PqXGwm09BcoZf8pa+9AFi+sDv96mlHl6cHL+KepjGXXfse9/+RKITA3gNHgN29Rw8c6ydO6gVhmPF9oL21GZjbNWvpgi5gcaX/RyKxPeRub4qRp9wwZhpQ5/3aw/8BEIjodALq754AAAAASUVORK5CYII=\" id=\"p1img1\"></img></caption>\r\n"
			+ "	  <thead>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\">TELECONSULTA ATENDIMENTO</th>\r\n"
			+ "		  <th scope=\"col\" colspan=\"3\">De $dateTool.format('dd/MM/yyyy',${model.dominio.dataDe}) até $dateTool.format('dd/MM/yyyy',${model.dominio.dataAte})</th>\r\n"
			+ "		  <th scope=\"col\">PAGINA 1</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\">ATENDIMENTO</th>\r\n"
			+ "		  <th scope=\"col\" colspan=\"3\">MEDICO</th>\r\n"
			+ "		  <th scope=\"col\">STATUS</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  </thead>\r\n"
			+ "	  <tbody>\r\n"
			+ "	  </tbody>\r\n"
			+ "   <tfoot>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\" colspan=\"5\" style=\"text-align: left;\" >TOTAL:</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\">&#160;</th>\r\n"
			+ "		 <th scope=\"col\">AGENDADOS: 0</th>\r\n"
			+ "		  <th scope=\"col\">ATENDIDOS: 0</th>\r\n"			
			+ "		  <th scope=\"col\" colspan=\"2\" >CANCELADOS: 0</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  </tfoot>"
			+ "	</table>\r\n"
			+"#else"		
			+"#foreach($pagina in ${model.dominio.paginas}) "
			+ "	<table >\r\n"
			+ "	  <caption style=\"text-align: left;\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHEAAABMCAIAAADV+M/0AAAS5klEQVR4nO2daZQcV3WAv/equns2zUgzoxnti6WRInnBWPYBwho7iZcYG+KACRCDY2xsjg/LOYDDGnEMGCMOwYGDFzAHJ2CFRTgxS7ATESCOlxhjQLJkWbs02jUazUg9S1e99/LjVlVX9/TM9IzahB99f0it7le33nL3e9+Vcs5RIxBUSqmJxgBgrNEoQGud/nU0KAC7e/uAvb2HDx87CfQNnAbCMPR9H5jZ1gzMnz1r8YIuYOn8biCX9UveYixglPO1N/btE82vujETg558SB2mCKqGdJoG51wZwYZhCHieR4qWd+0/Avz7L34NbHxi02+27gYOHO0HCqfyGAsoWzJDpxVAxmtoagLmds8Czl+19JJXnAtc/toLgLMWdifTICZb7XvpLyfmpzOBF2tPE7DWAk7hKQ0EoQUe3vjU3d/+KfDLp54Dwr5BwHla5TKAy/gA2ldaFh+h0g7AALIvsuNBCLjRQBkL+B2twGtevgq45a1XXHXJy4CMrwHjrHIW0NoH3Jkx+ARQ5/3aw4tIp8YYQGthdjY88gTwibu+Azz/661KeYBrzgE64wPaKWsNYBWAMwplAZwGos8lcwfwYl0nT9kgBFR+FHDO/NEFq4Db33ctcM2lr5DFCuuIFHox4MXa09Aa0bm79h4Gbll736M/eRzwMj7gWhojBWssULZbygG4aXFmxHeejjDlhwFXCIE/v+KP7/nUjcDSRXNlhmJ16FrLgDrv1x5qSacxZwEoj/UPPwa8+7Z/BPInBjKz2pIxoR3DyBXQ6Qr8XumtkFJkybOAUr4uqvigf6ClYyZwz+duBd521WtC44hpuoZmQM321DknqIShbvvCA5+/80FAz2gEVC5jAll8pW0SWahdPCeYKu9PcgAO8HzfjQaAPTUMfPi2t975wXcARowBVK22tc77tYdq6XQCOzmyq2Mj/8aP3g3cf9+G7Ox2IHAh4KxKRiOIJiLGM/YPhfe1Jb045zxRSloDwbETN9z0JuDrd9wMWGvLHOVpw9R4v+JaA2eBjNLv/thXgfvu2QBkuzqDIGDsrlUUf6p0x5PPFZlR3CpPc2ZSK+v7wZE+4MZbrgHu/czNRhCrM93ZOu/XHvzJh6SgjGxEb2Y8Ddz+5e/ed89DQLarAyiEQTy6lLiF9MposCLFFQe4+A9xHyyRwJlMRCiVxqzij8I6hTDMdncAMu0Fczo/fus1yaJ8b/qSZ/p63xgjrsi/bXwaeMM71vrNjUAoCMdFOxVZWVFQ1BCUAnylADM08tA3/x64+pKLSK1uGlDn/drDdOg0sQGOHB8ALrzivUDv4ROqMQc4CRyVWYvjKZxSSiwxBsrG28T9nwrxlvsCFRfrARRG589tB5750V1Ad2fbtEOCU5OnAnEMQn1i3T8BvXsOA15HqwkNRGyd8TxDhTmJXW8V1kpk0xR/U0r7GsCGgCnYaI+VApTv+coDApvsbIkYicb6GhBLzYSOwCS/Ke3hFZMLxth4iw3gNeUO7DwMyKLu/dyt8p5pCIA679cepsb7QqFiGj/57PZXvPFDANkMjGGrwSEht4izYpAzdM7RmAVcY46YcOzoKPk8oBqagKZZLVk/AwyPFICRgVMUQsCb0QRY33OpoIHney40gDs1TCydvBnNTS1NxEr89MhoMJgHGBkBvOZmm/MBF+cRJEBlgwB44qF1L3/pytSSp0B80+F92Zl19/5AjQSAasgBNuZiUaPvvOHqWTNbARcGgCHysmS1TQ25pzftAB792dOAHQmAuUvmvvNNbwaueN0aYNmiOZmsDwwPjQB7Dx7/3iOPA/d/66dAfmiEhhyglQLsQN7LZYHL3vBq4C2XvQo4/5yzOttbgYxSwOmh4X2H+oBHH/stcO+3fnL0cB+gmhsBZ63zNKBPh8C6e7///Xs+BsSRyCnsaZ33aw/V8n6aBX6zdS9w4ZXvNzqlf5SiEADdczqAHRu/2iLnX6o9AxMCGc//2J3/DHz2s/cDH/zo3wIfv+Wv2lqbxj4STQCnUMCmbXuBy6/7xMFjAwD5YeCVr13zlU/dBJy/amkphhIqS2M+1j949Q2fBp743+cA1dwYKUylAM/yqx/9A/CSVYship0rryoSrIr3nXMSjRCUDz70M8AMDnmdrYCoe1/pMDTAsoXdQEtzYyEwgI7cFxs9LUfosWV3L/Cd9Z8G3nzFq4DQmtAaYg2eiDAXpVKseDjnrVwM3L/u/Zdf/UHgAx/6G+CLH71eBkt2Fq0AHccaxAxwChHBxgLMntX6wBffD5x/5QeAoeFR0fGepwHTNyjLPH/V9UCoHOBjraRqirGZCrtc5/3aQ1W8n8TxJJN8zqW3Aju277cNWfkZyPp+4fgAcMO7Xg98/Y5bhejKKkEERgvh/kNHgOWL5wMy0lO6egM7PzTy8Mangbdc+SqEBk0xc1eNuR6EVuyBa9+3Dvjev/xHpn0mEDoLMFzo6VkIbH7kK8TZbIuLLeaJMFfL+4Ll15t3AC/s7AVoyMZ5EgUYjMYC5/UsmRRhLuuftXge8SEpHGCVFZ4dE22roHmbGnN//fpXE0fpnXVi+xvJm3pi+ZdHcdJ7rbRTSgMXntsDfG/9xjhf62R1skxZ8svOXyH7oKuIBNZ5v/YwNfv0f57ZAqihAqAbc0aOXdxNq1wuC6zqWSCD1YRCRVg1E9XbxOpI/kxyAePbhkopqR8QovNiCeMqEYlKPTX21+6OdgCt0nUunqdNfhR44pktpOi0moDa1Pb0mU07iRWxdnGZjeQdjcm2NAE9S+aNXYDFpXQlFiWCb/P2/cD6h38JnDw19Ja/eCXw6otWxwuYiI2Sagzgx7945tHHfkMsN95+9euAC85eLMKpshfkSgyscuQOozXw9KadyZcTU0nx2apG1WEqMAmdlp3h9j2HATIeSew5pkcXBAsWdAPz5nSkvxfQODm/2M5V//n474DLr1sLhAODANY9sP4RYPPP7gYWz589ga9tnBWS/MBnvgF86YvrMxkPCIZHgB/88L+Bzf91b0tDBTMgWpQzsvy+gUFAOWyKr0PnyOhkyek0e3pzKgqTqnhfay2BjIPHTwBEQrB0TwuhGEbZjEeFNGSUvE8KaW67/RtAmB8GGrs6BV2+bxDYsm0PsGT+bFuJK6NIo9bizt113wbA62h1vg9oOwM40DcAHDp4tOes+YBztjjR5O/YCew9dJwx1KOdM75KljwyGgCNDdkSy2Eci6rO+7WHanVUPj8MnM6PAqJIk3ONjiu0q1fMT8bbMRrYOkusQx5/Zuuzz+0EVGszMFwoAJ5SOucDM2Y0R8/oCoRgbQhonf3xxicBN2QAmnQYBhA5no1NTUBzS0OMxzEmBeYpLZbvtl29AL5Ok6oBlJ8sOZ/PA43i40wGk8lThcxmeLQAjBZCmU56TGSCaHXuikUToRI329fAz598zg0XAK+5ARDWNM55uQzQ1dEqj6hK+UDlyZztU7/dAeBHIUQx4AkCYE5nG9DV3hbj0Wk8iakmAYrtuw8DKpMpNwA0xFcMhkclFoxRULJrFay9Ou/XHqYYky7P3GmSBFFDZvXyhckvWpcnndMS/XfP74n40aYSdoWga95sYMGczrGPkETvlWSTzAt7DwFKClpjFSw1vWIj+75f2XKIsR48Mgj0HjkBuIyHncT+dM6NkUb1uNTvBSajUzk6rRpzWSCXyQIj+QKAF4vSMARaZ7UuXTgneU6PqUJJlyBs3XWQTIbEERR6HA2WL5oDNDc1IFqujE5FuFsHHDo6sP9gH0TGsnNoIaHAAGcvj/xjMYdVqX0qaR7P83bs3Y9kukA1N7gyOjUOyGV8oCmbjTBEwniiPZtkT5N5SNB+RksDMNA/CHh48msYGGDR/M7uzrbUoxEHyPMJbx7vGwD2HTgqexqZe8KboV29fEHypXVWl90Yc1GwGti1/8jQwGlANWUBrJMItPE0cE6ptiyXIfG/xDNmNAT8Fi+UEkQ5OYXDJQtvammceKPSUOf92kNVdGqtbWjIAnM72oDe3YcAm8tGx18oACuXzp3gVofFyQWRnfuPACf7B8kVM9gmdsnOXrEoeSljFIZL8d3WHfvUaAD4zQ1AgAmsAWjIAKt6FjK+75jAFqHTBH3JWEVogHmd7SSWqXWqkslcBtXGpOWD6NOnn3gO8JWKHGRrgXN7lsTZ5grK2jgnRu1WiWePFDKNOSAwpog/55/TU7QcxgSBStyIzdv3xnExGR1tQVt7G7B0UTeglNKVNHliBmzZsQ9QmWJMIAEvlmnLF3cnXxpnvVS5wnhnVuf92kN1dBofxppzlwEPPvgoYFWsYbwMcO6KJTKm7OjKGPm5bb0ANi5GF9IOLdDYNmPZoqLlMCbxq3VkHStg6/ZePJ9YbiilXBACi+Z2ALMTDyo1GVcaSMsPjezYdxRwWcFTumStxOa58LzlyZdGlRTVjCdY/GrSYcmvr1lzNkBTjnQNV3MO6FkyJx5cYuwLfj/+56YX9oA41zLYAgQhsGDp3HlzO4ovdSUmi3FWqvHFP961/whZv4hf6aAQAquXLUzebYzxIjc6Sk9FLoBSwP6DfUeP9QNkfEDZEkvKGEtzBnjlmtXJl151Kcg679ce/GrSv0kI5yXnLANWLl8IbNuxD+UBXV0zgYXzZqcfKQ9Hai1XJnbEDqXENJXyARuMAMsXz5HEdXxRVbuolrXk4A8cOg4cONovt1SFuqyOKixXp7ScdUpHai2q24hyoloDL+w+YE4PA7qtGbAmcrsjzT5c6OlZBFxwzvLk+TG3KG1FoqzW3y9YSeJ7wFUXXwh8YfMulwNYNr+LuP0DY5zryJOBg0dOAAfEufY9Ip4FsGEAnLdyiTySEkcp8WVCuXG3c99hoHAqL7VjYnU45/AzwNkri9a+VlFKTkV74TkVJr9u3r6fMGCM3IuqU4cLUoQumf2yWoV4hpW5vM77tYdq6dRPkf1b33gxcNc3f1wYOA2sWBY516fzw4y19q0Fmpsatuw4CIwMniblXEfWvtbAiqVRwlUSFb6JpIcQRWBNa1MG+O3zewBGAr+lCQjEKzFWyHbJ/KI5OTIa4BmKly4xxgHNvg9sen4P2k9+Nc4JW0j5l9fa9PY3XCwrIOb6RGtOotKnVdOrgWtuvuMHG34OtC/ukl2LLo4XOaIYGNRay473n8zLnNKegcQ4ZnfOzGYygJUo+5hEgbz35EAeOHV6WKR5En4UOTi3sy2ZobXJ4pPIsU125HjfqdFC6lacc9r3Ads/APzlG/9kwz0fKVvypI4Z8WvqUGOY4t3IlDH75LPbpPZcjHMXGsbx0+VJyZpMcOdAWSv147iYAMs7TDiIs7ZlYebkXnQQRq+T4ZF7ropfxnhUxncqjkGRuhlT+P3VngvXaCAMC8DLX7ryxmv/DPjaN34EZLpmBlL/FVeplHTeSByVikcot4A9T2RxfPM8VrLRX1HVX2U88QGoXBbQVRCKSZBI+sD3TN9J4Mbrr5LVmTjMOimqMqjzfu2hBnfO1lz5PuDAoROqIQu4anpIkOK41Of4zlmFfKlyk11MFyRiulcRkStiFv0zUpA7Z7/64ZeAObNbJcA2jTtn1d/fh3jtAuX3Ta/7lD8jC4Q22axKqeTim8tkXDVzBcbvPzEW4cTNJ+LFyK2UID/8rw+sBa665CLA1u+b/kHBGfVDSV/Lvv3L3/3k2q8B2a5ZQCGwUj06SVuTMuKaOvGOi6oKhFnfB4Ij/cDate/65Huv5f/3rjlJMo4o7R71mbj7+0C2u7MgxTbxi6b9luncOC8+Mq78yfp+4WgfcNPN0mfiPSZVfXQmUOf92kP1dDpuGXjSY0YM43d95KvA/fdtyMxuB6IEr/FK1YUu6X42HoeWWgVn3hxB/FdfJT1mrgG+fsd7AGutWN9n3oGhlv2lEiEAfHjdN9d9fj3p/lJi5dio8KEmr4QJ9yBhfQAyvjKjhqS/1IfffueH3kZUGlHvL/WHDdOn07FBmigo5wCyWn374V8CN//dV4DTx/vlRlfcWy5O6Ud8XerX164Niq+LF6WCk/0t7e2M01sueeOZv7X2fSUl6GBxFftKaimVaGpg/L6S8dTUNIyqaH/G6St599qbgLMWzwFCazxVcjWtVlDn/dpD1b6pjJ4K6iRVByilSnvKbouKVkp6ykY0booNpSaeuyLOWI3TU9atXLMS+PR7/4B7yk5jZxO/QEdxwgr9pINjg4DKetIGxGWjCGl0R0/JnVQvksVJ6YOkTCVWPxqIGMlIP+mXnQ3c8rbLrvrTi4CMVFc4W3aLvVZrLIM679ceXvS+5wlYW4zCxYG5sl7yv3t2y27g8NGTwEh+WKqFhR4TM0PqXGwm09BcoZf8pa+9AFi+sDv96mlHl6cHL+KepjGXXfse9/+RKITA3gNHgN29Rw8c6ydO6gVhmPF9oL21GZjbNWvpgi5gcaX/RyKxPeRub4qRp9wwZhpQ5/3aw/8BEIjodALq754AAAAASUVORK5CYII=\" id=\"p1img1\"></img></caption>\r\n"
			+ "	  <thead>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\">TELECONSULTA ATENDIMENTO</th>\r\n"
			+ "		  <th scope=\"col\" colspan=\"3\">De $dateTool.format('dd/MM/yyyy',${model.dominio.dataDe}) até $dateTool.format('dd/MM/yyyy',${model.dominio.dataAte})</th>\r\n"
			+ "		  <th scope=\"col\">PAGINA ${pagina.numeroPagina}</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\">ATENDIMENTO</th>\r\n"
			+ "		  <th scope=\"col\" colspan=\"3\">MEDICO</th>\r\n"
			+ "		  <th scope=\"col\">STATUS</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  </thead>\r\n"
			+ "	  <tbody>\r\n"
			+ "		#foreach($atendimento in ${pagina.dominios})"
			+ "		<tr>\r\n"
			+ "		#if($foreach.count % 2 >= 1) "
			+ "		  <td data-label=\"Account\"  style=\"background-color: #E9F4FB;\">$dateTool.format('dd/MM/yyyy',${atendimento.dataCadastro})</td>\r\n"
			+ "		  <td data-label=\"Due Date\" style=\"background-color: #E9F4FB;\" colspan=\"3\">${atendimento.registroPreAtendimento.medico.nomeExibicao.toUpperCase()}</td>\r\n"
			+ "		  <td data-label=\"Period\"   style=\"background-color: #E9F4FB;\">${atendimento.statusAtendimento.descricao}</td>\r\n"
			+ "		#else "
			+ "		  <td data-label=\"Account\">$dateTool.format('dd/MM/yyyy',${atendimento.dataCadastro})</td>\r\n"
			+ "		  <td data-label=\"Due Date\" colspan=\"3\">${atendimento.registroPreAtendimento.medico.nomeExibicao.toUpperCase()}</td>\r\n"
			+ "		  <td data-label=\"Amount\">${atendimento.statusAtendimento.descricao}</td>\r\n"
			+ "		#end "
			+ "		</tr>\r\n"
			+ "		#end "
			+ "	  </tbody>\r\n"
			+ "   <tfoot>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"3\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\" colspan=\"5\" style=\"text-align: left;\" >TOTAL:</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\">&#160;</th>\r\n"
			+ "		 <th scope=\"col\">AGENDADOS: ${model.dominio.totalAgendados}</th>\r\n"
			+ "		  <th scope=\"col\">ATENDIDOS: ${model.dominio.totalAtendidos}</th>\r\n"			
			+ "		  <th scope=\"col\" colspan=\"2\" >CANCELADOS: ${model.dominio.totalCancelados}</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  </tfoot>"
			+ "	</table>\r\n"
			+ "		#end "
			+ "	\r\n"
			+"#end"
			+ "	</div>";
	
	
	private final AtendimentoClient atendimentoClient;
//
	@Inject
	private UnidadeClient unidadeClient;
	
	@Inject
	private RegistroPreAtendimentoClient registroPreAtendimentoClient;
	
	@Inject
	private PerfilClient perfilClient;
	
	@Inject
	private AgendamentoClient agendamentoClient;
	
	@Inject
	private PacienteClient pacienteClient;
	
	@Inject
	private TipoConsultaClient tipoConsultaClient;
	
	@Inject
	private ModeloPosConsultaClient modeloPosConsultaClient;
	
	private final RegistroPreAtendimentoService registroPreAtendimentoService;
	
	private final AgendamentoService agendamentoService;
	
	private static final Long TIMEOUT = 10L;
	
    public AtendimentoService(AtendimentoClient atendimentoClient, RegistroPreAtendimentoService registroPreAtendimentoService, AgendamentoService agendamentoService) {
        this.atendimentoClient = atendimentoClient;
        this.registroPreAtendimentoService = registroPreAtendimentoService; 
        this.agendamentoService = agendamentoService;
    }
	
	public Optional<Atendimento> show(Long id) {
        
		Optional<Atendimento> atendimentoShow = atendimentoClient.show(id);
    	
    	if (atendimentoShow.isPresent()) {
    		Atendimento atendimento = atendimentoShow.get();
    		atendimento.setRegistroPreAtendimento(registroPreAtendimentoService.show(atendimento.getRegistroPreAtendimento().getId()).orElse(null));
//    		Agendamento agendamento = atendimento.getRegistroPreAtendimento().getAgendamento();
//    		Optional<Agendamento> showAgendamentoFull = agendamentoService.show(agendamento.getId());
//    		if (showAgendamentoFull.isPresent())
//    			atendimento.getRegistroPreAtendimento().setAgendamento(showAgendamentoFull.get());
    		
    		return Optional.of(atendimento);
    	}
    	
    	return atendimentoShow;
    }
	
	private Pageable createPageable(Integer offset, Integer max, String sort, String order) {
		return Pageable.from(Optional.ofNullable(offset).orElse(0),max == null ? 10 : max, Sort.of(new Sort.Order(sort != null ? sort : "id", order != null ? Sort.Order.Direction.valueOf(order.toUpperCase()) : Sort.Order.Direction.ASC, true)) );
	}
	
	public Page<Atendimento> list(
			@Nullable Long grupoEmpresa,
    		@Nullable Long empresa,
    		@Nullable List<Long> unidade,
    		@Nullable List<Long> setor,
    		@Nullable List<String> medico,
    		@Nullable List<Long> tipoConsulta,
    		@Nullable String dataInicial,
    		@Nullable String dataFinal,
    		@Nullable List<Long> status,
    		@Nullable List<Long> tipoOrientacaoPosConsulta,
    		@Nullable Integer max,
    		@Nullable Integer offset, 
    		@Nullable String sort, 
    		@Nullable String order) {
    		
		List<Unidade> unidades_ = new ArrayList<>();
		
		 return
			
				Flowable.zip(
					this.findByGrupoEmpresaRxH(grupoEmpresa).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()),
					this.findByEmpresaRxH(empresa).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()),
					(uGrupoEmpresa1, uEmpresa) -> {
						
						unidades_.addAll(uGrupoEmpresa1.getBody().orElse(Collections.emptyList()));
						unidades_.addAll(uEmpresa.getBody().orElse(Collections.emptyList()));
						
						List<Long> unidadesCalculada = unidades_.parallelStream().map(p -> p.getId()).collect(Collectors.toList());
				    	
						unidadesCalculada.addAll(Optional.ofNullable(unidade).orElse(Collections.emptyList()));
				    	
				    	if (unidadesCalculada.isEmpty()) {
							unidadesCalculada = null;
						}
				    	
				    	/*
				    	if (StringUtils.isNoneBlank(periodo)) {
				    		ajustarParaPeriodo(dataInicial, dataInicial, periodo);
				    	}
				    	*/
				    	return 
					    	atendimentoClient.findRxH(null, null, unidadesCalculada, null, medico, tipoConsulta, dataInicial, dataFinal, status, 99999, null, null, null)
					    		.subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
					    		.map(a -> a.getBody().get())
					    		.flatMap(atendimentoPageResponse -> {
					    			
					    			if (atendimentoPageResponse.getTotalSize() <=0) {
							    		return Flowable.just( 
							    				Page.<Atendimento>of(Collections.emptyList(), 
							    						createPageable(offset, max, sort, order),
							    						0
							    				));
							    	}
									
									List<Atendimento> it = atendimentoPageResponse.getContent();
					    			
//									List<Long> idsModelosPosConsulta	= Collections.emptyList();
									
									List<String> idsPerfis 				= it.stream().map(p -> p.getRegistroPreAtendimento().getMedico().getId()).collect(Collectors.toList());
							    	List<Long> idsAgendamento 			= it.stream().map(p -> p.getRegistroPreAtendimento().getAgendamento().getId()).collect(Collectors.toList());
							    	//List<Long> idsModelosPosConsulta	= it.stream().map(p -> p.getRegistroPosConsulta() != null && p.getRegistroPosConsulta().getModeloPosConsulta() != null && p.getRegistroPosConsulta().getModeloPosConsulta().getId() != null ?  p.getRegistroPosConsulta().getModeloPosConsulta().getId() : null).filter(Objects::nonNull).collect(Collectors.toList());
							    	
							    	
							    	return 
								    	Flowable.zip(
								    			findPerfilByIdsRxH(idsPerfis), 
								    			findAgendamentoByIdRxH(idsAgendamento),
								    			//findModeloPosConsultaByIdRxH(idsModelosPosConsulta),
								    			(listaPerfil, listaAgendamento/*, listaModeloPosConsulta*/) -> {
								    				
								    				Map<String, Perfil> mapPerfil = listaPerfil.getBody().orElse(Collections.emptyList()).stream().collect( Collectors.toMap(Perfil::getId,p->p) );
								    				Map<Long, Agendamento> mapAgendamento = listaAgendamento.getBody().orElse(Collections.emptyList()).stream().collect( Collectors.toMap(Agendamento::getId,p->p) );
								    				//Map<Long, ModeloPosConsulta> mapModeloPosConsulta = listaModeloPosConsulta.getBody().orElse(Collections.emptyList()).stream().collect( Collectors.toMap(ModeloPosConsulta::getId,p->p) );
								    				
								    				it.forEach(r -> {
								    					r.getRegistroPreAtendimento().setMedico(mapPerfil.get(r.getRegistroPreAtendimento().getMedico().getId()));
								    					r.getRegistroPreAtendimento().setAgendamento(mapAgendamento.get(r.getRegistroPreAtendimento().getAgendamento().getId()));
								    				//try { r.getRegistroPosConsulta().setModeloPosConsulta(mapModeloPosConsulta.get(r.getRegistroPosConsulta().getModeloPosConsulta().getId()));}catch (Exception e) {/* abafa */}
								    				});
								    				
								    				List<Atendimento> retorno = new ArrayList<>();
						    						retorno.addAll(it);
//						    						System.out.println("*********************************************************");
//						    						System.out.println("*********************************************************");
//						    						System.out.println("*********************************************************");
//						    						System.out.println("*********************************************************");
//						    						System.out.println(tipoOrientacaoPosConsulta);
//						    						System.out.println("*********************************************************");
//						    						System.out.println("*********************************************************");
//						    						System.out.println("*********************************************************");
//						    						System.out.println("*********************************************************");
//						    						System.out.println(new Gson().toJson(retorno));
						    						
//						    						if (tipoOrientacaoPosConsulta != null && !tipoOrientacaoPosConsulta.isEmpty()) {
//						    							retorno = 
//							    							retorno.stream().filter(_atendimento -> {
//							    								return tipoOrientacaoPosConsulta.contains(_atendimento.getRegistroPosConsulta().getModeloPosConsulta().getId());
//								    						}).collect(Collectors.toList());
//						    						}
						    						
						    						
						    						Page<Atendimento> retornoPageAtendimento = null;
						    						
						    						final int direction = "ASC".equalsIgnoreCase(Optional.ofNullable(order).orElse("ASC")) ? 1 : -1;
						    						
						    						final int offsetLocal = offset == null || offset <= 0 ? 0 : offset ;
								    				
						    						switch (sort == null ? "" : sort) {
						    							case "registroPreAtendimento.agendamento.paciente.nome":
						    								
						    								retornoPageAtendimento = Page.of(
								    								retorno
									    								.stream()
									    							    .sorted((atend1, atend2) -> {
									    							    	return direction * atend1.getRegistroPreAtendimento().getAgendamento().getPaciente().getNome().compareTo(atend2.getRegistroPreAtendimento().getAgendamento().getPaciente().getNome());
									    							    })
									    							    .skip(offsetLocal * max)
									    						        .limit(max == null ? 10 : max)
									    							    .collect(Collectors.toList())
									    							    ,
									    							    createPageable(offset, max, sort, order), 
									    							    atendimentoPageResponse.getTotalSize()
							    							    );
						    								break;
						    							case "registroPreAtendimento.agendamento.tipoConsulta.nome":
						    								
						    								retornoPageAtendimento = Page.of(
								    								retorno
									    								.stream()
									    							    .sorted((atend1, atend2) -> {
									    							    	return direction * atend1.getRegistroPreAtendimento().getAgendamento().getTipoConsulta().getNome().compareTo(atend2.getRegistroPreAtendimento().getAgendamento().getTipoConsulta().getNome());
									    							    })
									    							    .skip(offsetLocal * max)
									    						        .limit(max == null ? 10 : max)
									    							    .collect(Collectors.toList())
									    							    ,
									    							    createPageable(offset, max, sort, order), 
									    							    atendimentoPageResponse.getTotalSize()
							    							    );
						    								break;
						    							case "registroPreAtendimento.agendamento.unidade.nome":
						    								
						    								retornoPageAtendimento = Page.of(
								    								retorno
									    								.stream()
									    							    .sorted((atend1, atend2) -> {
									    							    	return direction * atend1.getRegistroPreAtendimento().getAgendamento().getUnidade().getNome().compareTo(atend2.getRegistroPreAtendimento().getAgendamento().getUnidade().getNome());
									    							    })
									    							    .skip(offsetLocal * max)
									    						        .limit(max == null ? 10 : max)
									    							    .collect(Collectors.toList())
									    							    ,
									    							    createPageable(offset, max, sort, order), 
									    							    atendimentoPageResponse.getTotalSize()
							    							    );
						    								break;
						    							case "registroPreAtendimento.agendamento.dataConsulta":
						    								
						    								retornoPageAtendimento = Page.of(
								    								retorno
									    								.stream()
									    							    .sorted((atend1, atend2) -> {
									    							    	return direction * atend1.getRegistroPreAtendimento().getAgendamento().getDataConsulta().compareTo(atend2.getRegistroPreAtendimento().getAgendamento().getDataConsulta());
									    							    })
									    							    .skip(offsetLocal * max)
									    						        .limit(max == null ? 10 : max)
									    							    .collect(Collectors.toList())
									    							    ,
									    							    createPageable(offset, max, sort, order), 
									    							    atendimentoPageResponse.getTotalSize()
							    							    );
						    								break;
						    							case "statusAtendimento":
						    								
						    								retornoPageAtendimento = Page.of(
								    								retorno
									    								.stream()
									    							    .sorted((atend1, atend2) -> {
									    							    	return direction * atend1.getStatusAtendimento().getDescricao().compareToIgnoreCase(atend2.getStatusAtendimento().getDescricao());
									    							    })
									    							    .skip(offsetLocal * max)
									    						        .limit(max == null ? 10 : max)
									    							    .collect(Collectors.toList())
									    							    ,
									    							    createPageable(offset, max, sort, order), 
									    							    atendimentoPageResponse.getTotalSize()
							    							    );
						    								break;
						    							default:
						    								
						    								retornoPageAtendimento = Page.of(
								    								retorno
									    								.stream()
									    							    .sorted((atend1, atend2) -> {
									    							    	return -1 /*DESC*/ * atend1.getRegistroPreAtendimento().getAgendamento().getDataConsulta().compareTo(atend2.getRegistroPreAtendimento().getAgendamento().getDataConsulta());
									    							    })
									    							    .skip(offsetLocal * max)
									    						        .limit(max == null ? 10 : max)
									    							    .collect(Collectors.toList())
									    							    ,
									    							    createPageable(offset, max, "registroPreAtendimento.agendamento.dataConsulta", order), 
									    							    atendimentoPageResponse.getTotalSize()
							    							    );
						    								break;
						    								
						    						}
						    						
								    				return  
								    						retornoPageAtendimento;
								    			});
					    		});
					})
					.flatMap(a -> a)
					.blockingLast();
				
    }

	private void ajustarParaPeriodo(String dataInicial, String dataFinal, String periodo) {
		
		LocalDate dataI =  null;
		LocalDate dataF = null;
		
		switch (periodo) {
			case "PROXIMOS_30_DIAS":
				
				dataI = LocalDate.now();
				dataF = LocalDate.now().plusDays(30);
				break;
			case "PROXIMOS_07_DIAS":
				dataI = LocalDate.now();
				dataF = LocalDate.now().plusDays(7);
				break;
			case "HOJE":
				dataI = LocalDate.now();
				dataF = LocalDate.now();
				break;
			case "AMANHA":
				dataI = LocalDate.now().plusDays(1);
				dataF = LocalDate.now().plusDays(1);
				break;
			case "ONTEM":
				dataI = LocalDate.now().minusDays(1);
				dataF = LocalDate.now().minusDays(1);
				break;
			case "ULTIMOS_03_DIAS":
				dataI = LocalDate.now().minusDays(3);
				dataF = LocalDate.now();
				break;
			case "ULTIMOS_07_DIAS":
				dataI = LocalDate.now().minusDays(7);
				dataF = LocalDate.now();
				break;
			case "ULTIMOS_30_DIAS":
				dataI = LocalDate.now().minusDays(30);
				dataF = LocalDate.now();
				break;
			default:
				break;
		}
		
		if (dataI != null && dataF != null) {
			dataInicial = dataI.getYear() + "-" + String.format("%02d", dataI.getMonthValue()) + "-" +  String.format("%02d", dataI.getDayOfMonth());
			dataFinal 	= dataF.getYear() + "-" + String.format("%02d", dataF.getMonthValue()) + "-" +  String.format("%02d", dataF.getDayOfMonth());
		}
		
	}
	
	
	
	
	
	
	private Flowable<HttpResponse<List<ModeloPosConsulta>>> findModeloPosConsultaByIdRxH(List<Long> idsModeloPosConsulta) {
		
		if (idsModeloPosConsulta == null || idsModeloPosConsulta.isEmpty()){
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		return modeloPosConsultaClient.findByIdInListRxH(idsModeloPosConsulta);
	}
	
	private Flowable<HttpResponse<List<Agendamento>>> findAgendamentoByIdRxH(List<Long> idsAgendamento) {
		
		return Flowable.just(HttpResponse.ok(agendamentoService.list(idsAgendamento)));
	}

	private Flowable<HttpResponse<List<Perfil>>> findPerfilByIdsRxH(List<String> idsPerfis) {
		
		if (idsPerfis == null || idsPerfis.isEmpty()){
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		
		return perfilClient.findByIdsRxH(idsPerfis);
	}
	
	
	private List<Unidade> findByGrupoEmpresa(Long grupoEmpresa) {
		
		if (null != grupoEmpresa) {
		
			List<String> parametroGrupoEmpresa = new ArrayList();
			parametroGrupoEmpresa.add( String.valueOf(grupoEmpresa));
			
			List<Unidade> findByGrupoEmpresaIdInList = unidadeClient.findByGrupoEmpresaIdInList(parametroGrupoEmpresa);
			
			return findByGrupoEmpresaIdInList;
			
		}
		
		
		
		return Collections.emptyList();
	}
	
	private Flowable<HttpResponse<List<Unidade>>> findByGrupoEmpresaRxH(Long grupoEmpresa) {
		
		if (grupoEmpresa == null) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		return unidadeClient.findByGrupoEmpresaIdInListRxH(  Collections.singletonList(String.valueOf(grupoEmpresa)));
		
	}
	
	private Flowable<HttpResponse<List<Unidade>>> findByEmpresaRxH(Long empresa) {
		 
		if (empresa == null) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		return unidadeClient.findByEmpresaIdInListRxH(  Collections.singletonList(String.valueOf(empresa)));
	}
	
	private List<Unidade> findByEmpresa(Long empresa) {
		
		if (null != empresa) {
		
			List<String> parametroGrupoEmpresa = new ArrayList();
			parametroGrupoEmpresa.add( String.valueOf(empresa));
			
			List<Unidade> findByGrupoEmpresaIdInList = unidadeClient.findByEmpresaIdInList(parametroGrupoEmpresa);
			
			return findByGrupoEmpresaIdInList;
			
		}
		
		
		
		return Collections.emptyList();
	}

//	private List<Agendamento> findAgendamentos(List<Long> id) {
//		
//        System.out.println("Optional<Agendamento> findAgendamentos(List<Long> id) ->>> " + id + "**********************************************************************************");
//    	Flowable<List<Agendamento>> flatMap = Flowable.just( agendamentoClient.byIds(id) )
//    		.subscribeOn(Schedulers.io())
//			.observeOn(Schedulers.io())
//			//.timeout(timeout, TimeUnit.SECONDS, p -> p.onComplete()).retry(2l)
//			.onErrorResumeNext(throwable -> {
//                if (throwable instanceof java.util.concurrent.TimeoutException) {
//                    return Flowable.just(null);
//                }
//
//                return Flowable.error(throwable);
//            })
//			.flatMap(listaAgendamentos -> {
//				
//				Agendamento anull = null;
//				if (listaAgendamentos.isEmpty()) return Flowable.just( Collections.emptyList() );
//				
//				List<Long> idsPacientes = listaAgendamentos.stream().map(p -> p.getPaciente().getId()).collect(Collectors.toList());
//				List<String> idsUnidadess = listaAgendamentos.stream().map(p -> String.valueOf( p.getUnidade().getId() )).collect(Collectors.toList());
//				List<Long> idsTiposConsultas = listaAgendamentos.stream().map(p -> p.getTipoConsulta().getId()).collect(Collectors.toList());
//				
//				return 
//				Flowable.zip(
//						Flowable.just(pacienteClient.byIds(idsPacientes))//.subscribeOn(Schedulers.io())//.timeout(timeout, TimeUnit.SECONDS).retry(2l)
//						,
//						Flowable.just(unidadeClient.findByIdInList(idsUnidadess))//.subscribeOn(Schedulers.io())//.timeout(timeout, TimeUnit.SECONDS).retry(2l)
//						,
//						Flowable.just(tipoConsultaClient.findByIdInList(idsTiposConsultas ))//.subscribeOn(Schedulers.io())// .timeout(timeout, TimeUnit.SECONDS).retry(2l)
//						,
//						(paci, uni, tcon ) -> {
//							
//							
//							Map<Long, Unidade> mapUnidade = uni.stream().collect( Collectors.toMap(Unidade::getId,Function.identity()) );
//							Map<Long, TipoConsulta> mapTipoConsulta = tcon.stream().collect( Collectors.toMap(TipoConsulta::getId,Function.identity()) );
//							Map<Long, Paciente> mapPaciente = paci.stream().collect( Collectors.toMap(Paciente::getId,Function.identity()) );
//							
//							listaAgendamentos.forEach(r -> {
//		    					if (r.getPaciente() != null && r.getPaciente().getId() != null)
//		    						r.setPaciente(mapPaciente.get(r.getPaciente().getId()));
//		    					if (r.getUnidade() != null && r.getUnidade().getId() != null)
//		    						r.setUnidade(mapUnidade.get(r.getUnidade().getId()));
//		    					if (r.getTipoConsulta() != null && r.getTipoConsulta().getId() != null)
//		    						r.setTipoConsulta(mapTipoConsulta.get(r.getTipoConsulta().getId()));
//		    				});
//							
//							return listaAgendamentos;
//						}
//				);
//			});
//	    	
//    	return flatMap.blockingLast();
//	}
	
	public Atendimento update(Atendimento atendimento) {
        return atendimentoClient.update(atendimento);
    }
	
	public byte[] generateStreamPDFRelatorioMedico(Long idAtendimento) throws IOException {
		
		Atendimento atendimento = this.show(idAtendimento).get();
    	
    	atendimento.setConteudo(atendimento.getConteudo().replace("<br>", "<br></br>").replace("&nbsp;", "&#160;"));
    	
    	String conteudo = atendimento.getConteudo();
    	Map<String, Object> buildContext = VelocityUtil.buildContext(TemplateModel.<Atendimento>of(atendimento));		
    	String conteudoTemplate = VelocityUtil.mountTemplate(buildContext, new ByteArrayInputStream(conteudo.getBytes())  );
    	atendimento.setConteudo(conteudoTemplate);

    	return TemplateToPdfConverterUtil.generatePdf(
    			TemplateModel.<Atendimento>of(atendimento), 
    			TEMPLATE_BODY_DOWNLOAD_RELATORIO_MEDICO, 
    			TEMPLATE_CSS_DOWNLOAD_RELATORIO_MEDICO);
	}
	
	
	public byte[] combinarFiltrosAtendimentoGerarStreamPDF(
			List<String> grupoEmpresaId, 
			List<String> empresaId, 
			List<Long> unidadeId,
			List<Long> tipoConsultaId,
			String dataDe, 
			String dataAte) {
		
			
			return 		 
					Flowable.zip(
						this.findByGrupoEmpresaRx(grupoEmpresaId).subscribeOn(Schedulers.io()),
						this.findByEmpresaRx(empresaId).subscribeOn(Schedulers.io()),
						(uGrupoEmpresa1, uEmpresa) -> {
							
							List<Unidade> unidadesSomadas = new ArrayList<>();
							
							List<Unidade> listauGrupoEmpresa1 	= uGrupoEmpresa1.getBody().orElse(Collections.emptyList());
							List<Unidade> listauEmpresa 		= uEmpresa.getBody().orElse(Collections.emptyList());
							
							if (listauGrupoEmpresa1 != null && !listauGrupoEmpresa1.isEmpty()) {
								unidadesSomadas.addAll(listauGrupoEmpresa1);
							}
							
							if (listauEmpresa != null && !listauEmpresa.isEmpty()) {
								unidadesSomadas.addAll(listauGrupoEmpresa1);
							}
							
							List<Long> idsUnidade = unidadesSomadas.stream().map(p -> p.getId()).collect(Collectors.toList());
							
							return 
									atendimentoClient.findRxH(null, null, idsUnidade, null, null, tipoConsultaId, getDateDe(dataDe), getDateAte(dataAte), null, -1, null, null, null);
							
						})
				.flatMap(a -> a)
				.flatMap(a -> Flowable.just( a.body() ))
				.flatMap(fullAtendimentos -> {
//					
					List<Atendimento> listaAtendimentos = fullAtendimentos.getContent() == null? Collections.emptyList() : fullAtendimentos.getContent();
					
					if (!listaAtendimentos.isEmpty()) {
					
						List<String> perfilIds = listaAtendimentos.stream().map(a -> a.getRegistroPreAtendimento().getMedico().getId()).collect(Collectors.toList());
						
						List<Perfil> findByIds = perfilClient.findByIds(perfilIds);
						
						final Map<String, Perfil> listaPacientesIdMapRetornados = findByIds.stream().collect( Collectors.toMap(Perfil::getId, p -> p)  );
						
						
						listaAtendimentos.forEach(_atendimento -> {
							_atendimento.getRegistroPreAtendimento().setMedico(listaPacientesIdMapRetornados.get(_atendimento.getRegistroPreAtendimento().getMedico().getId()));
						});
					}
					return Flowable.just( listaAtendimentos );
				}).flatMap(listaAtendimentoFull -> Flowable.just( generatePdfFromAtendimento(listaAtendimentoFull, dataDe, dataAte))).blockingLast();
				 
				 
//						.flatMap(a -> a)
//						.flatMap(a -> Flowable.just( a.getBody().orElse(Collections.emptyList())) )
//						.map(fullAtendimentos -> {
//							
//							List<Long> idsPacientes = fullAtendimentos.stream().map(p -> p.getRegistroPreAtendimento().getMedico().getId().getId()).collect(Collectors.toList());
//							
//							return 
//								callPacienteByIdsRxH(idsPacientes)
//									.subscribeOn(Schedulers.io())
//									.flatMap(a -> Flowable.just( a.getBody().orElse(Collections.emptyList())) )
//									.flatMap(listaPacientes -> {
//										
//										final Map<Long, Paciente> listaPacientesIdMapRetornados = listaPacientes.stream().collect( Collectors.toMap(Paciente::getId, p -> p)  );
//										
//										fullRegistroPreAtendimentoCancelados.forEach(rpa -> {
//											
//											rpa.getAgendamento().setPaciente( listaPacientesIdMapRetornados.get(rpa.getAgendamento().getPaciente().getId()));
//										});
//										
//										return Flowable.just( generatePdfFromRegistroPreAtendimentoCancelamento(fullRegistroPreAtendimentoCancelados, dataDe, dataAte) );
//										
//										
//									}).blockingLast();
//							
//							
//						}).blockingFirst();
						
				
//			return null;
	}
	
	
	private byte[] generatePdfFromAtendimento(Iterable<Atendimento> fullAtendimentos, String dataDe, String dataAte) throws IOException {
		
		return TemplateToPdfConverterUtil.generatePdf(
    			TemplateModel.<AtendimentoRel>of(geraRelatorioDomain(fullAtendimentos, dataDe, dataAte)), 
    			TEMPLATE_BODY_RELATORIO_ATENDIMENTO, 
    			TEMPLATE_CSS_RELATORIO_ATENDIMENTO);
		
	}
	
	
	private Flowable<HttpResponse<List<Unidade>>> findByGrupoEmpresaRx(List<String> grupoEmpresa) {
		
		if (null != grupoEmpresa  && !grupoEmpresa.isEmpty()) {
			return unidadeClient.findByGrupoEmpresaIdInListRxH(grupoEmpresa);
		}
		
		return Flowable.just(HttpResponse.ok(Collections.emptyList()));
	}
	
	private Flowable<HttpResponse<List<Unidade>>> findByEmpresaRx(List<String> empresa) {

		if (null != empresa  && !empresa.isEmpty()) {
		
			return unidadeClient.findByEmpresaIdInListRxH(empresa);
			
		}

		return Flowable.just(HttpResponse.ok(Collections.emptyList()));
	}
	
	private AtendimentoRel geraRelatorioDomain(Iterable<Atendimento> atendimentos,
			String dataDe, 
			String dataAte) {
		
		Map<Integer, List<Atendimento>> mapaPagina = agruparAtendimentosPorPagina(atendimentos, 14);//new HashMap<Integer, List<Agendamento>>();
		
		List<Pagina<Atendimento>> paginas = generateListPaginaByMap(mapaPagina);
		
		if (!paginas.isEmpty()) {
			paginas.get(paginas.size() - 1).setUltimaPagina(true);
		}
		
		AtendimentoRel rel = new AtendimentoRel();
		
		rel.setPaginas(paginas);
		
		rel.setDataDe(getDateDeTimestamp(dataDe));
		rel.setDataAte(getDateAteTimestamp(dataDe));
		
		List<Atendimento> collect = StreamSupport.stream(atendimentos.spliterator(), false).collect(Collectors.toList());

		rel.setTotal(collect.size() + "");
		rel.setTotalAgendados(collect.stream().filter(a -> a.getStatusAtendimento().getId() == StatusAtendimentoEnum.AGENDADO.getCodStatus()).collect(Collectors.toList()).size() + "");
		rel.setTotalAtendidos(collect.stream().filter(a -> a.getStatusAtendimento().getId() == StatusAtendimentoEnum.ATENDIDO.getCodStatus()).collect(Collectors.toList()).size() + "");
		rel.setTotalCancelados(collect.stream().filter(a -> a.getStatusAtendimento().getId() == StatusAtendimentoEnum.CANCELADO.getCodStatus()).collect(Collectors.toList()).size() + "");
		
		return rel;
	}
	
	private String getDateDe(String dataDe) {
		LocalDateTime dataIni = null;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		if (dataDe != null) {
			dataDe += " 00:00";
			dataIni = LocalDateTime.parse(dataDe, formatter);
		}
		
		if (dataIni == null) {
			dataIni = LocalDateTime.now().minusMonths(2L);
		}
		return dataIni.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
	
	private String getDateAte(String dataAte) {
		
		LocalDateTime dataFim = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		
		if (dataAte != null) {
			dataAte += " 23:59"; 
			dataFim = LocalDateTime.parse(dataAte, formatter);
		}
		
		if (dataFim == null) {
			dataFim = LocalDateTime.now();
		}
		
		return dataFim.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
	
	private Timestamp getDateDeTimestamp(String dataDe) {
		LocalDateTime dataIni = null;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		if (dataDe != null) {
			dataDe += " 00:00";
			dataIni = LocalDateTime.parse(dataDe, formatter);
		}
		
		if (dataIni == null) {
			dataIni = LocalDateTime.now().minusMonths(2L);
		}
		return Timestamp.valueOf(dataIni);
	}
	
	private Timestamp getDateAteTimestamp(String dataAte) {
		
		LocalDateTime dataFim = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		
		if (dataAte != null) {
			dataAte += " 23:59"; 
			dataFim = LocalDateTime.parse(dataAte, formatter);
		}
		
		if (dataFim == null) {
			dataFim = LocalDateTime.now();
		}
		
		return Timestamp.valueOf(dataFim);
	}
	
	private List<Pagina<Atendimento>> generateListPaginaByMap(Map<Integer, List<Atendimento>> mapaPagina) {
		return mapaPagina.keySet().stream().map(numeroPagina -> {
			
			Pagina<Atendimento> p = new Pagina<Atendimento>();
			
			if (1 == numeroPagina) {
				p.setPrimeiraPagina(true);
			}
			
			p.setNumeroPagina(numeroPagina + "");
			p.setDominios(mapaPagina.get(numeroPagina));
			
			return p;
			
		}).collect(Collectors.toList());
	}
	
	private Map<Integer, List<Atendimento>> agruparAtendimentosPorPagina(Iterable<Atendimento> atendimentos, int qntPorPagina) {
		
		int i = 1;
		int pagina = 1;
		int size = 0;
		Map<Integer, List<Atendimento>> mapaPagina = new HashMap<Integer, List<Atendimento>>();
		
		for (Atendimento agendamento : atendimentos) {
			
			if (mapaPagina.get(pagina) == null) {
				mapaPagina.put(pagina, new ArrayList<Atendimento>());
			}
			
			mapaPagina.get(pagina).add(agendamento);
			
			if (i == qntPorPagina) {
				pagina++;
				i = 0;
			}
			
			i++;
			size++;
		}
		return mapaPagina;
	}
	
	public class AtendimentoRel {

		private Timestamp dataDe;
		private Timestamp dataAte;
		
		private List<Pagina<Atendimento>> paginas;

		private String totalAgendados;
		private String totalAtendidos;
		private String totalCancelados;
		
		private String total;
		public Timestamp getDataDe() {
			return dataDe;
		}
		public void setDataDe(Timestamp dataDe) {
			this.dataDe = dataDe;
		}
		public Timestamp getDataAte() {
			return dataAte;
		}
		public void setDataAte(Timestamp dataAte) {
			this.dataAte = dataAte;
		}
		public List<Pagina<Atendimento>> getPaginas() {
			return paginas;
		}
		public void setPaginas(List<Pagina<Atendimento>> paginas) {
			this.paginas = paginas;
		}
		
		public String getTotalAgendados() {
			return totalAgendados;
		}
		public void setTotalAgendados(String totalAgendados) {
			this.totalAgendados = totalAgendados;
		}
		public String getTotalAtendidos() {
			return totalAtendidos;
		}
		public void setTotalAtendidos(String totalAtendidos) {
			this.totalAtendidos = totalAtendidos;
		}
		public String getTotalCancelados() {
			return totalCancelados;
		}
		public void setTotalCancelados(String totalCancelados) {
			this.totalCancelados = totalCancelados;
		}
		public String getTotal() {
			return total;
		}
		public void setTotal(String total) {
			this.total = total;
		}
		
		
		
	}
	
}
