package com.mezzow.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.mezzow.domain.Agendamento;
import com.mezzow.domain.ContratoProduto;
import com.mezzow.domain.Empresa;
import com.mezzow.domain.GrupoEmpresa;
import com.mezzow.domain.Unidade;
import com.mezzow.rest.client.ContratoProdutoClient;
import com.mezzow.rest.client.EmpresaClient;
import com.mezzow.rest.client.GrupoEmpresaClient;
import com.mezzow.rest.client.UnidadeClient;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Sort;
import io.micronaut.http.HttpResponse;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

@Introspected
@Singleton
public class ContratoProdutoService {
	
	@Inject
	private ContratoProdutoClient contratoProdutoClient;
	
	@Inject
	private EmpresaClient empresaClient;
	
	@Inject
	private GrupoEmpresaClient grupoEmpresaClient;
	
	@Inject
	private UnidadeClient unidadeClient;

	
	public Page<ContratoProduto> list(
			Optional<String>  nomeProduto, 
    		Optional<String> nomeGrupoEmpresa, 
    		Optional<String> nomeEmpresa, 
    		Optional<String> nomeUnidade, 
    		Optional<Integer> max, 
    		Optional<Integer> offset, 
    		Optional<String> sort, 
    		Optional<String> order,
    		String authorization){
		
		String sortDefault = "id";
	         
		Pageable pagleable = this.createPageable(max, offset, sort, order, sortDefault);
		
		return
			Flowable.zip(
					callUnidade(nomeUnidade).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()),
					callGrupoEmpresa(nomeGrupoEmpresa).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()),
					callEmpresa(nomeEmpresa).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()), 
					(uni, grm, emp) -> {
						
						List<String> idsUnidade = uni.getBody().orElse(Collections.emptyList()).stream().map(p ->  String.valueOf(p.getId())).collect(Collectors.toList());
						List<String> idsGrupoEmpresa = grm.getBody().orElse(Collections.emptyList()).stream().map(p ->  String.valueOf(p.getId())).collect(Collectors.toList());
						List<String> idsEmpresa = emp.getBody().orElse(Collections.emptyList()).stream().map(p ->  String.valueOf(p.getId())).collect(Collectors.toList());
						
						return 
							Flowable.just(
									contratoProdutoClient.find(nomeProduto.orElse(null),idsGrupoEmpresa,idsEmpresa,idsUnidade,
											99999 , null , null , null
											/*
											pagleable.getSize(), 
											Long.valueOf(pagleable.getOffset()).intValue(),
											pagleable.getOrderBy().get(0).getProperty(),
											pagleable.getOrderBy().get(0).getDirection().name()
											*/
											)//.subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
							)
							//.subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
							//.flatMap(pcp -> pcp)
							.flatMap(find -> {
								
								List<ContratoProduto> it = find.getContent();
								
								List<String> idsUnidadesRetornados 		=  it.stream().map(p ->  p.getUnidade()!= null && p.getUnidade().getId() != null ? String.valueOf(p.getUnidade().getId()) : null).filter(Objects::nonNull).collect(Collectors.toList());
								List<String> idsGrupoEmpresaRetornados 	=  it.stream().map(p ->  p.getGrupoEmpresa()!= null && p.getGrupoEmpresa().getId() != null ? String.valueOf(p.getGrupoEmpresa().getId()) : null).filter(Objects::nonNull).collect(Collectors.toList());
								List<String> idsEmpresaRetornados 		=  it.stream().map(p ->  p.getEmpresa()!= null && p.getEmpresa().getId() != null ? String.valueOf(p.getEmpresa().getId()) : null).filter(Objects::nonNull).collect(Collectors.toList());
								
								return 
									Flowable.zip(
											
											callUnidadeByIdInListRx(idsUnidadesRetornados).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()),
											callGrupoEmpresaByIdInListRxH(idsGrupoEmpresaRetornados).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()),
											callEmpresaByIdInListRxH(idsEmpresaRetornados).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()),
											
											(uniR, grmR, empR) -> {									
												
												final Map<Long, Unidade> listaUnidadesIdMapRetornados = uniR.getBody().orElse(Collections.emptyList()).stream().collect( Collectors.toMap(Unidade::getId, p -> p)  );
												final Map<Long, GrupoEmpresa> listaGrupoEmpresasIdMapRetornados = grmR.getBody().orElse(Collections.emptyList()).stream().collect( Collectors.toMap(GrupoEmpresa::getId, p -> p)  );
												final Map<Long, Empresa> listaEmpresasIdMapRetornados = empR.getBody().orElse(Collections.emptyList()).stream().collect( Collectors.toMap(Empresa::getId, p -> p)  );
												
												it.forEach(cp -> {
													
													if (cp.getUnidade()!= null && cp.getUnidade().getId() != null) {
														cp.setUnidade(listaUnidadesIdMapRetornados.get(cp.getUnidade().getId()));
													}
													
													if (cp.getGrupoEmpresa()!= null && cp.getGrupoEmpresa().getId() != null) {
														cp.setGrupoEmpresa(listaGrupoEmpresasIdMapRetornados.get(cp.getGrupoEmpresa().getId()));
													}
													
													if (cp.getEmpresa()!= null && cp.getEmpresa().getId() != null) {
														cp.setEmpresa(listaEmpresasIdMapRetornados.get(cp.getEmpresa().getId()));
													}
												});
												
												
												List<ContratoProduto> retorno = new ArrayList<>();
					    						retorno.addAll(it);
					    						
					    						Page<ContratoProduto> retornoPageAgendamento = null;
					    						
					    						final int direction = "ASC".equalsIgnoreCase(order.orElse("ASC")) ? 1 : -1;
					    						
					    						final int offsetLocal = offset.orElse(0);
					    						final String sortLocal = sort.orElse("id");
					    						
					    						switch (sortLocal) {
													case "grupoEmpresa.nome":
														retornoPageAgendamento = Page.of(
							    								retorno
								    								.stream()
								    							    .sorted((agend1, agend2) -> {
								    							    	
								    							    	return direction * Optional.ofNullable(Optional.ofNullable(agend1.getGrupoEmpresa()).orElse(new GrupoEmpresa()).getNome()).orElse("").compareTo(Optional.ofNullable(Optional.ofNullable(agend2.getGrupoEmpresa()).orElse(new GrupoEmpresa()).getNome()).orElse(""));
								    							    })
								    							    .skip(offsetLocal * max.orElse(10))
								    						        .limit(max.orElse(10))
								    							    .collect(Collectors.toList()),
								    							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
							    							    find.getTotalSize()
						    							    );		
														break;
													case "produto.nome":
														retornoPageAgendamento = Page.of(
							    								retorno
								    								.stream()
								    							    .sorted((agend1, agend2) -> {
								    							    	return direction * agend1.getProduto().getNome().compareTo(agend2.getProduto().getNome());
								    							    })
								    							    .skip(offsetLocal * max.orElse(10))
								    						        .limit(max.orElse(10))
								    							    .collect(Collectors.toList()),
								    							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
							    							    find.getTotalSize()
						    							    );		
														break;
													case "empresa.nome":
														retornoPageAgendamento = Page.of(
							    								retorno
								    								.stream()
								    							    .sorted((agend1, agend2) -> {
								    							    	return direction * Optional.ofNullable(Optional.ofNullable(agend1.getEmpresa()).orElse(new Empresa()).getNome()).orElse("").compareTo(Optional.ofNullable(Optional.ofNullable(agend2.getEmpresa()).orElse(new Empresa()).getNome()).orElse(""));
								    							    })
								    							    .skip(offsetLocal * max.orElse(10))
								    						        .limit(max.orElse(10))
								    							    .collect(Collectors.toList()),
								    							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
							    							    find.getTotalSize()
						    							    );		
														break;
													case "unidade.nome":
														retornoPageAgendamento = Page.of(
																retorno
																.stream()
																.sorted((agend1, agend2) -> {
																	return direction * Optional.ofNullable(Optional.ofNullable(agend1.getUnidade()).orElse(new Unidade()).getNome()).orElse("").compareTo(Optional.ofNullable(Optional.ofNullable(agend2.getUnidade()).orElse(new Unidade()).getNome()).orElse(""));
																})
																.skip(offsetLocal * max.orElse(10))
																.limit(max.orElse(10))
																.collect(Collectors.toList()),
																createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
																find.getTotalSize()
																);		
														break;
													case "situacao":
														retornoPageAgendamento = Page.of(
							    								retorno
								    								.stream()
								    							    .sorted((agend1, agend2) -> {
								    							    	return direction * agend1.getSituacao().compareTo(agend2.getSituacao());
								    							    })
								    							    .skip(offsetLocal * max.orElse(10))
								    						        .limit(max.orElse(10))
								    							    .collect(Collectors.toList()),
								    							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
							    							    find.getTotalSize()
						    							    );		
														break;
													default:
														retornoPageAgendamento = Page.of(
							    								retorno
								    								.stream()
								    							    .sorted((agend1, agend2) -> {
								    							    	return -1 * agend1.getDataCadastro().compareTo(agend2.getDataCadastro());
								    							    })
								    							    .skip(offsetLocal * max.orElse(10))
								    						        .limit(max.orElse(10))
								    							    .collect(Collectors.toList()),
								    							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
							    							    find.getTotalSize()
															);
														break;
												}
					    						
					    						
					    						
					    						return retornoPageAgendamento;
												
											})//.subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
											;
								
							});
					}).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).flatMap(l -> l).blockingLast();
		
	}
	
	private Pageable createPageable(Integer offset, Integer max, String sort, String order) {
		return Pageable.from(Optional.ofNullable(offset).orElse(0),max == null ? 10 : max, Sort.of(new Sort.Order(sort != null ? sort : "id", order != null ? Sort.Order.Direction.valueOf(order.toUpperCase()) : Sort.Order.Direction.ASC, true)) );
	}
	
	private List<Empresa> callEmpresaByIdInList(List<String> idsEmpresaRetornados) {
		
		if (idsEmpresaRetornados == null || idsEmpresaRetornados.isEmpty()) {
			return Collections.emptyList();
		}
		
		return empresaClient.findByIdInList(idsEmpresaRetornados);
	}
	
	private Flowable<List<Empresa>> callEmpresaByIdInListRx(List<String> idsEmpresaRetornados) {
		
		if (idsEmpresaRetornados == null || idsEmpresaRetornados.isEmpty()) {
			return Flowable.just(Collections.emptyList());
		}
		
		return empresaClient.findByIdInListRx(idsEmpresaRetornados);
	}
	
	private Flowable<HttpResponse<List<Empresa>>> callEmpresaByIdInListRxH(List<String> idsEmpresaRetornados) {
		
		if (idsEmpresaRetornados == null || idsEmpresaRetornados.isEmpty()) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		return empresaClient.findByIdInListRxH(idsEmpresaRetornados);
	}	

	private List<GrupoEmpresa> callGrupoEmpresaByIdInList(
			List<String> idsGrupoEmpresaRetornados) {
		
		if (idsGrupoEmpresaRetornados == null || idsGrupoEmpresaRetornados.isEmpty()) {
			return Collections.emptyList();
		}
		
		
		return grupoEmpresaClient.findByIdInList(idsGrupoEmpresaRetornados);
	}
	
	private Flowable<List<GrupoEmpresa>> callGrupoEmpresaByIdInListRx(
			List<String> idsGrupoEmpresaRetornados) {
		
		if (idsGrupoEmpresaRetornados == null || idsGrupoEmpresaRetornados.isEmpty()) {
			return Flowable.just(Collections.emptyList());
		}
		
		
		return grupoEmpresaClient.findByIdInListRx(idsGrupoEmpresaRetornados);
	}
	
	private Flowable<HttpResponse<List<GrupoEmpresa>>> callGrupoEmpresaByIdInListRxH(
			List<String> idsGrupoEmpresaRetornados) {
		
		if (idsGrupoEmpresaRetornados == null || idsGrupoEmpresaRetornados.isEmpty()) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		
		return grupoEmpresaClient.findByIdInListRxH(idsGrupoEmpresaRetornados);
	}

	private List<Unidade> callUnidadeByIdInList(List<String> idsUnidadesRetornados) {
		
		if (idsUnidadesRetornados == null || idsUnidadesRetornados.isEmpty()) {
			return Collections.emptyList();
		}
		
		return unidadeClient.findByIdInList(idsUnidadesRetornados);
	}
	
	private Flowable<HttpResponse<List<Unidade>>> callUnidadeByIdInListRx(List<String> idsUnidadesRetornados) {
		
		if (idsUnidadesRetornados == null || idsUnidadesRetornados.isEmpty()) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		return unidadeClient.findByIdInListRx(idsUnidadesRetornados);
	}

	private Pageable createPageable(Optional<Integer> max, Optional<Integer> offset, Optional<String> sort,
			Optional<String> order, String sortDefault) {
		return Pageable.from(offset.orElse(0), max.orElse(10), Sort.of(new Sort.Order(sort.orElse(sortDefault), order.isPresent() ? Sort.Order.Direction.valueOf(order.get().toUpperCase()) : Sort.Order.Direction.ASC , true)));
	}

	
	private Flowable<HttpResponse<List<Empresa>>> callEmpresa(Optional<String> nomeEmpresa) {
		if (!nomeEmpresa.isPresent() || nomeEmpresa.isEmpty() || nomeEmpresa.get().isEmpty()) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		return empresaClient.findByNomeRxH(nomeEmpresa.get());///*.timeout(5, TimeUnit.SECONDS).retry()*/.blockingFirst().getBody().orElse(null);
	}
	
	/**
	 * 
	 * @param nomeEmpresa
	 * @return
	 */
	private Flowable<HttpResponse<List<GrupoEmpresa>>> callGrupoEmpresa(Optional<String> nomeGrupoEmpresa) {
		if (!nomeGrupoEmpresa.isPresent() || nomeGrupoEmpresa.isEmpty() || nomeGrupoEmpresa.get().isEmpty()) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		return grupoEmpresaClient.findByNomeRxH(nomeGrupoEmpresa.get());///*.timeout(5, TimeUnit.SECONDS).retry()*/.blockingFirst().getBody().orElse(null);
	}
	
	/**
	 * 
	 * @param nomeEmpresa
	 * @return
	 */
	private Flowable<HttpResponse<List<Unidade>>> callUnidade(Optional<String> nomeUnidade) {
		
		if (!nomeUnidade.isPresent() || nomeUnidade.isEmpty() || nomeUnidade.get().isEmpty()) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		return unidadeClient.findByNomeRxH(nomeUnidade.get());
	}
	
}	
