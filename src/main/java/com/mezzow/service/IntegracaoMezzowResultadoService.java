package com.mezzow.service;

import static io.micronaut.http.HttpRequest.POST;

import java.io.IOException;
import java.util.Base64;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.commons.codec.digest.DigestUtils;

//import com.google.common.collect.Iterables;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mezzow.domain.Atendimento;
import com.mezzow.domain.Perfil;
import com.mezzow.rest.client.AtendimentoClient;
import com.mezzow.rest.client.PerfilClient;

import io.micronaut.context.annotation.Value;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;

@Singleton
public class IntegracaoMezzowResultadoService {
	
private final RxHttpClient rxClient;

	@Inject
	AtendimentoClient atendimentoClient;
	
	@Inject
	PerfilClient perfilClient;
	
	@Inject
	AtendimentoService atendimentoService;
	
	@Value("${micronaut.http.services.mezzowresultado.token}")
	String mezzowResultadoToken;
	
	private final Gson gson = new Gson();
	
//	private static final String MEZZOWRESULTADO_TOKEN 			= "jvq90o7qm5oknv42o3jtl0o7pe8ikgcl";
	private static final String CODIGO_EMPRESA_NAO_ENCONTRADA 	= "001";
	private static final String CODIGO_CERTIFICADO_EXPIRADO 	= "002";
	private static final String CODIGO_MAIS_DE_UM_CRM 			= "003";
	private static final String CODIGO_FALHA 					= "004";
	
	public IntegracaoMezzowResultadoService(@Client(id = "mezzowresultado")RxHttpClient rxClient) {
		this.rxClient = rxClient; 
	}
	
	public void generateIntegracaoComMezzowResultados(Long atendimentoId) throws IOException {
		generateIntegracaoComMezzowResultados(this.atendimentoService.show(atendimentoId).orElseThrow());
	}
	
	public void generateIntegracaoComMezzowResultados(Atendimento atendimentoFull) throws IOException {
		//relatorio externo verificar
		Long atendimentoId 				= atendimentoFull.getId();
		Long unidadeId 					= atendimentoFull.getRegistroPreAtendimento().getAgendamento().getUnidade().getId();
		Long exameId 					= atendimentoFull.getRegistroPreAtendimento().getAgendamento().getTipoConsulta().getId();
		String cnpjEmpresa 				= atendimentoFull.getRegistroPreAtendimento().getAgendamento().getUnidade().getEmpresa().getCnpj();
		String nomePaciente 			= atendimentoFull.getRegistroPreAtendimento().getAgendamento().getPaciente().getNome();
		String cpfPaciente 				= atendimentoFull.getRegistroPreAtendimento().getAgendamento().getPaciente().getCpf();
		String sexoPaciente 			= atendimentoFull.getRegistroPreAtendimento().getAgendamento().getPaciente().getSexo();
		String idConnect 				= "";
		String storage 					= "";
		String tipo 					= atendimentoFull.getRegistroPreAtendimento().getAgendamento().getTipoConsulta().getNome();
		String modalidade 				= atendimentoFull.getRegistroPreAtendimento().getAgendamento().getTipoConsulta().getTipoAtendimento().getNome();
		String totalImagens 			= "1";
		String cpfMedico 				= atendimentoFull.getRegistroPreAtendimento().getMedico().getDadosPessoais().getCpf();
		String nomeMedico 				= atendimentoFull.getRegistroPreAtendimento().getMedico().getNomeExibicao();
		String crmMedico 				= atendimentoFull.getRegistroPreAtendimento().getMedico().getDadosProfissionais().getCrm();
		String crmUfMedico 				= atendimentoFull.getRegistroPreAtendimento().getMedico().getDadosProfissionais().getCrmUf().getSigla();
		String nomeArquivoPfd 			= "arquivo.pdf";
		
		byte[] byteArrayArquivoPdf 		= atendimentoService.generateStreamPDFRelatorioMedico(atendimentoFull.getId());
		
		String md5ChecksumDoAqruivoPdf 	= DigestUtils.md5Hex(byteArrayArquivoPdf);
		String base64ArquivoPdf 		= Base64.getEncoder().encodeToString(byteArrayArquivoPdf);
		
		String modelRsultado = "{\n"
				+ " \"versao\": '1.0.0',"
				+ " \"id\": "
				+ "			\""+atendimentoId+"\",\n"
				+ "	\"empresa\": {\n"
				+ "		\"cnpj\": "
				+ "			\""+cnpjEmpresa+"\"\n"
				+ "	},\n"
				+ "	\"unidade\": {\n"
				+ "		\"id\": "
				+ "			\""+unidadeId+"\"\n"
				+ "	},\n"
				+ "	\"paciente\": {\n"
				+ "		\"nome\": \""+nomePaciente+"\",\n"
				+ "		\"cpf\": \""+cpfPaciente+"\",\n"
				+ "		\"sexo\": \""+sexoPaciente+"\",\n"
				+ "	},\n"
				+ "	\"exame\": {\n"
				+ "		\"id\": "
				+ "			\""+exameId+"\",\n"
				+ "		\"idConnect\": "
				+ "			\""+idConnect+"\",\n"
				+ "		\"storage\": "
				+ "			\""+storage+"\",\n"
				+ "		\"tipo\": "
				+ "			\""+tipo+"\",\n"
				+ "		\"modalidade\": "
				+ "			\""+modalidade+"\",\n"
				+ "		\"totalImagens\": "
				+ "			\""+totalImagens+"\"\n"
				+ "	},\n"
				+ "	\"arquivo\": {\n"
				+ "		\"assinaturas\": [\n"
				+ "			{,\n"
				+ "				\"assinar\": \""+nomePaciente+"\",\n"
				+ "				\"cpf\": \""+cpfMedico+"\",\n"
				+ "				\"nome\": \""+nomeMedico+"\",\n"
				+ "				\"crm\": \""+crmMedico+"\",\n"
				+ "				\"crmUf\": \""+crmUfMedico+"\",\n"
				+ "			},\n"
				+ "		],\n"
				+ "		\"possuiImagem\": \""+nomePaciente+"\",\n"
				+ "		\"nome\": \""+nomeArquivoPfd+"\",\n"
				+ "		\"md5\": \""+md5ChecksumDoAqruivoPdf+"\",\n"
				+ "		\"base64\": \""+base64ArquivoPdf+"\",\n"
				+ "	},\n"
				+ "}\n"
				+ "";
		
		System.out.println("5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555");
		System.out.println("5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555");
		System.out.println("5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555");
		System.out.println("5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555");
		System.out.println(modelRsultado);
		System.out.println("5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555");
		System.out.println("5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555");
		System.out.println("5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555");
		System.out.println("5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555");
		System.out.println(mezzowResultadoToken);
		
		
		rxClient.retrieve( POST("/api/publicar", modelRsultado ).header("Content-Type", "application/json").header("X-Auth-Token", mezzowResultadoToken))
		.flatMap(mezzowResultadoReponse -> {
			System.out.println("************************************************************************************************************************************************");
			System.out.println("mezzowResultadoReponse");
			System.out.println(mezzowResultadoReponse);
			JsonObject asJsonObject = gson.fromJson(mezzowResultadoReponse, JsonElement.class).getAsJsonObject();
			
			
			//criar tabela resultado com atendimento relacionado
			
			if (CODIGO_CERTIFICADO_EXPIRADO.equalsIgnoreCase(asJsonObject.get("codigo").getAsString())) {
				
				Perfil perfilMedico = perfilClient.show(atendimentoFull.getRegistroPreAtendimento().getMedico().getId()).orElseThrow();
				
				perfilMedico.getDadosProfissionais().setCertificadoValido(Boolean.FALSE);
				
				perfilClient.update(perfilMedico);
			}
			
			return null;
		});
	}
}
