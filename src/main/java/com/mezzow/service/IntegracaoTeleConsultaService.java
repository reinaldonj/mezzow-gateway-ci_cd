package com.mezzow.service;

import static io.micronaut.http.HttpRequest.POST;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.commons.lang3.StringUtils;

//import com.google.common.collect.Iterables;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mezzow.domain.Atendimento;
import com.mezzow.rest.client.AtendimentoClient;

import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Singleton
public class IntegracaoTeleConsultaService {
	
	private final RxHttpClient rxClient;
	
	private final Gson gson = new Gson();
	
	@Inject
	AtendimentoClient atendimentoClient;
	
	//AtendimentoService atendimentoService;
	
	private static final String TELECONSULTA_USERNAME = "opty";
	private static final String TELECONSULTA_PASSWORD = "opty123456";
	
	
	public IntegracaoTeleConsultaService(@Client(id = "teleconsulta")RxHttpClient rxClient//, AtendimentoService atendimentoService
			) {
		this.rxClient = rxClient; 
		//this.atendimentoService = atendimentoService;
	}
	
	//public void generateTeleConsultaAndPersisteNoAtendimento(Long atendimentoId)  {
		//generateTeleConsultaAndPersisteNoAtendimento(this.atendimentoService.show(atendimentoId).orElseThrow());
	//
	
	public void generateTeleConsultaAndPersisteNoAtendimento(Atendimento atendimentoFull) {
		
		TeleConsulta teleConsultaSaved 	= generateTeleConsulta(atendimentoFull);
		
		persisteTeleConsultaNoAtendimento(atendimentoFull, teleConsultaSaved);
		
	}
	
	private void persisteTeleConsultaNoAtendimento(Atendimento atendimentoSaved, TeleConsulta teleConsultaSaved) {
		
//		TeleConsulta tcAtendimento = new TeleConsulta();
//		tcAtendimento.setId(teleConsultaSaved.getId());
//		
		System.out.println(teleConsultaSaved.getLinkMedico().length());
		System.out.println(teleConsultaSaved.getLinkMedico());
		System.out.println(teleConsultaSaved.getLinkPaciente().length());
		System.out.println(teleConsultaSaved.getLinkPaciente());
		atendimentoSaved.setLinkTeleConsultaMedico(teleConsultaSaved.getLinkMedico());
		atendimentoSaved.setLinkTeleConsultaPaciente(teleConsultaSaved.getLinkPaciente());
		atendimentoSaved.setIdLinkTeleConsultaMedico(teleConsultaSaved.getIdLinkMedico());
		atendimentoSaved.setIdLinkTeleConsultaPaciente(teleConsultaSaved.getIdLinkPaciente());
		atendimentoSaved.setDataHoraTeleConsulta( this.stringToTimeStamp( teleConsultaSaved.getDataHora() ) );
		atendimentoSaved.setIdTeleConsulta(teleConsultaSaved.getIdExterno());
		
		
		
		//atendimentoSaved.setTeleConsulta(tcAtendimento);
		atendimentoClient.update(atendimentoSaved);
		
	}
	
	private TeleConsulta generateTeleConsulta(Atendimento atendimentoSved) {
		
		// chama post para gerar token
		// chama post para recuperar link teleconsulta
		// gravar esse link na tebela de teleconsulta 
		// na tela de Atendimento add esse link no botao de TeleConsulta
		System.out.println("************************************************************************************************************************************************");
		System.out.println("generateTeleConsulta");
		
		Flowable<TeleConsulta> teleConsultaSaved = rxClient
			.retrieve(POST("/login", "{\"username\": \""+TELECONSULTA_USERNAME+"\", \"password\": \""+TELECONSULTA_PASSWORD+"\"}").header("Content-Type", "application/json") )
			.map(tokenResponse -> {
				System.out.println("************************************************************************************************************************************************");
				System.out.println("tokenResponse");
				System.out.println(tokenResponse);
				return  gson.fromJson(tokenResponse, JsonElement.class).getAsJsonObject().get("token").getAsString();
			})
			.flatMap(token -> {
				System.out.println("************************************************************************************************************************************************");
				System.out.println("token");
				System.out.println(token);
				return 
						rxClient.retrieve( POST("/consulta", generateTeleconsultaRequestByAtendimento(atendimentoSved) ).header("Content-Type", "application/json").header("Authorization", "Bearer "+token) )
							.flatMap(consultaReponse -> {
								System.out.println("************************************************************************************************************************************************");
								System.out.println("consultaReponse");
								System.out.println(consultaReponse);
								TeleConsulta tc = generateTeleConsultaByConsultaResponse(consultaReponse); 
								return Flowable.just(tc);
						});
				
				
			})
//			.map(tc -> {
//				return  persistTeleConsulta(tc) ;
//				
//			})
			;
		
		return teleConsultaSaved.blockingLast();

	}
	
	private TeleConsulta generateTeleConsultaByConsultaResponse(String consultaReponse) {
		System.out.println("************************************************************************************************************************************************");
		System.out.println("generateTeleConsultaByConsultaResponse");
		System.out.println("consultaReponse");
		JsonObject asJsonObject = gson.fromJson(consultaReponse, JsonElement.class).getAsJsonObject();
		
		TeleConsulta retorno = new TeleConsulta();
		
		if (asJsonObject.has("id")) {
			
			retorno.setIdExterno(asJsonObject.get("id").getAsString());
			retorno.setDataHora(asJsonObject.get("dataHora").getAsString());
			retorno.setLinkMedico(asJsonObject.get("medico").getAsJsonObject().get("link").getAsString()); 
			retorno.setLinkPaciente(asJsonObject.get("paciente").getAsJsonObject().get("link").getAsString()); 
			retorno.setIdLinkMedico(asJsonObject.get("medico").getAsJsonObject().get("id").getAsString()); 
			retorno.setIdLinkPaciente(asJsonObject.get("paciente").getAsJsonObject().get("id").getAsString());
		}
		
		return retorno;
	}
	
	private String generateTeleconsultaRequestByAtendimento(Atendimento atendimentoSved) {
		
		Long idTipoConsulta 			= 0L;
		Long idPaciente 				= 0L;
		Long idEmpresa 					= 0L;
		String idMedico 				= "";
        String nomePaciente 			= "";
        String nomeMedico 				= "";
        String userNameMedico			= "";
        String cpfMedico 				= "";
        String cpfPaciente 				= "";
        String dataNascimentoPaciente 	= "";
        String dataConsulta 			= "";
		String password 				= "";
		String crm 						= "";
        
		try {
			
			Date dataPaciente = new Date();
			dataPaciente.setTime(atendimentoSved.getRegistroPreAtendimento().getAgendamento().getPaciente().getDataNascimento().getTime());
			
			idTipoConsulta 			= atendimentoSved.getRegistroPreAtendimento().getAgendamento().getTipoConsulta().getId();
			idPaciente 				= atendimentoSved.getRegistroPreAtendimento().getAgendamento().getPaciente().getId();
	        nomePaciente 			= atendimentoSved.getRegistroPreAtendimento().getAgendamento().getPaciente().getNome();
	        cpfPaciente				= atendimentoSved.getRegistroPreAtendimento().getAgendamento().getPaciente().getCpf();
	        dataNascimentoPaciente 	= new SimpleDateFormat("dd/MM/yyyy").format(dataPaciente);
	        dataConsulta 			= new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(atendimentoSved.getRegistroPreAtendimento().getAgendamento().getDataConsulta())/*.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"))*/;
	        idMedico 				= atendimentoSved.getRegistroPreAtendimento().getMedico().getId();
	        nomeMedico				= atendimentoSved.getRegistroPreAtendimento().getMedico().getNomeExibicao();
	        userNameMedico			= atendimentoSved.getRegistroPreAtendimento().getMedico().getUsuarioChat();
	        cpfMedico				= atendimentoSved.getRegistroPreAtendimento().getMedico().getDadosPessoais().getCpf();
	        idEmpresa 				= atendimentoSved.getRegistroPreAtendimento().getAgendamento().getUnidade().getEmpresa().getId();
	        password				= atendimentoSved.getRegistroPreAtendimento().getMedico().getSenhaChat();
	        crm						= atendimentoSved.getRegistroPreAtendimento().getMedico().getDadosProfissionais().getCrm();
	        
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
	        
		String retorno = "{\n"
				+ "	\"medico\": {\n"
				+ "		\"id\": "
				+ "			\""+idMedico+"\",\n"
				+ "		\"nome\": "
				+ "			\""+nomeMedico+"\",\n"
				+ "		\"cpf\": "
				+ "			\""+cpfMedico+"\",\n"
				+ "		\"username\": "
				+ "			\""+userNameMedico+"\",\n"
				+ "		\"password\": "
				+ "			\""+password+"\",\n"
				+ "		\"crm\": "
				+ "			\""+crm+"\"\n"
				+ "	},\n"
				+ "	\"empresa\": {\n"
				+ "		\"id\": "
				+ "			\""+idEmpresa+"\"\n"
				+ "	},\n"
				+ "	\"paciente\": {\n"
				+ "		\"id\": \""+idPaciente+"\",\n"
				+ "		\"nome\": \""+nomePaciente+"\",\n"
				+ "		\"cpf\": \""+cpfPaciente+"\",\n"
				+ "		\"dataNascimento\": \""+dataNascimentoPaciente+"\",\n"
				+ "		\"username\": \""+cpfPaciente+"\",\n"
				+ "		\"password\": \""+password+"\"\n"
				+ "	},\n"
				+ "	\"prontuario\": \""+idTipoConsulta+"\",\n"
				+ "	\"dataHora\": \""+dataConsulta+"\"\n"
				+ "}\n"
				+ "";
		
		
		System.out.println("************************************************************************************************************************************************");
		System.out.println("generateTeleconsultaRequestByAtendimento");
		System.out.println(retorno);
		
		return retorno;
	}
	
	private Timestamp stringToTimeStamp(String dataHora) {
		
		
		if (StringUtils.isBlank(dataHora)) return null;
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS");
	    Date parsedDate;
		try {
			parsedDate = dateFormat.parse(dataHora);
			Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
			return timestamp;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	    
		
		return null;
	}
	
	public class TeleConsulta {
		
		private Long id;
		
		private String idExterno;
		
		private String linkMedico;
		
		private String linkPaciente;
		
		private String idLinkMedico;
		
		private String idLinkPaciente;
		
		private String dataHora;
		
		public String getLinkMedico() {
			return linkMedico;
		}
	
		public void setLinkMedico(String linkMedico) {
			this.linkMedico = linkMedico;
		}
	
		public String getLinkPaciente() {
			return linkPaciente;
		}
	
		public void setLinkPaciente(String linkPaciente) {
			this.linkPaciente = linkPaciente;
		}
	
		
		public Long getId() {
			return id;
		}
	
		public void setId(Long id) {
			this.id = id;
		}
		
		public String getIdExterno() {
			return idExterno;
		}
	
		public void setIdExterno(String idExterno) {
			this.idExterno = idExterno;
		}

		public String getIdLinkMedico() {
			return idLinkMedico;
		}

		public void setIdLinkMedico(String idLinkMedico) {
			this.idLinkMedico = idLinkMedico;
		}

		public String getIdLinkPaciente() {
			return idLinkPaciente;
		}

		public void setIdLinkPaciente(String idLinkPaciente) {
			this.idLinkPaciente = idLinkPaciente;
		}

		public String getDataHora() {
			return dataHora;
		}

		public void setDataHora(String dataHora) {
			this.dataHora = dataHora;
		}
		
		
		
	}
}
