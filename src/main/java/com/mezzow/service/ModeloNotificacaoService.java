package com.mezzow.service;

import com.mezzow.domain.GrupoEmpresa;
import com.mezzow.domain.ModeloNotificacao;
import com.mezzow.domain.Unidade;
import com.mezzow.rest.client.GrupoEmpresaClient;
import com.mezzow.rest.client.ModeloNotificacaoClient;
import com.mezzow.rest.client.UnidadeClient;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

@Introspected
@Singleton
public class ModeloNotificacaoService {

	@Inject
	private ModeloNotificacaoClient client;

	@Inject
	private GrupoEmpresaClient grupoEmpresaClient;

	@Inject
	private UnidadeClient unidadeClient;

	public Single<Optional<ModeloNotificacao>> show(Long id) {

		return Single.fromObservable(Observable.just(client.show(id)))
				.subscribeOn(Schedulers.io())
				.flatMap(modeloNotificacao -> {
//					System.out.println("################################ GRUPO EMPRESA");
					if(modeloNotificacao.isPresent()) {
						if (modeloNotificacao.get().getGrupoEmpresa() != null) {
							modeloNotificacao.get().setGrupoEmpresa(grupoEmpresaClient.show(modeloNotificacao.get().getGrupoEmpresa().getId()).get());
						}
					}
					return Single.just(Optional.of(modeloNotificacao.get()));
				}).flatMap(modeloNotificacao -> {
//					System.out.println("################################ UNIDADE");
					if(modeloNotificacao.isPresent()) {
						if (modeloNotificacao.get().getUnidade() != null) {
							modeloNotificacao.get().setUnidade(unidadeClient.show(modeloNotificacao.get().getUnidade().getId()).get());
						}
					}
					return Single.just(Optional.of(modeloNotificacao.get()));
				});
	}

	public Single<Page<ModeloNotificacao>> find(
			@Nullable String grupoEmpresa,
			@Nullable String unidade,
			@Nullable String tipoNotificacao,
			@Nullable String titulo,
			@Nullable Integer max,
			@Nullable Integer offset,
			@Nullable String sort,
			@Nullable String order) {

			List<Long> grupoEmpresaIds = null;
			if (grupoEmpresa != null) {
				Page<GrupoEmpresa> grupoEmpresaList = grupoEmpresaClient.find(grupoEmpresa, grupoEmpresa, -1, null, null, null);

				/*
				return Observable.fromIterable(grupoEmpresaList.getContent())
						.subscribeOn(Schedulers.io())
						.map(g -> g.getId())
						.toList()
						.flatMap(gpIds ->
							Observable.fromIterable(client.find(gpIds, null, null, null, max, offset, sort, order).getContent())
								.subscribeOn(Schedulers.io())
								.flatMap(modeloNotificacao -> {
									System.out.println("################################ GRUPO EMPRESA");
									if(modeloNotificacao.getGrupoEmpresa() !=null) {
										modeloNotificacao.setGrupoEmpresa(grupoEmpresaClient.show(modeloNotificacao.getGrupoEmpresa().getId()).get());
									}
									return Single.just(modeloNotificacao).toObservable();
								})
								.flatMap(modeloNotificacao -> {
									System.out.println("################################ UNIDADE");
									if (modeloNotificacao.getUnidade() != null) {
										modeloNotificacao.setUnidade(unidadeClient.show(modeloNotificacao.getUnidade().getId()).get());
									}
									return Single.just(modeloNotificacao).toObservable();
								})
								.toList()
								.map(p -> Page.of(p, Pageable.from(1, 10), 1))
						);
				 */

				grupoEmpresaIds = Observable.fromIterable(grupoEmpresaList.getContent())
						.subscribeOn(Schedulers.io())
						.map(g -> g.getId())
						.toList()
						.blockingGet();

				if (grupoEmpresaIds.isEmpty()) {
					grupoEmpresaIds.add(-1l);
				}
			}

//			System.out.println("---------------------> \n\n" +  grupoEmpresaIds);

			List<Long> unidadeIds = null;
			if (unidade != null) {
				Page<Unidade> unidadeList = unidadeClient.find(null, unidade, null, -1, null, null, null);
				unidadeIds = Observable.fromIterable(unidadeList.getContent())
						.subscribeOn(Schedulers.io())
						.map(g -> g.getId())
						.toList()
						.blockingGet();

				if (unidadeIds.isEmpty()) {
					unidadeIds.add(-1l);
				}
			}

//			System.out.println("---------------------> \n\n" +  unidadeIds);

			return Observable.fromIterable(client.find(grupoEmpresaIds, unidadeIds, tipoNotificacao, titulo, max, offset, sort, order))
				.subscribeOn(Schedulers.io())
				/*.doOnNext(modeloNotificacao -> {
					System.out.println("################################ GRUPO EMPRESA");
					if(modeloNotificacao.getGrupoEmpresa() !=null) {
						modeloNotificacao.setGrupoEmpresa(grupoEmpresaClient.show(modeloNotificacao.getGrupoEmpresa().getId()).get());
					}
				})
				.doOnNext(modeloNotificacao -> {
					System.out.println("################################ UNIDADE");
					if (modeloNotificacao.getUnidade() != null) {
						modeloNotificacao.setUnidade(unidadeClient.show(modeloNotificacao.getUnidade().getId()).get());
					}
				})*/
				.flatMap(modeloNotificacao -> {
//					System.out.println("################################ GRUPO EMPRESA");
					if(modeloNotificacao.getGrupoEmpresa() !=null) {
						
						modeloNotificacao.setGrupoEmpresa(grupoEmpresaClient.show(modeloNotificacao.getGrupoEmpresa().getId()).orElse(null));
					}
					return Single.just(modeloNotificacao).toObservable();
				})
				.flatMap(modeloNotificacao -> {
//					System.out.println("################################ UNIDADE");
					if (modeloNotificacao.getUnidade() != null) {
						modeloNotificacao.setUnidade(unidadeClient.show(modeloNotificacao.getUnidade().getId()).get());
					}
					return Single.just(modeloNotificacao).toObservable();
				})
				.toList()
				.map(p -> Page.of(p, Pageable.from(1, 10), 1))
				.doOnError(e -> System.out.println("*************************************************************************************" + e))
				.doOnSuccess(s -> System.out.println("sucesso final!"))
				;


										/*
								Observable.fromIterable(client.find(mapProduto.keySet().parallelStream().map(Object::toString).collect(Collectors.toList()),null,null,null,max.orElse(50),offset.orElse(null),sort.orElse("id"),order.orElse("desc")))
										.subscribeOn(Schedulers.io())
										.map( cp -> new ContratoProdutoResponse(cp.getId(), mapProduto.get(cp.getProdutoId()),  cp.getGrupoEmpresa(), cp.getUnidade(), cp.getEmpresa(), cp.getDataCadastro(), cp.getDataIniVigencia(), cp.getDataFimVigencia() ))
										.toList()
										.map(p -> Page.of(p, Pageable.from(1, 10), 1))
//							.doOnSuccess(s -> System.out.println("sucesso contrato produto"))
								 */


		/*List<ModeloNotificacao> list = find.getContent().stream().map(item ->
			new ModeloNotificacao(item.getId(), item.getNome(), item.getDescricao(), produtoClient.show(item.getProdutoId()).get())
		).collect(Collectors.toList());*/

		//return find;//Page.of(list, find.getPageable(), find.getTotalSize());



	}
}
