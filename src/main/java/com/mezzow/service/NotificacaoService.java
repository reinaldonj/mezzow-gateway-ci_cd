package com.mezzow.service;

import java.sql.Timestamp;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.mezzow.domain.AgendaNotificacao;
import com.mezzow.domain.GrupoEmpresa;
import com.mezzow.domain.MeioComunicacao;
import com.mezzow.domain.ModeloNotificacao;
import com.mezzow.domain.Notificacao;
import com.mezzow.domain.Paciente;
import com.mezzow.domain.Unidade;
import com.mezzow.domain.enums.UnidadeTempoEnum;
import com.mezzow.rest.client.ModeloNotificacaoClient;
import com.mezzow.rest.client.NotificacaoClient;
import com.mezzow.rest.client.PacienteClient;
import com.mezzow.rest.client.UnidadeClient;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.client.exceptions.HttpClientResponseException;

@Introspected
@Singleton
public class NotificacaoService {
	
	@Inject
	private NotificacaoClient notificacaoClient;

	@Inject
	private UnidadeClient unidadeClient;
	
	@Inject
	private ModeloNotificacaoClient modeloNotificacaoClient;
	
	@Inject
	private PacienteClient pacienteClient;
	
	public Notificacao save(Notificacao notificacao) {
		
		if(notificacao.getPaciente() != null) {
			Optional<Paciente> paciente = pacienteClient.show(notificacao.getPaciente().getId());
			if(paciente.isPresent()) {
				notificacao.setPaciente(paciente.get());
			}
		}
		
		if(notificacao.getUnidade() != null && notificacao.getTipoNotificacao() != null) {
			
			Optional<ModeloNotificacao> modeloNotificacao = modeloNotificacaoClient.findAllByUnidadeIdAndTipoNotificacaoId(
					notificacao.getUnidade().getId(), notificacao.getTipoNotificacao().getId()).stream().findFirst(); // BUSCAR O MODELO PELA UNIDADE E O TIPO DE NOTIFICAÇÃO
			
			if(modeloNotificacao.isPresent()) {
				sendNotificacao(notificacao, modeloNotificacao);
			} else {
				Optional<Unidade> unidade = unidadeClient.show(notificacao.getUnidade().getId());
				GrupoEmpresa grupoEmpresa = unidade.get().getEmpresa().getGrupoEmpresa();
				
				if(grupoEmpresa != null) {
					modeloNotificacao = modeloNotificacaoClient.findAllByGrupoEmpresaIdAndTipoNotificacaoId(grupoEmpresa.getId(), notificacao.getTipoNotificacao().getId()).stream().findFirst();
					if(modeloNotificacao.isPresent()) {
						sendNotificacao(notificacao, modeloNotificacao);
					}
				}
			}
			
			if(modeloNotificacao.isEmpty()) {
				throw new HttpClientResponseException("Modelo de Notificação não encontrado", HttpResponse.badRequest());
			}
		} else {
			if(notificacao.getUnidade() == null) {
				throw new HttpClientResponseException("Unidade não informada!", null);				
			} else {
				throw new HttpClientResponseException("Tipo de notificação não informada!", null);				
			}
		}
		return notificacao;
	}
	
	private void sendNotificacao(Notificacao notificacao, Optional<ModeloNotificacao> modeloNotificacao) {
		notificacao.setTipoNotificacao(modeloNotificacao.get().getTipoNotificacao());
		
		for(MeioComunicacao meio : modeloNotificacao.get().getMeiosComunicacao()) {
			
			for(AgendaNotificacao agenda : modeloNotificacao.get().getAgendasNotificacao()) {
			
				Timestamp dataEmissao = null;
				if(agenda.getUnidade().equals(UnidadeTempoEnum.MINUTO)) {
					dataEmissao = Timestamp.valueOf(notificacao.getDataReferencia().toLocalDateTime().plus(-agenda.getValor(), ChronoUnit.MINUTES));
					notificacao.setDataEmissao(dataEmissao);
				} else if(agenda.getUnidade().equals(UnidadeTempoEnum.HORA)) {
					dataEmissao = Timestamp.valueOf(notificacao.getDataReferencia().toLocalDateTime().plus(-agenda.getValor(), ChronoUnit.HOURS));
					notificacao.setDataEmissao(dataEmissao);
				} else if(agenda.getUnidade().equals(UnidadeTempoEnum.DIA)) {
					dataEmissao = Timestamp.valueOf(notificacao.getDataReferencia().toLocalDateTime().plus(-agenda.getValor(), ChronoUnit.DAYS));
					notificacao.setDataEmissao(dataEmissao);
				} else if(agenda.getUnidade().equals(UnidadeTempoEnum.MES)) {
					dataEmissao = Timestamp.valueOf(notificacao.getDataReferencia().toLocalDateTime().plus(-agenda.getValor(), ChronoUnit.MONTHS));
					notificacao.setDataEmissao(dataEmissao);
				}
				
				notificacaoClient.save(new Notificacao(notificacao.getId(), notificacao.getDomain(), notificacao.getDomainId(),
								notificacao.getDataCadastro(), notificacao.getDataReferencia(), dataEmissao,
								notificacao.getDataEnvio(), notificacao.getUnidade(),
								notificacao.getTipoNotificacao(), meio, notificacao.getPaciente(), 
								notificacao.getConteudoNotificacao(), notificacao.getDestinatarioNome(),
								notificacao.getDestinatarioEmail(), notificacao.getDestinatarioTelefone(),
								notificacao.getRemetenteEmail(), notificacao.getTentativaEnvio()));
			}
		}
	}
}	
