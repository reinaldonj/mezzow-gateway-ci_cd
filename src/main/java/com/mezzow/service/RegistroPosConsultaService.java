package com.mezzow.service;

import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.mezzow.domain.RegistroPosConsulta;
import com.mezzow.rest.client.ModeloPosConsultaClient;
import com.mezzow.rest.client.RegistroPosConsultaClient;

import io.micronaut.core.annotation.Introspected;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

@Introspected
@Singleton
public class RegistroPosConsultaService {

	@Inject
	private RegistroPosConsultaClient client;

	@Inject
	private ModeloPosConsultaClient modeloPosConsultaClient;

	public Single<Optional<RegistroPosConsulta>> show(Long id) {

		return Single.fromObservable(Observable.just(client.show(id)))
				.subscribeOn(Schedulers.io())
				.flatMap(registroPosConsulta -> {
					if(registroPosConsulta.isPresent()) {
						if (registroPosConsulta.get().getModeloPosConsulta() != null) {
							registroPosConsulta.get().setModeloPosConsulta(modeloPosConsultaClient.show(registroPosConsulta.get().getModeloPosConsulta().getId()).get());
						}
					}
					return Single.just(Optional.of(registroPosConsulta.get()));
				});
	}
}
