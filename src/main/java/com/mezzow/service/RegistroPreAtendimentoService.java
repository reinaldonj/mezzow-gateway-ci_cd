package com.mezzow.service;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.commons.lang3.StringUtils;

import com.mezzow.domain.Agendamento;
import com.mezzow.domain.Atendimento;
import com.mezzow.domain.Paciente;
import com.mezzow.domain.Perfil;
import com.mezzow.domain.RegistroPreAtendimento;
import com.mezzow.domain.StatusAgendamento;
import com.mezzow.domain.StatusAtendimento;
import com.mezzow.domain.Unidade;
import com.mezzow.domain.enums.StatusAgendamentoEnum;
import com.mezzow.domain.enums.StatusAtendimentoEnum;
import com.mezzow.rest.client.AgendamentoClient;
import com.mezzow.rest.client.AtendimentoClient;
import com.mezzow.rest.client.PacienteClient;
import com.mezzow.rest.client.PerfilClient;
import com.mezzow.rest.client.RegistroPreAtendimentoClient;
import com.mezzow.rest.client.UnidadeClient;
import com.mezzow.util.Pagina;
import com.mezzow.util.TemplateModel;
import com.mezzow.util.TemplateToPdfConverterUtil;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.micronaut.http.HttpResponse;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class RegistroPreAtendimentoService {
	
	private static final String TEMPLATE_CSS_ATENDIMENTO_RELATORIO_CANCELAMENTO = 
			
			"table {\r\n"
			+ "  margin: 0;\r\n"
			+ "  padding: 0;\r\n"
			+ "  width: 100%;\r\n"
			+ "  table-layout: fixed;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "table caption {\r\n"
			+ "  font-size: 1.5em;\r\n"
			+ "  margin: .5em 0 .75em;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "table tr {\r\n"
			+ "  background-color: #f8f8f8;\r\n"
			+ "  border: 1px solid #ddd;\r\n"
			+ "  padding: .35em;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "table tr,\r\n"
			+ "table td {\r\n"
			+ "  padding: .625em;\r\n"
			+ "  text-align: center;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "table tr {\r\n"
			+ "  font-size: .85em;\r\n"
			+ "  letter-spacing: .1em;\r\n"
			+ "  text-transform: uppercase;\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "@media screen and (max-width: 600px) {\r\n"
			+ "  \r\n"
			+ "  #container { width: 400px; }\r\n"
			+ "  \r\n"
			+ "  table {\r\n"
			+ "    border: 0;\r\n"
			+ "  }\r\n"
			+ "\r\n"
			+ "  table caption {\r\n"
			+ "    font-size: 1.3em;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table tr {\r\n"
			+ "    border: none;\r\n"
			+ "    clip: rect(0 0 0 0);\r\n"
			+ "    height: 1px;\r\n"
			+ "    margin: -1px;\r\n"
			+ "    overflow: hidden;\r\n"
			+ "    padding: 0;\r\n"
			+ "    position: absolute;\r\n"
			+ "    width: 1px;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table tr {\r\n"
			+ "    border-bottom: 3px solid #ddd;\r\n"
			+ "    display: block;\r\n"
			+ "    margin-bottom: .625em;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table td {\r\n"
			+ "    border-bottom: 1px solid #ddd;\r\n"
			+ "    display: block;\r\n"
			+ "    font-size: .8em;\r\n"
			+ "    text-align: right;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table td::before {\r\n"
			+ "    content: attr(data-label);\r\n"
			+ "    float: left;\r\n"
			+ "    font-weight: bold;\r\n"
			+ "    text-transform: uppercase;\r\n"
			+ "  }\r\n"
			+ "  \r\n"
			+ "  table td:last-child {\r\n"
			+ "    border-bottom: 0;\r\n"
			+ "  }\r\n"
			+ "}\r\n"
			+ "\r\n"
			+ "body {\r\n"
			+ "  font-family: \"Open Sans\", sans-serif;\r\n"
			+ "  line-height: 1.25;\r\n"
			+ "}";
	
	private static final String TEMPLATE_BODY_ATENDIMENTO_RELATORIO_CANCELAMENTO = 
			"<div id=\"container\">\r\n"
			+"#if(${model.dominio.paginas.isEmpty()}) "
			+ "	<table >\r\n"
			+ "	  <caption style=\"text-align: left;\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHEAAABMCAIAAADV+M/0AAAS5klEQVR4nO2daZQcV3WAv/equns2zUgzoxnti6WRInnBWPYBwho7iZcYG+KACRCDY2xsjg/LOYDDGnEMGCMOwYGDFzAHJ2CFRTgxS7ATESCOlxhjQLJkWbs02jUazUg9S1e99/LjVlVX9/TM9IzahB99f0it7le33nL3e9+Vcs5RIxBUSqmJxgBgrNEoQGud/nU0KAC7e/uAvb2HDx87CfQNnAbCMPR9H5jZ1gzMnz1r8YIuYOn8biCX9UveYixglPO1N/btE82vujETg558SB2mCKqGdJoG51wZwYZhCHieR4qWd+0/Avz7L34NbHxi02+27gYOHO0HCqfyGAsoWzJDpxVAxmtoagLmds8Czl+19JJXnAtc/toLgLMWdifTICZb7XvpLyfmpzOBF2tPE7DWAk7hKQ0EoQUe3vjU3d/+KfDLp54Dwr5BwHla5TKAy/gA2ldaFh+h0g7AALIvsuNBCLjRQBkL+B2twGtevgq45a1XXHXJy4CMrwHjrHIW0NoH3Jkx+ARQ5/3aw4tIp8YYQGthdjY88gTwibu+Azz/661KeYBrzgE64wPaKWsNYBWAMwplAZwGos8lcwfwYl0nT9kgBFR+FHDO/NEFq4Db33ctcM2lr5DFCuuIFHox4MXa09Aa0bm79h4Gbll736M/eRzwMj7gWhojBWssULZbygG4aXFmxHeejjDlhwFXCIE/v+KP7/nUjcDSRXNlhmJ16FrLgDrv1x5qSacxZwEoj/UPPwa8+7Z/BPInBjKz2pIxoR3DyBXQ6Qr8XumtkFJkybOAUr4uqvigf6ClYyZwz+duBd521WtC44hpuoZmQM321DknqIShbvvCA5+/80FAz2gEVC5jAll8pW0SWahdPCeYKu9PcgAO8HzfjQaAPTUMfPi2t975wXcARowBVK22tc77tYdq6XQCOzmyq2Mj/8aP3g3cf9+G7Ox2IHAh4KxKRiOIJiLGM/YPhfe1Jb045zxRSloDwbETN9z0JuDrd9wMWGvLHOVpw9R4v+JaA2eBjNLv/thXgfvu2QBkuzqDIGDsrlUUf6p0x5PPFZlR3CpPc2ZSK+v7wZE+4MZbrgHu/czNRhCrM93ZOu/XHvzJh6SgjGxEb2Y8Ddz+5e/ed89DQLarAyiEQTy6lLiF9MposCLFFQe4+A9xHyyRwJlMRCiVxqzij8I6hTDMdncAMu0Fczo/fus1yaJ8b/qSZ/p63xgjrsi/bXwaeMM71vrNjUAoCMdFOxVZWVFQ1BCUAnylADM08tA3/x64+pKLSK1uGlDn/drDdOg0sQGOHB8ALrzivUDv4ROqMQc4CRyVWYvjKZxSSiwxBsrG28T9nwrxlvsCFRfrARRG589tB5750V1Ad2fbtEOCU5OnAnEMQn1i3T8BvXsOA15HqwkNRGyd8TxDhTmJXW8V1kpk0xR/U0r7GsCGgCnYaI+VApTv+coDApvsbIkYicb6GhBLzYSOwCS/Ke3hFZMLxth4iw3gNeUO7DwMyKLu/dyt8p5pCIA679cepsb7QqFiGj/57PZXvPFDANkMjGGrwSEht4izYpAzdM7RmAVcY46YcOzoKPk8oBqagKZZLVk/AwyPFICRgVMUQsCb0QRY33OpoIHney40gDs1TCydvBnNTS1NxEr89MhoMJgHGBkBvOZmm/MBF+cRJEBlgwB44qF1L3/pytSSp0B80+F92Zl19/5AjQSAasgBNuZiUaPvvOHqWTNbARcGgCHysmS1TQ25pzftAB792dOAHQmAuUvmvvNNbwaueN0aYNmiOZmsDwwPjQB7Dx7/3iOPA/d/66dAfmiEhhyglQLsQN7LZYHL3vBq4C2XvQo4/5yzOttbgYxSwOmh4X2H+oBHH/stcO+3fnL0cB+gmhsBZ63zNKBPh8C6e7///Xs+BsSRyCnsaZ33aw/V8n6aBX6zdS9w4ZXvNzqlf5SiEADdczqAHRu/2iLnX6o9AxMCGc//2J3/DHz2s/cDH/zo3wIfv+Wv2lqbxj4STQCnUMCmbXuBy6/7xMFjAwD5YeCVr13zlU/dBJy/amkphhIqS2M+1j949Q2fBp743+cA1dwYKUylAM/yqx/9A/CSVYship0rryoSrIr3nXMSjRCUDz70M8AMDnmdrYCoe1/pMDTAsoXdQEtzYyEwgI7cFxs9LUfosWV3L/Cd9Z8G3nzFq4DQmtAaYg2eiDAXpVKseDjnrVwM3L/u/Zdf/UHgAx/6G+CLH71eBkt2Fq0AHccaxAxwChHBxgLMntX6wBffD5x/5QeAoeFR0fGepwHTNyjLPH/V9UCoHOBjraRqirGZCrtc5/3aQ1W8n8TxJJN8zqW3Aju277cNWfkZyPp+4fgAcMO7Xg98/Y5bhejKKkEERgvh/kNHgOWL5wMy0lO6egM7PzTy8Mangbdc+SqEBk0xc1eNuR6EVuyBa9+3Dvjev/xHpn0mEDoLMFzo6VkIbH7kK8TZbIuLLeaJMFfL+4Ll15t3AC/s7AVoyMZ5EgUYjMYC5/UsmRRhLuuftXge8SEpHGCVFZ4dE22roHmbGnN//fpXE0fpnXVi+xvJm3pi+ZdHcdJ7rbRTSgMXntsDfG/9xjhf62R1skxZ8svOXyH7oKuIBNZ5v/YwNfv0f57ZAqihAqAbc0aOXdxNq1wuC6zqWSCD1YRCRVg1E9XbxOpI/kxyAePbhkopqR8QovNiCeMqEYlKPTX21+6OdgCt0nUunqdNfhR44pktpOi0moDa1Pb0mU07iRWxdnGZjeQdjcm2NAE9S+aNXYDFpXQlFiWCb/P2/cD6h38JnDw19Ja/eCXw6otWxwuYiI2Sagzgx7945tHHfkMsN95+9euAC85eLMKpshfkSgyscuQOozXw9KadyZcTU0nx2apG1WEqMAmdlp3h9j2HATIeSew5pkcXBAsWdAPz5nSkvxfQODm/2M5V//n474DLr1sLhAODANY9sP4RYPPP7gYWz589ga9tnBWS/MBnvgF86YvrMxkPCIZHgB/88L+Bzf91b0tDBTMgWpQzsvy+gUFAOWyKr0PnyOhkyek0e3pzKgqTqnhfay2BjIPHTwBEQrB0TwuhGEbZjEeFNGSUvE8KaW67/RtAmB8GGrs6BV2+bxDYsm0PsGT+bFuJK6NIo9bizt113wbA62h1vg9oOwM40DcAHDp4tOes+YBztjjR5O/YCew9dJwx1KOdM75KljwyGgCNDdkSy2Eci6rO+7WHanVUPj8MnM6PAqJIk3ONjiu0q1fMT8bbMRrYOkusQx5/Zuuzz+0EVGszMFwoAJ5SOucDM2Y0R8/oCoRgbQhonf3xxicBN2QAmnQYBhA5no1NTUBzS0OMxzEmBeYpLZbvtl29AL5Ok6oBlJ8sOZ/PA43i40wGk8lThcxmeLQAjBZCmU56TGSCaHXuikUToRI329fAz598zg0XAK+5ARDWNM55uQzQ1dEqj6hK+UDlyZztU7/dAeBHIUQx4AkCYE5nG9DV3hbj0Wk8iakmAYrtuw8DKpMpNwA0xFcMhkclFoxRULJrFay9Ou/XHqYYky7P3GmSBFFDZvXyhckvWpcnndMS/XfP74n40aYSdoWga95sYMGczrGPkETvlWSTzAt7DwFKClpjFSw1vWIj+75f2XKIsR48Mgj0HjkBuIyHncT+dM6NkUb1uNTvBSajUzk6rRpzWSCXyQIj+QKAF4vSMARaZ7UuXTgneU6PqUJJlyBs3XWQTIbEERR6HA2WL5oDNDc1IFqujE5FuFsHHDo6sP9gH0TGsnNoIaHAAGcvj/xjMYdVqX0qaR7P83bs3Y9kukA1N7gyOjUOyGV8oCmbjTBEwniiPZtkT5N5SNB+RksDMNA/CHh48msYGGDR/M7uzrbUoxEHyPMJbx7vGwD2HTgqexqZe8KboV29fEHypXVWl90Yc1GwGti1/8jQwGlANWUBrJMItPE0cE6ptiyXIfG/xDNmNAT8Fi+UEkQ5OYXDJQtvammceKPSUOf92kNVdGqtbWjIAnM72oDe3YcAm8tGx18oACuXzp3gVofFyQWRnfuPACf7B8kVM9gmdsnOXrEoeSljFIZL8d3WHfvUaAD4zQ1AgAmsAWjIAKt6FjK+75jAFqHTBH3JWEVogHmd7SSWqXWqkslcBtXGpOWD6NOnn3gO8JWKHGRrgXN7lsTZ5grK2jgnRu1WiWePFDKNOSAwpog/55/TU7QcxgSBStyIzdv3xnExGR1tQVt7G7B0UTeglNKVNHliBmzZsQ9QmWJMIAEvlmnLF3cnXxpnvVS5wnhnVuf92kN1dBofxppzlwEPPvgoYFWsYbwMcO6KJTKm7OjKGPm5bb0ANi5GF9IOLdDYNmPZoqLlMCbxq3VkHStg6/ZePJ9YbiilXBACi+Z2ALMTDyo1GVcaSMsPjezYdxRwWcFTumStxOa58LzlyZdGlRTVjCdY/GrSYcmvr1lzNkBTjnQNV3MO6FkyJx5cYuwLfj/+56YX9oA41zLYAgQhsGDp3HlzO4ovdSUmi3FWqvHFP961/whZv4hf6aAQAquXLUzebYzxIjc6Sk9FLoBSwP6DfUeP9QNkfEDZEkvKGEtzBnjlmtXJl151Kcg679ce/GrSv0kI5yXnLANWLl8IbNuxD+UBXV0zgYXzZqcfKQ9Hai1XJnbEDqXENJXyARuMAMsXz5HEdXxRVbuolrXk4A8cOg4cONovt1SFuqyOKixXp7ScdUpHai2q24hyoloDL+w+YE4PA7qtGbAmcrsjzT5c6OlZBFxwzvLk+TG3KG1FoqzW3y9YSeJ7wFUXXwh8YfMulwNYNr+LuP0DY5zryJOBg0dOAAfEufY9Ip4FsGEAnLdyiTySEkcp8WVCuXG3c99hoHAqL7VjYnU45/AzwNkri9a+VlFKTkV74TkVJr9u3r6fMGCM3IuqU4cLUoQumf2yWoV4hpW5vM77tYdq6dRPkf1b33gxcNc3f1wYOA2sWBY516fzw4y19q0Fmpsatuw4CIwMniblXEfWvtbAiqVRwlUSFb6JpIcQRWBNa1MG+O3zewBGAr+lCQjEKzFWyHbJ/KI5OTIa4BmKly4xxgHNvg9sen4P2k9+Nc4JW0j5l9fa9PY3XCwrIOb6RGtOotKnVdOrgWtuvuMHG34OtC/ukl2LLo4XOaIYGNRay473n8zLnNKegcQ4ZnfOzGYygJUo+5hEgbz35EAeOHV6WKR5En4UOTi3sy2ZobXJ4pPIsU125HjfqdFC6lacc9r3Ads/APzlG/9kwz0fKVvypI4Z8WvqUGOY4t3IlDH75LPbpPZcjHMXGsbx0+VJyZpMcOdAWSv147iYAMs7TDiIs7ZlYebkXnQQRq+T4ZF7ropfxnhUxncqjkGRuhlT+P3VngvXaCAMC8DLX7ryxmv/DPjaN34EZLpmBlL/FVeplHTeSByVikcot4A9T2RxfPM8VrLRX1HVX2U88QGoXBbQVRCKSZBI+sD3TN9J4Mbrr5LVmTjMOimqMqjzfu2hBnfO1lz5PuDAoROqIQu4anpIkOK41Of4zlmFfKlyk11MFyRiulcRkStiFv0zUpA7Z7/64ZeAObNbJcA2jTtn1d/fh3jtAuX3Ta/7lD8jC4Q22axKqeTim8tkXDVzBcbvPzEW4cTNJ+LFyK2UID/8rw+sBa665CLA1u+b/kHBGfVDSV/Lvv3L3/3k2q8B2a5ZQCGwUj06SVuTMuKaOvGOi6oKhFnfB4Ij/cDate/65Huv5f/3rjlJMo4o7R71mbj7+0C2u7MgxTbxi6b9luncOC8+Mq78yfp+4WgfcNPN0mfiPSZVfXQmUOf92kP1dDpuGXjSY0YM43d95KvA/fdtyMxuB6IEr/FK1YUu6X42HoeWWgVn3hxB/FdfJT1mrgG+fsd7AGutWN9n3oGhlv2lEiEAfHjdN9d9fj3p/lJi5dio8KEmr4QJ9yBhfQAyvjKjhqS/1IfffueH3kZUGlHvL/WHDdOn07FBmigo5wCyWn374V8CN//dV4DTx/vlRlfcWy5O6Ud8XerX164Niq+LF6WCk/0t7e2M01sueeOZv7X2fSUl6GBxFftKaimVaGpg/L6S8dTUNIyqaH/G6St599qbgLMWzwFCazxVcjWtVlDn/dpD1b6pjJ4K6iRVByilSnvKbouKVkp6ykY0booNpSaeuyLOWI3TU9atXLMS+PR7/4B7yk5jZxO/QEdxwgr9pINjg4DKetIGxGWjCGl0R0/JnVQvksVJ6YOkTCVWPxqIGMlIP+mXnQ3c8rbLrvrTi4CMVFc4W3aLvVZrLIM679ceXvS+5wlYW4zCxYG5sl7yv3t2y27g8NGTwEh+WKqFhR4TM0PqXGwm09BcoZf8pa+9AFi+sDv96mlHl6cHL+KepjGXXfse9/+RKITA3gNHgN29Rw8c6ydO6gVhmPF9oL21GZjbNWvpgi5gcaX/RyKxPeRub4qRp9wwZhpQ5/3aw/8BEIjodALq754AAAAASUVORK5CYII=\" id=\"p1img1\"></img></caption>\r\n"
			+ "	  <thead>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\">TELECONSULTA CANCELADAS</th>\r\n"
			+ "		  <th scope=\"col\" colspan=\"3\">De $dateTool.format('dd/MM/yyyy',${model.dominio.dataDe}) até $dateTool.format('dd/MM/yyyy',${model.dominio.dataAte})</th>\r\n"
			+ "		  <th scope=\"col\">PAGINA 1</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\">PACIENTE</th>\r\n"
			+ "		  <th scope=\"col\">CONSULTA</th>\r\n"
			+ "		  <th scope=\"col\">CONSULTA ANTERIOR</th>\r\n"
			+ "		  <th scope=\"col\" colspan=\"2\">MOTIVO</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  </thead>\r\n"
			+ "	  <tbody>\r\n"
			+ "	  </tbody>\r\n"
			+ "   <tfoot>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\" colspan=\"5\" style=\"text-align: left;\" >TOTAL:</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\">PACIENTE: 0</th>\r\n"
			+ "		 <th scope=\"col\">OPERAÇÃO: 0</th>\r\n"
			+ "		  <th scope=\"col\">CONEXÃO: 0</th>\r\n"
			+ "		  <th scope=\"col\" >MEZZOW: 0</th>\r\n"
			+ "		  <th scope=\"col\" >TOTAL: 0</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  </tfoot>"
			+ "	</table>\r\n"
			+"#else"
			+"#foreach($pagina in ${model.dominio.paginas}) "
			+ "	<table >\r\n"
			+ "	  <caption style=\"text-align: left;\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHEAAABMCAIAAADV+M/0AAAS5klEQVR4nO2daZQcV3WAv/equns2zUgzoxnti6WRInnBWPYBwho7iZcYG+KACRCDY2xsjg/LOYDDGnEMGCMOwYGDFzAHJ2CFRTgxS7ATESCOlxhjQLJkWbs02jUazUg9S1e99/LjVlVX9/TM9IzahB99f0it7le33nL3e9+Vcs5RIxBUSqmJxgBgrNEoQGud/nU0KAC7e/uAvb2HDx87CfQNnAbCMPR9H5jZ1gzMnz1r8YIuYOn8biCX9UveYixglPO1N/btE82vujETg558SB2mCKqGdJoG51wZwYZhCHieR4qWd+0/Avz7L34NbHxi02+27gYOHO0HCqfyGAsoWzJDpxVAxmtoagLmds8Czl+19JJXnAtc/toLgLMWdifTICZb7XvpLyfmpzOBF2tPE7DWAk7hKQ0EoQUe3vjU3d/+KfDLp54Dwr5BwHla5TKAy/gA2ldaFh+h0g7AALIvsuNBCLjRQBkL+B2twGtevgq45a1XXHXJy4CMrwHjrHIW0NoH3Jkx+ARQ5/3aw4tIp8YYQGthdjY88gTwibu+Azz/661KeYBrzgE64wPaKWsNYBWAMwplAZwGos8lcwfwYl0nT9kgBFR+FHDO/NEFq4Db33ctcM2lr5DFCuuIFHox4MXa09Aa0bm79h4Gbll736M/eRzwMj7gWhojBWssULZbygG4aXFmxHeejjDlhwFXCIE/v+KP7/nUjcDSRXNlhmJ16FrLgDrv1x5qSacxZwEoj/UPPwa8+7Z/BPInBjKz2pIxoR3DyBXQ6Qr8XumtkFJkybOAUr4uqvigf6ClYyZwz+duBd521WtC44hpuoZmQM321DknqIShbvvCA5+/80FAz2gEVC5jAll8pW0SWahdPCeYKu9PcgAO8HzfjQaAPTUMfPi2t975wXcARowBVK22tc77tYdq6XQCOzmyq2Mj/8aP3g3cf9+G7Ox2IHAh4KxKRiOIJiLGM/YPhfe1Jb045zxRSloDwbETN9z0JuDrd9wMWGvLHOVpw9R4v+JaA2eBjNLv/thXgfvu2QBkuzqDIGDsrlUUf6p0x5PPFZlR3CpPc2ZSK+v7wZE+4MZbrgHu/czNRhCrM93ZOu/XHvzJh6SgjGxEb2Y8Ddz+5e/ed89DQLarAyiEQTy6lLiF9MposCLFFQe4+A9xHyyRwJlMRCiVxqzij8I6hTDMdncAMu0Fczo/fus1yaJ8b/qSZ/p63xgjrsi/bXwaeMM71vrNjUAoCMdFOxVZWVFQ1BCUAnylADM08tA3/x64+pKLSK1uGlDn/drDdOg0sQGOHB8ALrzivUDv4ROqMQc4CRyVWYvjKZxSSiwxBsrG28T9nwrxlvsCFRfrARRG589tB5750V1Ad2fbtEOCU5OnAnEMQn1i3T8BvXsOA15HqwkNRGyd8TxDhTmJXW8V1kpk0xR/U0r7GsCGgCnYaI+VApTv+coDApvsbIkYicb6GhBLzYSOwCS/Ke3hFZMLxth4iw3gNeUO7DwMyKLu/dyt8p5pCIA679cepsb7QqFiGj/57PZXvPFDANkMjGGrwSEht4izYpAzdM7RmAVcY46YcOzoKPk8oBqagKZZLVk/AwyPFICRgVMUQsCb0QRY33OpoIHney40gDs1TCydvBnNTS1NxEr89MhoMJgHGBkBvOZmm/MBF+cRJEBlgwB44qF1L3/pytSSp0B80+F92Zl19/5AjQSAasgBNuZiUaPvvOHqWTNbARcGgCHysmS1TQ25pzftAB792dOAHQmAuUvmvvNNbwaueN0aYNmiOZmsDwwPjQB7Dx7/3iOPA/d/66dAfmiEhhyglQLsQN7LZYHL3vBq4C2XvQo4/5yzOttbgYxSwOmh4X2H+oBHH/stcO+3fnL0cB+gmhsBZ63zNKBPh8C6e7///Xs+BsSRyCnsaZ33aw/V8n6aBX6zdS9w4ZXvNzqlf5SiEADdczqAHRu/2iLnX6o9AxMCGc//2J3/DHz2s/cDH/zo3wIfv+Wv2lqbxj4STQCnUMCmbXuBy6/7xMFjAwD5YeCVr13zlU/dBJy/amkphhIqS2M+1j949Q2fBp743+cA1dwYKUylAM/yqx/9A/CSVYship0rryoSrIr3nXMSjRCUDz70M8AMDnmdrYCoe1/pMDTAsoXdQEtzYyEwgI7cFxs9LUfosWV3L/Cd9Z8G3nzFq4DQmtAaYg2eiDAXpVKseDjnrVwM3L/u/Zdf/UHgAx/6G+CLH71eBkt2Fq0AHccaxAxwChHBxgLMntX6wBffD5x/5QeAoeFR0fGepwHTNyjLPH/V9UCoHOBjraRqirGZCrtc5/3aQ1W8n8TxJJN8zqW3Aju277cNWfkZyPp+4fgAcMO7Xg98/Y5bhejKKkEERgvh/kNHgOWL5wMy0lO6egM7PzTy8Mangbdc+SqEBk0xc1eNuR6EVuyBa9+3Dvjev/xHpn0mEDoLMFzo6VkIbH7kK8TZbIuLLeaJMFfL+4Ll15t3AC/s7AVoyMZ5EgUYjMYC5/UsmRRhLuuftXge8SEpHGCVFZ4dE22roHmbGnN//fpXE0fpnXVi+xvJm3pi+ZdHcdJ7rbRTSgMXntsDfG/9xjhf62R1skxZ8svOXyH7oKuIBNZ5v/YwNfv0f57ZAqihAqAbc0aOXdxNq1wuC6zqWSCD1YRCRVg1E9XbxOpI/kxyAePbhkopqR8QovNiCeMqEYlKPTX21+6OdgCt0nUunqdNfhR44pktpOi0moDa1Pb0mU07iRWxdnGZjeQdjcm2NAE9S+aNXYDFpXQlFiWCb/P2/cD6h38JnDw19Ja/eCXw6otWxwuYiI2Sagzgx7945tHHfkMsN95+9euAC85eLMKpshfkSgyscuQOozXw9KadyZcTU0nx2apG1WEqMAmdlp3h9j2HATIeSew5pkcXBAsWdAPz5nSkvxfQODm/2M5V//n474DLr1sLhAODANY9sP4RYPPP7gYWz589ga9tnBWS/MBnvgF86YvrMxkPCIZHgB/88L+Bzf91b0tDBTMgWpQzsvy+gUFAOWyKr0PnyOhkyek0e3pzKgqTqnhfay2BjIPHTwBEQrB0TwuhGEbZjEeFNGSUvE8KaW67/RtAmB8GGrs6BV2+bxDYsm0PsGT+bFuJK6NIo9bizt113wbA62h1vg9oOwM40DcAHDp4tOes+YBztjjR5O/YCew9dJwx1KOdM75KljwyGgCNDdkSy2Eci6rO+7WHanVUPj8MnM6PAqJIk3ONjiu0q1fMT8bbMRrYOkusQx5/Zuuzz+0EVGszMFwoAJ5SOucDM2Y0R8/oCoRgbQhonf3xxicBN2QAmnQYBhA5no1NTUBzS0OMxzEmBeYpLZbvtl29AL5Ok6oBlJ8sOZ/PA43i40wGk8lThcxmeLQAjBZCmU56TGSCaHXuikUToRI329fAz598zg0XAK+5ARDWNM55uQzQ1dEqj6hK+UDlyZztU7/dAeBHIUQx4AkCYE5nG9DV3hbj0Wk8iakmAYrtuw8DKpMpNwA0xFcMhkclFoxRULJrFay9Ou/XHqYYky7P3GmSBFFDZvXyhckvWpcnndMS/XfP74n40aYSdoWga95sYMGczrGPkETvlWSTzAt7DwFKClpjFSw1vWIj+75f2XKIsR48Mgj0HjkBuIyHncT+dM6NkUb1uNTvBSajUzk6rRpzWSCXyQIj+QKAF4vSMARaZ7UuXTgneU6PqUJJlyBs3XWQTIbEERR6HA2WL5oDNDc1IFqujE5FuFsHHDo6sP9gH0TGsnNoIaHAAGcvj/xjMYdVqX0qaR7P83bs3Y9kukA1N7gyOjUOyGV8oCmbjTBEwniiPZtkT5N5SNB+RksDMNA/CHh48msYGGDR/M7uzrbUoxEHyPMJbx7vGwD2HTgqexqZe8KboV29fEHypXVWl90Yc1GwGti1/8jQwGlANWUBrJMItPE0cE6ptiyXIfG/xDNmNAT8Fi+UEkQ5OYXDJQtvammceKPSUOf92kNVdGqtbWjIAnM72oDe3YcAm8tGx18oACuXzp3gVofFyQWRnfuPACf7B8kVM9gmdsnOXrEoeSljFIZL8d3WHfvUaAD4zQ1AgAmsAWjIAKt6FjK+75jAFqHTBH3JWEVogHmd7SSWqXWqkslcBtXGpOWD6NOnn3gO8JWKHGRrgXN7lsTZ5grK2jgnRu1WiWePFDKNOSAwpog/55/TU7QcxgSBStyIzdv3xnExGR1tQVt7G7B0UTeglNKVNHliBmzZsQ9QmWJMIAEvlmnLF3cnXxpnvVS5wnhnVuf92kN1dBofxppzlwEPPvgoYFWsYbwMcO6KJTKm7OjKGPm5bb0ANi5GF9IOLdDYNmPZoqLlMCbxq3VkHStg6/ZePJ9YbiilXBACi+Z2ALMTDyo1GVcaSMsPjezYdxRwWcFTumStxOa58LzlyZdGlRTVjCdY/GrSYcmvr1lzNkBTjnQNV3MO6FkyJx5cYuwLfj/+56YX9oA41zLYAgQhsGDp3HlzO4ovdSUmi3FWqvHFP961/whZv4hf6aAQAquXLUzebYzxIjc6Sk9FLoBSwP6DfUeP9QNkfEDZEkvKGEtzBnjlmtXJl151Kcg679ce/GrSv0kI5yXnLANWLl8IbNuxD+UBXV0zgYXzZqcfKQ9Hai1XJnbEDqXENJXyARuMAMsXz5HEdXxRVbuolrXk4A8cOg4cONovt1SFuqyOKixXp7ScdUpHai2q24hyoloDL+w+YE4PA7qtGbAmcrsjzT5c6OlZBFxwzvLk+TG3KG1FoqzW3y9YSeJ7wFUXXwh8YfMulwNYNr+LuP0DY5zryJOBg0dOAAfEufY9Ip4FsGEAnLdyiTySEkcp8WVCuXG3c99hoHAqL7VjYnU45/AzwNkri9a+VlFKTkV74TkVJr9u3r6fMGCM3IuqU4cLUoQumf2yWoV4hpW5vM77tYdq6dRPkf1b33gxcNc3f1wYOA2sWBY516fzw4y19q0Fmpsatuw4CIwMniblXEfWvtbAiqVRwlUSFb6JpIcQRWBNa1MG+O3zewBGAr+lCQjEKzFWyHbJ/KI5OTIa4BmKly4xxgHNvg9sen4P2k9+Nc4JW0j5l9fa9PY3XCwrIOb6RGtOotKnVdOrgWtuvuMHG34OtC/ukl2LLo4XOaIYGNRay473n8zLnNKegcQ4ZnfOzGYygJUo+5hEgbz35EAeOHV6WKR5En4UOTi3sy2ZobXJ4pPIsU125HjfqdFC6lacc9r3Ads/APzlG/9kwz0fKVvypI4Z8WvqUGOY4t3IlDH75LPbpPZcjHMXGsbx0+VJyZpMcOdAWSv147iYAMs7TDiIs7ZlYebkXnQQRq+T4ZF7ropfxnhUxncqjkGRuhlT+P3VngvXaCAMC8DLX7ryxmv/DPjaN34EZLpmBlL/FVeplHTeSByVikcot4A9T2RxfPM8VrLRX1HVX2U88QGoXBbQVRCKSZBI+sD3TN9J4Mbrr5LVmTjMOimqMqjzfu2hBnfO1lz5PuDAoROqIQu4anpIkOK41Of4zlmFfKlyk11MFyRiulcRkStiFv0zUpA7Z7/64ZeAObNbJcA2jTtn1d/fh3jtAuX3Ta/7lD8jC4Q22axKqeTim8tkXDVzBcbvPzEW4cTNJ+LFyK2UID/8rw+sBa665CLA1u+b/kHBGfVDSV/Lvv3L3/3k2q8B2a5ZQCGwUj06SVuTMuKaOvGOi6oKhFnfB4Ij/cDate/65Huv5f/3rjlJMo4o7R71mbj7+0C2u7MgxTbxi6b9luncOC8+Mq78yfp+4WgfcNPN0mfiPSZVfXQmUOf92kP1dDpuGXjSY0YM43d95KvA/fdtyMxuB6IEr/FK1YUu6X42HoeWWgVn3hxB/FdfJT1mrgG+fsd7AGutWN9n3oGhlv2lEiEAfHjdN9d9fj3p/lJi5dio8KEmr4QJ9yBhfQAyvjKjhqS/1IfffueH3kZUGlHvL/WHDdOn07FBmigo5wCyWn374V8CN//dV4DTx/vlRlfcWy5O6Ud8XerX164Niq+LF6WCk/0t7e2M01sueeOZv7X2fSUl6GBxFftKaimVaGpg/L6S8dTUNIyqaH/G6St599qbgLMWzwFCazxVcjWtVlDn/dpD1b6pjJ4K6iRVByilSnvKbouKVkp6ykY0booNpSaeuyLOWI3TU9atXLMS+PR7/4B7yk5jZxO/QEdxwgr9pINjg4DKetIGxGWjCGl0R0/JnVQvksVJ6YOkTCVWPxqIGMlIP+mXnQ3c8rbLrvrTi4CMVFc4W3aLvVZrLIM679ceXvS+5wlYW4zCxYG5sl7yv3t2y27g8NGTwEh+WKqFhR4TM0PqXGwm09BcoZf8pa+9AFi+sDv96mlHl6cHL+KepjGXXfse9/+RKITA3gNHgN29Rw8c6ydO6gVhmPF9oL21GZjbNWvpgi5gcaX/RyKxPeRub4qRp9wwZhpQ5/3aw/8BEIjodALq754AAAAASUVORK5CYII=\" id=\"p1img1\"></img></caption>\r\n"
			+ "	  <thead>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\">TELECONSULTA CANCELADAS</th>\r\n"
			+ "		  <th scope=\"col\" colspan=\"3\">De $dateTool.format('dd/MM/yyyy',${model.dominio.dataDe}) até $dateTool.format('dd/MM/yyyy',${model.dominio.dataAte})</th>\r\n"
			+ "		  <th scope=\"col\">PAGINA ${pagina.numeroPagina}</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "		<tr>\r\n"
			+ "		  <th scope=\"col\">PACIENTE</th>\r\n"
			+ "		  <th scope=\"col\">CONSULTA</th>\r\n"
			+ "		  <th scope=\"col\">CONSULTA ANTERIOR</th>\r\n"
			+ "		  <th scope=\"col\" colspan=\"2\">MOTIVO</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  </thead>\r\n"
			+ "	  <tbody>\r\n"
			+ "		#foreach($dominio in ${pagina.dominios})"
			+ "		<tr>\r\n"
			+ "		#if($foreach.count % 2 >= 1) "
			+ "		  <td data-label=\"Account\"  style=\"background-color: #E9F4FB;\">${dominio.agendamento.paciente.nome.toUpperCase()}</td>\r\n"
			+ "		  <td data-label=\"Due Date\" style=\"background-color: #E9F4FB;\">$dateTool.format('dd/MM/yyyy HH:mm',${dominio.agendamento.dataConsulta})</td>\r\n"
			+ "		  <td data-label=\"Amount\"   style=\"background-color: #E9F4FB;\">&#160;</td>\r\n"
			+ "		  <td data-label=\"Period\"   style=\"background-color: #E9F4FB;\" colspan=\"2\">${dominio.motivoCancelamento.descricao.toUpperCase()}</td>\r\n"
			//+ "		  <td data-label=\"Period\"   style=\"background-color: #E9F4FB;\">n/a</td>\r\n"
			+ "		#else "
			+ "		  <td data-label=\"Account\">${dominio.agendamento.paciente.nome.toUpperCase()}</td>\r\n"
			+ "		  <td data-label=\"Due Date\">$dateTool.format('dd/MM/yyyy HH:mm',${dominio.agendamento.dataConsulta})</td>\r\n"
			+ "		  <td data-label=\"Amount\">&#160;</td>\r\n"
			+ "		  <td data-label=\"Period\" colspan=\"2\">${dominio.motivoCancelamento.descricao.toUpperCase()}</td>\r\n"
//			+ "		  <td data-label=\"Period\">n/a</td>\r\n"
			+ "		#end "
			+ "		</tr>\r\n"
			+ "		#end "
			+ "	  </tbody>\r\n"
			+ "   <tfoot>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" colspan=\"2\">&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\" >&#160;</th>\r\n"
			+ "		  <th scope=\"col\" style=\"background-color: white;\">&#160;</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\" colspan=\"5\" style=\"text-align: left;\" >TOTAL:</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  	<tr>\r\n"
			+ "		  <th scope=\"col\">PACIENTE: ${pagina.dominios.size()}</th>\r\n"
			+ "		 <th scope=\"col\">OPERAÇÃO: 1</th>\r\n"
			+ "		  <th scope=\"col\">CONEXÃO: ${model.dominio.totalConexao}</th>\r\n"
			+ "		  <th scope=\"col\" >MEZZOW: ${model.dominio.totalMezzow}</th>\r\n"
			+ "		  <th scope=\"col\" >TOTAL: ${pagina.dominios.size()}</th>\r\n"
			+ "		</tr>\r\n"
			+ "	  </tfoot>"
			+ "	</table>\r\n"
			+ "		#end "
			+ "	\r\n"
			+"#end"
			+ "	</div>";
	
	@Inject
	RegistroPreAtendimentoClient client;
	
	@Inject
	PerfilClient perfilClient;
	
	@Inject
	AgendamentoService agendamentoService;
	
	IntegracaoTeleConsultaService integracaoTeleConsultaService;
	
	@Inject
	AtendimentoClient atendimentoClient;
	
	@Inject
	private PacienteClient pacienteClient;
	
	@Inject
	private UnidadeClient unidadeClient;
	
//	private final AtendimentoClient atendimentoclient;
	
//	private final RxHttpClient rxClient;
	
//	private final Gson gson = new Gson();
	
//	private static final String TELECONSULTA_USERNAME = "opty";
//	private static final String TELECONSULTA_PASSWORD = "opty123456";
	
	public RegistroPreAtendimentoService(IntegracaoTeleConsultaService integracaoTeleConsultaService) {
		this.integracaoTeleConsultaService = integracaoTeleConsultaService;
	}
	
	public RegistroPreAtendimento save(RegistroPreAtendimento registroPreAtendimento) {
		
		return 
		Flowable
		.just(client.save(registroPreAtendimento))
		.flatMap(r -> {
			
			// ALTERA O STATUS DO AGENDAMENTO
			changeStatusAgendamento(r);
			
			StatusAgendamentoEnum byDEscricao = StatusAgendamentoEnum.getByDescricao(registroPreAtendimento.getStatus().name());
			System.out.println("************************************************************************************************************************************************");
			System.out.println(StatusAgendamentoEnum.CONFIRMADO.equals(byDEscricao));
			
			if (StatusAgendamentoEnum.CONFIRMADO.equals(byDEscricao)) {
				
				Atendimento atendimentoSaved = generatePersistAtendimento(r);
				Atendimento atendimentoFull = atendimentoClient.show(atendimentoSaved.getId()).get();

				atendimentoFull.setRegistroPreAtendimento( this.show(atendimentoFull.getRegistroPreAtendimento().getId()).get() ) ;
				integracaoTeleConsultaService.generateTeleConsultaAndPersisteNoAtendimento(atendimentoFull);
			}
			
			return Flowable.just(r);
			
		}).blockingFirst();
		
	}
	
	public RegistroPreAtendimento update(RegistroPreAtendimento registroPreAtendimento) {
		
		RegistroPreAtendimento updated = client.update(registroPreAtendimento);
		
		// ALTERA O STATUS DO AGENDAMENTO
		changeStatusAgendamento(updated);
		
		StatusAgendamentoEnum byDEscricao = StatusAgendamentoEnum.getByDescricao(updated.getStatus().name());		
		if (StatusAgendamentoEnum.CONFIRMADO.equals(byDEscricao)) {
			
			//new Thread(() -> {
				 Atendimento atendimentoSaved 	= generatePersistAtendimento(updated);
					
				 Atendimento atendimentoFull = atendimentoClient.show(atendimentoSaved.getId()).get();
					
				 atendimentoFull.setRegistroPreAtendimento( this.show(atendimentoFull.getRegistroPreAtendimento().getId()).get() ) ;
				
				 integracaoTeleConsultaService.generateTeleConsultaAndPersisteNoAtendimento(atendimentoFull);
			//}).start();
			
		
		}
		
		return updated;
	}
	
	private void changeStatusAgendamento(RegistroPreAtendimento registroPreAtendimento) {
		
		Agendamento agendamento = agendamentoService.show(registroPreAtendimento.getAgendamento().getId()).get();
		StatusAgendamentoEnum byDEscricao = StatusAgendamentoEnum.getByDescricao(registroPreAtendimento.getStatus().name());
		StatusAgendamento status = new StatusAgendamento();
		
		status.setId(byDEscricao.getCodStatus());
		status.setDescricao(byDEscricao.getDescStatus());
		agendamento.setStatusAgendamento(status);
		
		agendamentoService.update(agendamento);
	}
	
	
	private Timestamp stringToTimeStamp(String dataHora) {
		
		if (StringUtils.isBlank(dataHora)) return null;
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS");
	    Date parsedDate;
		try {
			parsedDate = dateFormat.parse(dataHora);
			Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
			return timestamp;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private Atendimento generatePersistAtendimento(RegistroPreAtendimento savePreAtendimento) {
		
		Atendimento a = new Atendimento();
		StatusAtendimento sta = new StatusAtendimento();
		RegistroPreAtendimento r = new RegistroPreAtendimento();
		
		r.setId(savePreAtendimento.getId());
		a.setRegistroPreAtendimento(r);		
		sta.setId(StatusAtendimentoEnum.AGENDADO.getCodStatus());
		a.setStatusAtendimento(sta);
		a.setDataConfirmacao(Timestamp.valueOf(LocalDateTime.now()));
		
		return atendimentoClient.save(a);
	}
	
	public Optional<RegistroPreAtendimento> show(Long id) {
//
		Optional<RegistroPreAtendimento> show = Optional.ofNullable(null);
		
		try {
			show = client.show(id);
			if (show.isPresent()) {
				RegistroPreAtendimento registroPreAtendimento = show.get();
				
				if(registroPreAtendimento.getMedico() != null) {
					registroPreAtendimento.setMedico(findPerfil(registroPreAtendimento.getMedico().getId()));
				}
				
				if(registroPreAtendimento.getAgendamento() != null) {
					registroPreAtendimento.setAgendamento(findAgendamento(registroPreAtendimento.getAgendamento().getId()));
				}
				
				return Optional.of(registroPreAtendimento);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return show;
		
	}
		
	public Page<RegistroPreAtendimento> find(@Nullable Long agendamentoId, @Nullable String perfilId, @Nullable String observacao, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order) {
		
		Page<RegistroPreAtendimento> find = client.find(agendamentoId, perfilId, observacao, max, offset, sort, order);
		
		//  Retorna os Perfis ativos com base na lista de perfis extraída da lista de Registros de Pré Atendimento.
		List<String> listaId = find.getContent().stream().map(item -> item.getMedico() != null ? item.getMedico().getId() : " ").distinct().collect(Collectors.toList());
		List<Perfil> perfilLista = perfilClient.findByIds(listaId);
		
		
		List<Long> listaIdAgendamento = find.getContent().stream().map(item -> item.getAgendamento() != null ? item.getAgendamento().getId() : 0L).distinct().collect(Collectors.toList());
		
		List<Agendamento> agendamentoLista = find.getTotalSize() > 0 ?  agendamentoService.list(listaIdAgendamento) : Collections.emptyList();
		
		List<RegistroPreAtendimento> list = find.getContent().stream()
		.map(item -> {
			if(item.getMedico() != null) {
				Perfil perfil = findPerfil(item.getMedico().getId(), perfilLista);
				if(perfil != null) {
					item.setMedico(perfil);															
				} else {
					return null;
				}
			}
			return item;
		})
		.map(item -> {
			if(item.getAgendamento() != null) {
				Agendamento agendamento = findAgendamento(item.getAgendamento().getId(), agendamentoLista);
				if(agendamento != null) {
					item.setAgendamento(agendamento);															
				} else {
					return null;
				}
			}
			return item;
		})
		.collect(Collectors.toList());
		
		// RETORNA REMOVE TODOS OS ITENS QUE NÃO ATENDERAM OS CRITÉRIOS DE SEGURANÇA DO PERFIL.
		list.removeIf(p -> p == null);
		
		return Page.of(list, find.getPageable(), list.size());
	}

	private Perfil findPerfil(String perfilId, List<Perfil> perfilLista) {
		for(Perfil perfil : perfilLista) {
			if(perfil.getId().equals(perfilId)) {
				return perfil;
			}
		}
		return null;
//		return perfilLista.stream().filter(u -> u.getId().equals(perfilId)).collect(Collectors.toList()).get(0);
	}
	
	private Agendamento findAgendamento(Long agendamentoId, List<Agendamento> agendamentoLista) {
		for(Agendamento agendamento : agendamentoLista) {
			if(agendamento.getId() == (agendamentoId)) {
				return agendamento;
			}
		}
		return null;
	}

	private Perfil findPerfil(String perfilId) {
		return perfilClient.show(perfilId).get();
	}
	
	private Agendamento findAgendamento(Long agendamentoId) {
		return agendamentoService.show(agendamentoId).get();
	}
	
	public byte[] combinarFiltrosCancelamentoGerarStreamPDF(
			List<String> grupoEmpresaId, 
			List<String> empresaId, 
			List<Long> unidadeId,
			String dataDe, 
			String dataAte) {
		
			return
				Flowable.zip(
						this.findByGrupoEmpresaRx(grupoEmpresaId).subscribeOn(Schedulers.io()),
						this.findByEmpresaRx(empresaId).subscribeOn(Schedulers.io()),
						(uGrupoEmpresa1, uEmpresa) -> {
							
							List<Unidade> listauGrupoEmpresa1 	= uGrupoEmpresa1.getBody().orElse(Collections.emptyList());
							List<Unidade> listauEmpresa 		= uEmpresa.getBody().orElse(Collections.emptyList());
							List<Long> somarUnidades 			= somarUnidades(unidadeId, listauGrupoEmpresa1, listauEmpresa);
							
							return client.findAgendamentosCanceladosRxH(somarUnidades, getDateDe(dataDe), getDateAte(dataAte)).subscribeOn(Schedulers.io());

						})
						.flatMap(a -> a)
						.flatMap(a -> Flowable.just( a.getBody().orElse(Collections.emptyList())) )
						.map(fullRegistroPreAtendimentoCancelados -> {
							
							List<Long> idsPacientes = fullRegistroPreAtendimentoCancelados.stream().map(p -> p.getAgendamento().getPaciente().getId()).collect(Collectors.toList());
							
							return 
								callPacienteByIdsRxH(idsPacientes)
									.subscribeOn(Schedulers.io())
									.flatMap(a -> Flowable.just( a.getBody().orElse(Collections.emptyList())) )
									.flatMap(listaPacientes -> {
										
										final Map<Long, Paciente> listaPacientesIdMapRetornados = listaPacientes.stream().collect( Collectors.toMap(Paciente::getId, p -> p)  );
										
										fullRegistroPreAtendimentoCancelados.forEach(rpa -> {
											
											rpa.getAgendamento().setPaciente( listaPacientesIdMapRetornados.get(rpa.getAgendamento().getPaciente().getId()));
										});
										
										return Flowable.just( generatePdfFromRegistroPreAtendimentoCancelamento(fullRegistroPreAtendimentoCancelados, dataDe, dataAte) );
										
										
									}).blockingLast();
							
							
						}).blockingFirst();
						
				
				 
	}

	private Flowable<HttpResponse<List<Paciente>>> callPacienteByIdsRxH(List<Long> idsPacientes) {
		
		
		if (idsPacientes == null || idsPacientes.isEmpty()) {
			return Flowable.just(HttpResponse.ok(Collections.emptyList()));
		}
		
		return pacienteClient
			.byIdsRx(idsPacientes);
	}
	
	private String getDateDe(String dataDe) {
		LocalDateTime dataIni = null;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		if (dataDe != null) {
			dataDe += " 00:00";
			dataIni = LocalDateTime.parse(dataDe, formatter);
		}
		
		if (dataIni == null) {
			dataIni = LocalDateTime.now().minusMonths(2L);
		}
		return dataIni.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
	
	private String getDateAte(String dataAte) {
		
		LocalDateTime dataFim = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		
		if (dataAte != null) {
			dataAte += " 23:59"; 
			dataFim = LocalDateTime.parse(dataAte, formatter);
		}
		
		if (dataFim == null) {
			dataFim = LocalDateTime.now();
		}
		
		return dataFim.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
	
	private List<Long> somarUnidades(List<Long> unidadeId, List<Unidade> uGrupoEmpresa1, List<Unidade> uEmpresa) {
		
		
		List<Unidade> unidades_ = new ArrayList<>();
		
		if (uGrupoEmpresa1 != null && !unidades_.isEmpty()) {
			unidades_.addAll(uGrupoEmpresa1);
		}
		
		if (uEmpresa != null && !unidades_.isEmpty()) {
			unidades_.addAll(uEmpresa);
		}

		List<Long> unidadesCalculada = unidades_.stream().map(p -> p.getId()).collect(Collectors.toList());
		
		if (unidadeId != null) {
			unidadesCalculada.addAll(unidadeId);
		}
		
		if (unidadesCalculada.isEmpty()) {
			unidadesCalculada = null;
		}
		return unidadesCalculada;
	}
	
	private Flowable<HttpResponse<List<Unidade>>> findByGrupoEmpresaRx(List<String> grupoEmpresa) {
		
		if (null != grupoEmpresa  && !grupoEmpresa.isEmpty()) {
			return unidadeClient.findByGrupoEmpresaIdInListRxH(grupoEmpresa);
		}
		
		return Flowable.just(HttpResponse.ok(Collections.emptyList()));
	}
	
	private Flowable<HttpResponse<List<Unidade>>> findByEmpresaRx(List<String> empresa) {

		if (null != empresa  && !empresa.isEmpty()) {
		
			return unidadeClient.findByEmpresaIdInListRxH(empresa);
			
		}

		return Flowable.just(HttpResponse.ok(Collections.emptyList()));
	}
	
	private byte[] generatePdfFromRegistroPreAtendimentoCancelamento(Iterable<RegistroPreAtendimento> fullRegistrosPreAtendimentos, String dataDe, String dataAte) throws IOException {
		
		return TemplateToPdfConverterUtil.generatePdf(
    			TemplateModel.<RegistroPreAtendimentoCancelamentoRel>of(geraRelatorioDomain(fullRegistrosPreAtendimentos, dataDe, dataAte)), 
    			TEMPLATE_BODY_ATENDIMENTO_RELATORIO_CANCELAMENTO, 
    			TEMPLATE_CSS_ATENDIMENTO_RELATORIO_CANCELAMENTO);
		
	}
	
	private RegistroPreAtendimentoCancelamentoRel geraRelatorioDomain(Iterable<RegistroPreAtendimento> registrosPreAtendimentos,
			String dataDe, 
			String dataAte) {
		
		Map<Integer, List<RegistroPreAtendimento>> mapaPagina = agruparAgendamentosPorPagina(registrosPreAtendimentos, 14);//new HashMap<Integer, List<Agendamento>>();
		
		List<Pagina<RegistroPreAtendimento>> paginas = generateListPaginaByMap(mapaPagina);
		
		if (!paginas.isEmpty()) {
			paginas.get(paginas.size() - 1).setUltimaPagina(true);
		}
		
		RegistroPreAtendimentoCancelamentoRel rel = new RegistroPreAtendimentoCancelamentoRel();
		
		rel.setPaginas(paginas);
		
		rel.setDataDe(getDateDeTimestamp(dataDe));
		rel.setDataAte(getDateAteTimestamp(dataDe));
		
		List<RegistroPreAtendimento> collect = StreamSupport.stream(registrosPreAtendimentos.spliterator(), false).collect(Collectors.toList());

//		rel.setTotal(Iterables.size(registrosPreAtendimentos) + "");
		rel.setTotalConexao(collect.stream().filter(a -> Normalizer.normalize(a.getMotivoCancelamento().getDescricao(), Form.NFD).replaceAll("\\p{M}", "").toLowerCase().indexOf("conexao")       > -1).collect(Collectors.toList()).size() + "");
		rel.setTotalMezzow (collect.stream().filter(a -> Normalizer.normalize(a.getMotivoCancelamento().getDescricao(), Form.NFD).replaceAll("\\p{M}", "").toLowerCase().indexOf("instabilidade") > -1).collect(Collectors.toList()).size() + "");
		rel.setTotalOperacao("0");
//		rel.setTotalPaciente(Iterables.size(registrosPreAtendimentos) + "");
		
		return rel;
	}
	
	private List<Pagina<RegistroPreAtendimento>> generateListPaginaByMap(Map<Integer, List<RegistroPreAtendimento>> mapaPagina) {
		return mapaPagina.keySet().stream().map(numeroPagina -> {
			
			Pagina<RegistroPreAtendimento> p = new Pagina<RegistroPreAtendimento>();
			
			if (1 == numeroPagina) {
				p.setPrimeiraPagina(true);
			}
			
			p.setNumeroPagina(numeroPagina + "");
			p.setDominios(mapaPagina.get(numeroPagina));
			
			return p;
			
		}).collect(Collectors.toList());
	}


	private Map<Integer, List<RegistroPreAtendimento>> agruparAgendamentosPorPagina(Iterable<RegistroPreAtendimento> agendamentos, int qntPorPagina) {
		
		int i = 1;
		int pagina = 1;
		int size = 0;
		Map<Integer, List<RegistroPreAtendimento>> mapaPagina = new HashMap<Integer, List<RegistroPreAtendimento>>();
		
		for (RegistroPreAtendimento agendamento : agendamentos) {
			
			if (mapaPagina.get(pagina) == null) {
				mapaPagina.put(pagina, new ArrayList<RegistroPreAtendimento>());
			}
			
			mapaPagina.get(pagina).add(agendamento);
			
			if (i == qntPorPagina) {
				pagina++;
				i = 0;
			}
			
			i++;
			size++;
		}
		return mapaPagina;
	}
	
	private Timestamp getDateDeTimestamp(String dataDe) {
		LocalDateTime dataIni = null;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		if (dataDe != null) {
			dataDe += " 00:00";
			dataIni = LocalDateTime.parse(dataDe, formatter);
		}
		
		if (dataIni == null) {
			dataIni = LocalDateTime.now().minusMonths(2L);
		}
		return Timestamp.valueOf(dataIni);
	}
	
	private Timestamp getDateAteTimestamp(String dataAte) {
		
		LocalDateTime dataFim = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		
		if (dataAte != null) {
			dataAte += " 23:59"; 
			dataFim = LocalDateTime.parse(dataAte, formatter);
		}
		
		if (dataFim == null) {
			dataFim = LocalDateTime.now();
		}
		
		return Timestamp.valueOf(dataFim);
	}
	
	public class RegistroPreAtendimentoCancelamentoRel {

		private Timestamp dataDe;
		private Timestamp dataAte;
		
		private List<Pagina<RegistroPreAtendimento>> paginas;

		private String totalConexao;
		private String totalPaciente;
		private String totalOperacao;
		private String totalMezzow;
		private String total;
		public Timestamp getDataDe() {
			return dataDe;
		}
		public void setDataDe(Timestamp dataDe) {
			this.dataDe = dataDe;
		}
		public Timestamp getDataAte() {
			return dataAte;
		}
		public void setDataAte(Timestamp dataAte) {
			this.dataAte = dataAte;
		}
		public List<Pagina<RegistroPreAtendimento>> getPaginas() {
			return paginas;
		}
		public void setPaginas(List<Pagina<RegistroPreAtendimento>> paginas) {
			this.paginas = paginas;
		}
		public String getTotalConexao() {
			return totalConexao;
		}
		public void setTotalConexao(String totalConexao) {
			this.totalConexao = totalConexao;
		}
		
		public String getTotalPaciente() {
			return totalPaciente;
		}
		public void setTotalPaciente(String totalPaciente) {
			this.totalPaciente = totalPaciente;
		}
		public String getTotalOperacao() {
			return totalOperacao;
		}
		public void setTotalOperacao(String totalOperacao) {
			this.totalOperacao = totalOperacao;
		}
		public String getTotalMezzow() {
			return totalMezzow;
		}
		public void setTotalMezzow(String totalMezzow) {
			this.totalMezzow = totalMezzow;
		}
		public String getTotal() {
			return total;
		}
		public void setTotal(String total) {
			this.total = total;
		}
		
		
		
	}
	
}
