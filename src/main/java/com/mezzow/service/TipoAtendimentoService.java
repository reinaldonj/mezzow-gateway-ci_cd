
package com.mezzow.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.mezzow.domain.ContratoProduto;
import com.mezzow.domain.Empresa;
import com.mezzow.domain.GrupoEmpresa;
import com.mezzow.domain.Produto;
import com.mezzow.domain.TipoAtendimento;
import com.mezzow.rest.client.ProdutoClient;
import com.mezzow.rest.client.TipoAtendimentoClient;
import com.mezzow.rest.domain.TipoAtendimentoResponse;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Sort;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

@Introspected
@Singleton
public class TipoAtendimentoService {
	
	@Inject
	private TipoAtendimentoClient client;
	
	@Inject
	private ProdutoClient produtoClient;
	
	public Single<Optional<TipoAtendimentoResponse>> show(Long id) {

		return Single.fromObservable(Observable.just(client.show(id)))
				.subscribeOn(Schedulers.io())
				.flatMap(t -> {
					if(t.isPresent()) {
						Optional<Produto> show = produtoClient.show(t.get().getProduto().getId());
						return Single.just(Optional.of(new TipoAtendimentoResponse(t.get().getId(), t.get().getNome(), t.get().getDescricao(), show.get())));
					} else {
						return null;
					}
				});
	}
	
	public Page<TipoAtendimento> list(
			Optional<String>  nomeProduto, 
    		Optional<String> nome, 
    		Optional<String> descricao,  
    		Optional<Integer> max, 
    		Optional<Integer> offset, 
    		Optional<String> sort, 
    		Optional<String> order){
		
		if(max.isPresent() && max.get() == -1) {
			max = Optional.of(999999);
		}
		
		if (nomeProduto.isPresent() && !nomeProduto.isEmpty() && !nomeProduto.get().isEmpty()) {
			
			List<Produto> findByNome = produtoClient.findByNome(nomeProduto.get());
			
			if(!findByNome.isEmpty()) {
				Map<Long, Produto> mapProduto = findByNome.stream().collect( Collectors.toMap(Produto::getId, p -> p)  );
				
				List<String> idsProduto = findByNome.stream().map(p -> String.valueOf( p.getId() )).collect(Collectors.toList());
				
				Page<TipoAtendimento> find = client.find(idsProduto,null,null,
						99999 , null , null , null
						/*max.orElse(50),offset.orElse(null),sort.orElse("id"),order.orElse("desc")*/);
				
				List<TipoAtendimento> it = find.getContent().stream().map(cp -> {
								return new TipoAtendimento(cp.getId(), cp.getNome(), cp.getDescricao(), mapProduto.get(cp.getProduto().getId()));
							}).collect(Collectors.toList());
				
				List<TipoAtendimento> retorno = new ArrayList<>();
				retorno.addAll(it);
				
				Page<TipoAtendimento> retornoPageTipoAtendimento = null;
				
				final int direction = "ASC".equalsIgnoreCase(order.orElse("ASC")) ? 1 : -1;
				
				final int offsetLocal = offset.orElse(0);
				final String sortLocal = sort.orElse("id");
				
				switch (sortLocal) {
					case "id":
						retornoPageTipoAtendimento = Page.of(
								retorno
    								.stream()
    							    .sorted((agend1, agend2) -> {
    							    	
    							    	return direction * agend1.getId().compareTo(agend2.getId());
    							    })
    							    .skip(offsetLocal * max.orElse(10))
    						        .limit(max.orElse(10))
    							    .collect(Collectors.toList()),
    							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
							    find.getTotalSize()
						    );		
						break;
					case "produto.nome":
						retornoPageTipoAtendimento = Page.of(
								retorno
    								.stream()
    							    .sorted((agend1, agend2) -> {
    							    	return direction * agend1.getProduto().getNome().compareTo(agend2.getProduto().getNome());
    							    })
    							    .skip(offsetLocal * max.orElse(10))
    						        .limit(max.orElse(10))
    							    .collect(Collectors.toList()),
    							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
							    find.getTotalSize()
						    );		
						break;
					case "nome":
						retornoPageTipoAtendimento = Page.of(
								retorno
    								.stream()
    							    .sorted((agend1, agend2) -> {
    							    	return direction * agend1.getNome().compareTo(agend2.getNome());
    							    })
    							    .skip(offsetLocal * max.orElse(10))
    						        .limit(max.orElse(10))
    							    .collect(Collectors.toList()),
    							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
							    find.getTotalSize()
						    );		
						break;
					case "descricao":
						retornoPageTipoAtendimento = Page.of(
								retorno
    								.stream()
    							    .sorted((agend1, agend2) -> {
    							    	return direction * agend1.getDescricao().compareTo(agend2.getDescricao());
    							    })
    							    .skip(offsetLocal * max.orElse(10))
    						        .limit(max.orElse(10))
    							    .collect(Collectors.toList()),
    							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
							    find.getTotalSize()
						    );		
						break;
					default:
						retornoPageTipoAtendimento = Page.of(
								retorno
    								.stream()
    							    .sorted((agend1, agend2) -> {
    							    	
    							    	return direction * agend1.getId().compareTo(agend2.getId());
    							    })
    							    .skip(offsetLocal * max.orElse(10))
    						        .limit(max.orElse(10))
    							    .collect(Collectors.toList()),
    							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
							    find.getTotalSize()
						    );		
						break;
				}
				
				
				
				return retornoPageTipoAtendimento;
				
				
				
			} else {
				return Page.empty();
			}
			
		}else {
			
			
			Page<TipoAtendimento> find = client.find(null,nome.orElse(null),descricao.orElse(null),99999 , null , null , null);
			
			//java STREAM
			List<TipoAtendimento> it = find.getContent().stream()
	    			.map( cp -> new TipoAtendimento(cp.getId(),cp.getNome(), cp.getDescricao(), produtoClient.show(cp.getProduto().getId()).orElse(null)))
	    			.collect(Collectors.toList());
			
			List<TipoAtendimento> retorno = new ArrayList<>();
			retorno.addAll(it);
			
			Page<TipoAtendimento> retornoPageTipoAtendimento = null;
			
			final int direction = "ASC".equalsIgnoreCase(order.orElse("ASC")) ? 1 : -1;
			
			final int offsetLocal = offset.orElse(0);
			final String sortLocal = sort.orElse("id");
			
			switch (sortLocal) {
				case "id":
					retornoPageTipoAtendimento = Page.of(
							retorno
								.stream()
							    .sorted((agend1, agend2) -> {
							    	
							    	return direction * agend1.getId().compareTo(agend2.getId());
							    })
							    .skip(offsetLocal * max.orElse(10))
						        .limit(max.orElse(10))
							    .collect(Collectors.toList()),
							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
						    find.getTotalSize()
					    );		
					break;
				case "produto.nome":
					retornoPageTipoAtendimento = Page.of(
							retorno
								.stream()
							    .sorted((agend1, agend2) -> {
							    	return direction * agend1.getProduto().getNome().compareTo(agend2.getProduto().getNome());
							    })
							    .skip(offsetLocal * max.orElse(10))
						        .limit(max.orElse(10))
							    .collect(Collectors.toList()),
							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
						    find.getTotalSize()
					    );		
					break;
				case "nome":
					retornoPageTipoAtendimento = Page.of(
							retorno
								.stream()
							    .sorted((agend1, agend2) -> {
							    	return direction * agend1.getNome().compareTo(agend2.getNome());
							    })
							    .skip(offsetLocal * max.orElse(10))
						        .limit(max.orElse(10))
							    .collect(Collectors.toList()),
							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
						    find.getTotalSize()
					    );		
					break;
				case "descricao":
					retornoPageTipoAtendimento = Page.of(
							retorno
								.stream()
							    .sorted((agend1, agend2) -> {
							    	return direction * agend1.getDescricao().compareTo(agend2.getDescricao());
							    })
							    .skip(offsetLocal * max.orElse(10))
						        .limit(max.orElse(10))
							    .collect(Collectors.toList()),
							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
						    find.getTotalSize()
					    );		
					break;
				default:
					retornoPageTipoAtendimento = Page.of(
							retorno
								.stream()
							    .sorted((agend1, agend2) -> {
							    	
							    	return direction * agend1.getId().compareTo(agend2.getId());
							    })
							    .skip(offsetLocal * max.orElse(10))
						        .limit(max.orElse(10))
							    .collect(Collectors.toList()),
							    createPageable(offset.orElse(null), max.orElse(null), sort.orElse(null), order.orElse(null)),
						    find.getTotalSize()
					    );		
					break;
			}
			
			
			
			return retornoPageTipoAtendimento;
			
		}
		
//		if (nomeProduto.isPresent()) {
//			return 
//			Observable.fromIterable(produtoClient.findByNome(nomeProduto.get()))
//			.toMap(p -> p.getId())
//			.flatMap(mapProduto -> {
//				
//				List<String> idsProduto = mapProduto.keySet().parallelStream().map(Object::toString).collect(Collectors.toList());
//				
//				 return Observable
//						.fromIterable(client.find(idsProduto,null,null,max.orElse(50),offset.orElse(null),sort.orElse("id"),order.orElse("desc")))
//						.subscribeOn(Schedulers.io())
//						.map(cp -> {
//							return new TipoAtendimento(cp.getId(), cp.getNome(), cp.getDescricao(), mapProduto.get(cp.getProduto().getId()));
//						})
//						.toList()
//						.map(page -> {
//							return Page.of(page, Pageable.from(1, 10), 100);
//						});				
//			})
//			.subscribeOn(Schedulers.io());
			
		   
			
//		}else {
			
//			Page<TipoAtendimento> find = client.find(null,nome.orElse(null),descricao.orElse(null),max.orElse(50),offset.orElse(null),sort.orElse("id"),order.orElse("desc"));
//			
//			//java STREAM
//			List<TipoAtendimento> collect = find.getContent().stream()
//	    			.map( cp -> new TipoAtendimento(cp.getId(),cp.getNome(), cp.getDescricao(), produtoClient.show(cp.getProduto().getId()).orElse(null)))
//	    			.collect(Collectors.toList());
//			
//			return Single.just(Page.of(collect, find.getPageable(), find.getTotalSize()));
			
//		}
		
	}
	
	private Pageable createPageable(Integer offset, Integer max, String sort, String order) {
		return Pageable.from(Optional.ofNullable(offset).orElse(0),max == null ? 10 : max, Sort.of(new Sort.Order(sort != null ? sort : "id", order != null ? Sort.Order.Direction.valueOf(order.toUpperCase()) : Sort.Order.Direction.ASC, true)) );
	}
}	
/*
=======
package com.mezzow.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.mezzow.domain.Produto;
import com.mezzow.domain.TipoAtendimento;
import com.mezzow.rest.client.ProdutoClient;
import com.mezzow.rest.client.TipoAtendimentoClient;
import com.mezzow.rest.domain.TipoAtendimentoResponse;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.model.Page;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

@Introspected
@Singleton
public class TipoAtendimentoService {

	@Inject
	private TipoAtendimentoClient client;

	@Inject
	private ProdutoClient produtoClient;

	public Single<Optional<TipoAtendimentoResponse>> show(Long id) {

		return Single.fromObservable(Observable.just(client.show(id)))
				.subscribeOn(Schedulers.io())
				.flatMap(t -> {
					if(t.isPresent()) {
						Optional<Produto> show = produtoClient.show(t.get().getProdutoId());
						return Single.just(Optional.of(new TipoAtendimentoResponse(t.get().getId(), t.get().getNome(), t.get().getDescricao(), show.get())));
					} else {
						return null;
					}
				});
	}

	public Page<TipoAtendimentoResponse> find(@Nullable String nome, @Nullable String descricao, @Nullable Integer max, @Nullable Integer offset, @Nullable String sort, @Nullable String order) {
		
		Page<TipoAtendimento> find = client.find(nome, descricao, max, offset, sort, order);
		
		List<TipoAtendimentoResponse> list = find.getContent().stream().map(item -> 
			new TipoAtendimentoResponse(item.getId(), item.getNome(), item.getDescricao(), produtoClient.show(item.getProdutoId()).get())
		).collect(Collectors.toList());
		
		return Page.of(list, find.getPageable(), find.getTotalSize());
				
	}
}
>>>>>>> 27371051141e21f819ac457db7a261daf1913a73
*/
